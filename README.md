# Report-management

## Development Guide line

1. Install [npm](https://nodejs.org/en/) (5.6.0 or higher)

## Environment Variables

## Build Setup

``` bash
# pull the lastest code
report-management> git pull

# install all dependencies 
report-management> cd /server && npm install
report-management> cd /client && npm install

#Remove/restart server if server exist
#For Linux
kill <id> (id of server and client)

# Run server at http://localhost:3000 and server of client at Run server at http://localhost:80
report-management> nohup npm start >/dev/null 2>&1 & # runs server in background
report-management> nohup webpack-dev-server --host 0.0.0.0 --port 80 --mode development --hot >/dev/null 2>&1 & # runs server of client in background

#For init database testing
report-management> node /server/db/init.js
```

# APIs

[![Run in Postman](https://run.pstmn.io/button.svg)](https://documenter.getpostman.com/view/4195948/S1LyVnjS)

> **NOTE** All APIs always require the authentication

