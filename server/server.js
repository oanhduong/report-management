global.BASE_DIR = __dirname
const path = require('path')
const http = require('http')
const express = require('express')
const fileUpload = require('express-fileupload')
const mongoose = require('mongoose')
const cors = require('cors')
const bodyParser = require('body-parser')
const multiQuery = require('mine/multiple-query')
const config = require('./config')
const logger = require('mine/logger')

Date.prototype.isValid = function () {
  return this.getTime() === this.getTime();
}

const app = express()
const port = config.server.port
const server = http.createServer(app)

// Set up mongoose connection
mongoose.connect(config.db.url, { useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true })
mongoose.Promise = global.Promise
const db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'))

//add the router
app.use(cors())
app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))
app.use(fileUpload())
app.use('/api', require('mine/auth').router)
app.use('/api/upload', require('mine/upload').router)
app.use('/api/reports', multiQuery, require('mine/report').router)
app.use('/api/events', require('mine/event').router)
app.use('/api/users', multiQuery, require('mine/user').router)
app.use('/api/templates', multiQuery, require('mine/template').router)
app.use('/api/quytrinh', multiQuery, require('mine/quytrinh').router)
app.use('/api/workPlaces', require('mine/work-place').router)
app.use('/api/download', require('mine/download').router)
app.use('/api/thongke', multiQuery, require('mine/thongke').router)



app.use('/data', express.static(path.join(__dirname, config.statics.dataDir)))

const root = path.join(__dirname, config.statics.dir)
app.use(express.static(root))
app.get('*', (req, res, next) => {
  res.sendFile('index.html', { root })
})

app.use((err, req, res, next) => {
  res.status(500).json({message: err.message})
})

const ws = require('socket.io')(server)
ws.on('connection', (socket) => {
  socket.on('disconnect', () => {
    logger.info(`socket connection - disconnected - client id = ${socket.id}`)
  })
  logger.info(`socket connection - established - client id = ${socket.id}`)
})
global.ws = ws

server.setTimeout(1 * 60 * 60 * 1000)
server.listen(port, (err) => {
  if (err) {
    logger.error(`Failed to start server`, err)
  }
  logger.info(`Server is running at port`, port)
  try {
    const {everyDay, everyMonth, everyQuarter} = require('mine/deadline')
    everyDay().start()
    everyMonth().start()
    everyQuarter().start()
  } catch (err) {
    logger.error(`Start jobs error`, err)
  }
})
