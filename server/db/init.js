const cryptoJS = require('crypto-js')
const config = require('../config')

const secret = config.jwt.secrectKey

const uri = config.db.url
const mongodb = require('mongodb').MongoClient
mongodb.connect(uri, (err, db) => {
  if (err) {
    console.log(`Cannot connect to server ${uri}`)
    throw err
  }
  console.log(`Connected to server ${uri}`)

  const steps = [
    initEvents,
    initNormalReports,
    initSummaryReports,
    initTemplates,
    initQuyTrinh,
    initWorkPlaces,
    initUsers,
  ]

  runSequences(db, steps, 0)
})

function runSequences(db, steps, idx) {
  if (idx < steps.length) {
    steps[idx](db).then(() => runSequences(db, steps, idx + 1))
  } else {
    // finish all steps, close database connection
    db.close()
    console.log('Done. Disconnected')
  }
}

function readConfig(name) {
  const fs = require('fs')
  const path = require('path')
  const data = JSON.parse(fs.readFileSync(path.join(__dirname, name), 'utf8'))
  return data
}

function initEvents(db) {
  return new Promise(function (resolve, reject) {
    db.collection('events').deleteMany().then(() => {
      console.log('Deleted all events records')
      resolve()
    })
  })
}

function initNormalReports(db) {
  return new Promise(function (resolve, reject) {
    db.collection('normalReports').deleteMany().then(() => {
      console.log('Deleted all normalReports records')
      resolve()
    })
  })
}

function initSummaryReports(db) {
  return new Promise(function (resolve, reject) {
    db.collection('summaryReports').deleteMany().then(() => {
      console.log('Deleted all summaryReports records')
      resolve()
    })
  })
}

function initWorkPlaces(db) {
  return new Promise(function (resolve, reject) {
    const workplaces = readConfig('default_workplace.json').workplaces

    db.collection('workPlaces').deleteMany().then(() => {
      console.log('Deleted all workplaces records')

      insert(db.collection('workPlaces'), workplaces, resolve)
    })
  })
}

function initQuyTrinh(db) {
  return new Promise(function (resolve, reject) {
    const quytrinhs = readConfig('default_quytrinh.json').quytrinh

    db.collection('quytrinh').deleteMany().then(() => {
      console.log('Deleted all quytrinh records')

      insert(db.collection('quytrinh'), quytrinhs, resolve)
    })
  })
}

function insert(collection, items, resolve) {
  if (items.length > 0) {
    collection.insertOne(items.pop()).then(() => insert(collection, items, resolve))
  } else {
    resolve()
  }
}

function initTemplates(db) {
  return new Promise(function (resolve, reject) {
    db.collection('templates').deleteMany().then(() => {
      console.log('Deleted all templates records')
      resolve()
    })
  })
}

function initUsers(db) {
  return new Promise(function (resolve, reject) {
    const users = readConfig('default_users.json').users
    db.collection('workPlaces').find().toArray(function (err, workPlaces) {
      db.collection('users').deleteMany().then(() => {
        console.log('Deleted all user records')
        for (let idx = 0, len = users.length; idx < len; idx++) {
          let user = users[idx]
          user.password = encrypt(user.password).toString()
          user.workPlace = workPlaces.find((workPlace) => workPlace.shortName === user.workPlace)._id
          db.collection('users').insertOne(user)
          console.log(`Inserted ${user.username} into database`)
        }
        resolve()
      })
    })
  })
}

function encrypt(plainText) {
  return cryptoJS.AES.encrypt(plainText, secret)
}
