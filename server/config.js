const productionIP = '10.17.4.28'
module.exports = {
  statics: {
    dataDir: 'data',
    dir: 'dist',
  },
  server: {
    url: `http://${String(process.env.NODE_ENV).toLowerCase() === 'development' ? 'localhost': productionIP }:${process.env.PORT || 3000}`,
    port: process.env.PORT || 3000,
  },
  jwt: {
    secrectKey: 'randomkey',
    expiresInMinutes: process.env.JWT_TIMEOUT || 60 * 8, // 8 hours
    algorithm: 'HS256'
  },
  db: {
    url: process.env.DB_URL || 'mongodb://localhost:27017/pvgas_report'
  },
  pdf: {
    orther: { 
      format: 'A3',
      border: {
        top: '2cm',            // default is 0, units: mm, cm, in, px
        right: '3cm',
        bottom: '2cm',
        left: '2cm'
      },
    },
    linux: {
      pageSize: 'A4',
      'margin-top':'20mm',
      'margin-bottom':'20mm',
      'margin-left':'20mm',
      'margin-right':'30mm'
    }
  },
  secret: process.env.SECRET || 'randomkey',
  isProduction: !(String(process.env.NODE_ENV).toLowerCase() === 'development')
}