CKEDITOR.plugins.add("comments", {
  icons: "comments",
  init: function(editor) {
    CKEDITOR.addCss(
      `
      [data-title] {
        position: relative;
        cursor: help;
      }
      
      [data-title]:hover::before {
        content: attr(data-title);
        position: absolute;
        bottom: -30px;
        display: inline-block;
        padding: 3px 6px;
        border-radius: 2px;
        background: #000;
        color: #fff;
        font-size: 14px;
        font-family: sans-serif;
        white-space: nowrap;
        z-index: 10000;
      }
      [data-title]:hover::after {
        content: '';
        position: absolute;
        bottom: -12px;
        left: 8px;
        display: inline-block;
        color: #fff;
        border: 8px solid transparent;	
        border-bottom: 8px solid #000;
      }
      `
    );
    editor.addCommand("addComment", {
      exec: function(editor) {
        var selected = editor.getSelectedHtml().getHtml();
        if (selected) {
          let dComment = new CKEDITOR.dialogCommand("NewCommentDialog");
          dComment.exec(editor);
        }
      }
    });
    editor.on("doubleclick", function(evt) {
      var element = evt.data.element;
      if (element.hasClass("ckComment")) {
        let dComment = new CKEDITOR.dialogCommand("EditCommentDialog");
        dComment.exec(editor);
      } else {
        console.log("no comment");
      }
    });
    editor.ui.addButton("comments", {
      label: "Add comments",
      command: "addComment",
      toolbar: "insert"
    });
    if (editor.contextMenu) {
      editor.addMenuGroup("abbrGroup");
      editor.addMenuItem("abbrItem", {
        label: "Edit Abbreviation",
        icon: this.path + "icons/comments.png",
        command: "addComment",
        group: "abbrGroup"
      });

      editor.contextMenu.addListener(function(element) {
        if (element.getAscendant("abbr", true)) {
          return { abbrItem: CKEDITOR.TRISTATE_OFF };
        }
      });
    }
    CKEDITOR.dialog.add("NewCommentDialog", this.path + "dialogs/new.js");
    CKEDITOR.dialog.add("EditCommentDialog", this.path + "dialogs/edit.js");
  }
});
