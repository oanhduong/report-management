CKEDITOR.dialog.add("EditCommentDialog", function(editor) {
  return {
    title: "Sửa comment",
    minWidth: 400,
    minHeight: 150,
    contents: [
      {
        id: "tab-basic",
        elements: [
          {
            type: "textarea",
            id: "title",
            label: "Comment",
            validate: CKEDITOR.dialog.validate.notEmpty(
              "Vui lòng nhập comment..."
            ),
            setup: function(element) {
              this.setValue(element.getAttribute("data-title"));
            },
            commit: function(element) {
              element.setAttribute("data-title", this.getValue());
            }
          }
        ]
      }
    ],
    buttons: [
      CKEDITOR.dialog.okButton,
      {
        id: "unique name",
        type: "button",
        label: "Xoá comment",
        title: "Xoá comment",
        accessKey: "C",
        disabled: false,
        onClick: function(e) {
          let element = e.data.dialog.element;
          element.removeClass("tooltip");
          element.removeClass("ckComment");
          element.removeStyle("background-color");
          element.removeAttribute("data-title");
          CKEDITOR.dialog.getCurrent().hide();
        }
      },
      CKEDITOR.dialog.cancelButton
    ],
    onShow: function() {
      var selection = editor.getSelection();
      var element = selection.getStartElement();
      this.element = element;
      this.setupContent(this.element);
    },

    onOk: function() {
      this.commitContent(this.element);
    }
  };
});
