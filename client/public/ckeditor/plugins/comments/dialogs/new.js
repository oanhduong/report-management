// eslint-disable-next-line no-undef
CKEDITOR.dialog.add("NewCommentDialog", function(editor) {
  return {
    title: "Thêm comment",
    minWidth: 400,
    minHeight: 150,
    contents: [
      {
        elements: [
          {
            type: "textarea",
            id: "title",
            label: "Comment",
            // eslint-disable-next-line no-undef
            validate: CKEDITOR.dialog.validate.notEmpty(
              "Vui lòng nhập comment..."
            ),
            setup: function(element) {
              this.setValue(element.getAttribute("title"));
            },
            commit: function(element) {
              element.setAttribute(
                "style",
                "background-color: yellow;"
              );
              element.setAttribute("class", "ckComment tooltip");
              element.setAttribute("data-title", this.getValue());
              var selection = editor.getSelection();
              if (selection.getType() == CKEDITOR.SELECTION_TEXT) {
                element.setHtml(selection.getSelectedText());
              } else if (selection.getType() == CKEDITOR.SELECTION_ELEMENT) {
                element.setHtml(selection.getSelectedElement().getHtml());
              } else {
                console.log("no selected");
              }
            }
          }
        ]
      }
    ],

    onShow: function() {
      this.element = editor.document.createElement("span");
      this.setupContent(this.element);
    },

    onOk: function() {
      this.commitContent(this.element);
      editor.insertElement(this.element);
      
    }
  };
});
