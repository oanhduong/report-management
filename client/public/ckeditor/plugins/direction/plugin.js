CKEDITOR.plugins.add("direction", {
  icons: "direction",
  init: function(editor) {
    editor.addCommand("direction", {
      exec: function(editor) {
        var selected = editor.getSelectedHtml().getHtml();
        if (selected) {
          editor.fire("openDirection");
        }
      }
    });
    editor.on("doubleclick", function(evt) {
      var element = evt.data.element;
      if (element.hasClass("ckDirection")) {
        editor.fire("updateDirection");
      } else {
        console.log("no action");
      }
    });
    editor.ui.addButton("direction", {
      label: "Giám đốc chỉ đạo",
      command: "direction",
      toolbar: "insert"
    });
    if (editor.contextMenu) {
      editor.addMenuGroup("abbrGroup");
      editor.addMenuItem("abbrItem", {
        label: "Edit Abbreviation",
        icon: this.path + "icons/direction.png",
        command: "direction",
        group: "abbrGroup"
      });

      editor.contextMenu.addListener(function(element) {
        if (element.getAscendant("abbr", true)) {
          return { abbrItem: CKEDITOR.TRISTATE_OFF };
        }
      });
    }
  }
});
