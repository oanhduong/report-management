import * as ActionTypes from "actions/ActionTypes";

export default function base(state = {}, action) {
  switch (action.type) {
    case ActionTypes.GET_WORKPLACES_FAIL:
    case ActionTypes.GET_USERS_FAIL:
    case ActionTypes.SIGNIN_FAIL: {
      return {
        ...state,
        type: action.type,
        message: action.message
      };
    }
    case ActionTypes.GET_WORKPLACES_PENDING:
    case ActionTypes.GET_USERS_PENDING:
    case ActionTypes.SIGNIN_PENDING: {
      return {
        ...state,
        type: action.type,
        message: ""
      };
    }
    case ActionTypes.SIGNIN_SUCCESS: {
      return {
        ...state,
        type: action.type,
        message: "",
        user: action.user
      };
    }
    case ActionTypes.SIGN_OUT: {
      return {
        ...state,
        type: action.type,
        message: "",
        user: null
      };
    }
    case ActionTypes.GET_USERS_SUCCESS: {
      return {
        ...state,
        type: action.type,
        message: "",
        users: action.users
      };
    }
    case ActionTypes.GET_WORKPLACES_SUCCESS: {
      return {
        ...state,
        type: action.type,
        workplaces: action.workplaces
      };
    }
    case ActionTypes.UPDATE_USER: {
      let user = { ...state.user };
      user.firstName = action.user.firstName;
      user.lastName = action.user.lastName;
      user.email = action.user.email;
      return {
        ...state,
        type: action.type,
        user
      };
    }
    default:
      return state;
  }
}
