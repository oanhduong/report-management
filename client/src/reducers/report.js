import * as ActionTypes from "actions/ActionTypes";
import * as _ from "lodash";

export default function base(state = {}, action) {
  switch (action.type) {
    case ActionTypes.GET_REPORT_TEMPLATES_PENDING: {
      return {
        ...state,
        type: action.type,
        message: ""
      };
    }

    case ActionTypes.GET_REPORT_TEMPLATES_ERROR: {
      return {
        ...state,
        type: action.type,
        message: action.message
      };
    }
    case ActionTypes.GET_REPORT_TEMPLATES_SUCCESS: {
      return {
        ...state,
        type: action.type,
        templates: action.templates
      };
    }
    case ActionTypes.CLEAN_TEMPLATE: {
      return {
        ...state,
        type: action.type,
        templates: []
      };
    }

    default:
      return state;
  }
}
