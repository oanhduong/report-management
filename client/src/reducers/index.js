import { persistCombineReducers } from "redux-persist";
import { createWhitelistFilter } from "redux-persist-transform-filter";
import storage from "redux-persist/es/storage";
import usersReducers from "./users";
import reportReducers from "./report";

const config = {
  key: "root",
  storage,
  transforms: [
    createWhitelistFilter("usersReducers", ["user"]),
    createWhitelistFilter("reportReducers", ["reports", "reportDrafts"])
  ]
};

const reducers = persistCombineReducers(config, {
  usersReducers,
  reportReducers
});

export default reducers;
