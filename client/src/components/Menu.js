import React from "react";
import { Config } from "common";
import NavLinkCus from './NavLinkCus'

class Menu extends React.PureComponent {
  render() {
    const { user } = this.props;
    let menus = Config.USER_MENU[user.role];
    return (
      <ul className="sidebar-menu" data-widget="tree">
        {menus.map((item, index) => {
          return <NavLinkCus item={item} key={index} />;
        })}
      </ul>
    );
  }
}

export default Menu;
