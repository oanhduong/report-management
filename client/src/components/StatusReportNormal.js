import React from "react";
import { Constants } from "common";

class StatusReportNormal extends React.PureComponent {
  render() {
    const { report, userRole } = this.props;
    let str = "";
    if (report.isCompleted) {
      return <small>{"Đã duyệt"}</small>;
    }

    if (userRole === Constants.Role.NV) {
      switch (report.status.location.role) {
        case Constants.Role.TP:
          str = "Chờ trưởng phòng duyệt";
          break;
        case Constants.Role.TPB:
          str = "Chờ trưởng phòng ban duyệt";
          break;
        case Constants.Role.GDDV:
          str = "Chờ giám đốc đơn vị duyệt";
          break;
        case Constants.Role.NVKT:
          str = "Chờ nhân viên kỹ thuật tổng hợp";
          break;
      }
    } else if (
      userRole === Constants.Role.TP ||
      userRole === Constants.Role.TPB ||
      userRole === Constants.Role.GDDV
    ) {
      switch (report.status.location.role) {
        case Constants.Role.NV:
          str = "Gửi trả cho nhân viên";
          break;
        case Constants.Role.TPB:
          str = "Chờ trưởng phòng ban duyệt";
          break;
        case Constants.Role.GDDV:
          str = "Chờ giám đốc đơn vị duyệt";
          break;
        case Constants.Role.NVKT:
          str = "Chờ nhân viên kỹ thuật tổng hợp";
          break;
      }
    } else if (userRole === Constants.Role.NVKT) {
      switch (report.status.location.role) {
        case Constants.Role.TPKT:
          str = "Chờ trưởng phòng kỹ thuật duyệt";
          break;
        case Constants.Role.GD:
          str = "Chờ giám đốc duyệt ";
          break;
        case Constants.Role.NV:
          str = "Báo cáo đã duyệt";
          break;
      }
    } else if (userRole === Constants.Role.TPKT) {
      switch (report.status.location.role) {
        case Constants.Role.GD:
          str = "Chờ giám đốc duyệt";
          break;
        case Constants.Role.NV:
          str = "Báo cáo đã duyệt";
          break;
      }
    } else if (userRole === Constants.Role.GD) {
      str = "Đã duyệt";
    }
    return <small>{str}</small>;
  }
}

export default StatusReportNormal;
