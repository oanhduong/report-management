import React from "react";
import { Modal, Form, Select, Input } from "antd";
import { Config, Utils } from "common";
const { Option } = Select;

class TemplateModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      processId:
        props.process && props.process.length > 0 ? props.process[0]._id : "",
      templateType: Config.TemplateType[0].value,
      templateScope: Config.TemplateScope[0].value,
      name: ""
    };
  }

  handleOk = () => {
    const { name, templateScope, templateType, processId } = this.state;
    if (Utils.isEmpty(name)) {
      return Utils.showWarning("Chưa nhập tên template");
    }
    let obj = {
      name,
      type: templateType,
      scope: templateScope,
      quytrinh: processId
    };
    this.props.onAddTemplate(obj);
    this.handleCancel();
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  open = () => {
    this.setState({ visible: true });
  };

  onChangeProcess = value => {
    this.setState({ processId: value });
  };

  onChangeTemplateType = value => {
    this.setState({ templateType: value });
  };

  onChangeTemplateScope = value => {
    this.setState({ templateScope: value });
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps.process && nextProps.process.length > 0) {
      this.setState({ processId: nextProps.process[0]._id });
    }
  };

  onChangeName = e => {
    this.setState({ name: e.target.value });
  };

  render() {
    const {
      visible,
      processId,
      templateScope,
      templateType,
      name
    } = this.state;
    const { process } = this.props;
    return (
      <Modal
        title="Tạo template"
        visible={visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
      >
        <Form layout="vertical">
          <Form.Item label="Tên template" className={"form-item"}>
            <Input value={name} onChange={this.onChangeName} />
          </Form.Item>
          <Form.Item label="Quy trình" className={"form-item"}>
            {process && process.length > 0 && (
              <Select
                style={{ width: "100%" }}
                value={[processId]}
                onChange={this.onChangeProcess}
              >
                {process.map((item, index) => {
                  return (
                    <Option key={index} value={item._id}>
                      {item.name}
                    </Option>
                  );
                })}
              </Select>
            )}
          </Form.Item>
          <Form.Item label="Loại template" className={"form-item"}>
            <Select
              style={{ width: "100%" }}
              value={[templateType]}
              onChange={this.onChangeTemplateType}
            >
              {Config.TemplateType.map((item, index) => {
                return (
                  <Option key={index} value={item.value}>
                    {item.name}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item label="Phạm vi" className={"form-item"}>
            <Select
              style={{ width: "100%" }}
              value={[templateScope]}
              onChange={this.onChangeTemplateScope}
            >
              {Config.TemplateScope.map((item, index) => {
                return (
                  <Option key={index} value={item.value}>
                    {item.name}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}

export default TemplateModal;
