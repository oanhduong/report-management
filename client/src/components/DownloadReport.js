import React from "react";
import Button from "./Button";
import * as Services from "services";
import { Constants } from "common";

class DownloadReport extends React.PureComponent {
  downloadWord = () => {
    const { report, isSummary } = this.props;
    if (isSummary) {
      Services.downloadReportSummary(
        report._id,
        report.name + "." + Constants.DownloadType.DOCX,
        Constants.DownloadType.DOCX
      );
    } else {
      Services.downloadReportNormal(
        report._id,
        report.name + "." + Constants.DownloadType.DOCX,
        Constants.DownloadType.DOCX
      );
    }
  };

  downloadPdf = () => {
    const { report, isSummary } = this.props;
    if (isSummary) {
      Services.downloadReportSummary(
        report._id,
        report.name + "." + Constants.DownloadType.PDF,
        Constants.DownloadType.PDF
      );
    } else {
      Services.downloadReportNormal(
        report._id,
        report.name + "." + Constants.DownloadType.PDF,
        Constants.DownloadType.PDF
      );
    }
  };

  downloadExcel = () => {
    const { report, isSummary } = this.props;
    if (isSummary) {
      Services.downloadReportSummary(
        report._id,
        report.name + "." + Constants.DownloadType.XLS,
        Constants.DownloadType.XLS
      );
    } else {
      Services.downloadReportNormal(
        report._id,
        report.name + "." + Constants.DownloadType.XLS,
        Constants.DownloadType.XLS
      );
    }
  };

  render() {
    return (
      <>
        <Button className="btn btn-primary btn-xs" onClick={this.downloadWord}>
          <i className="fa fa-file-word-o" /> Word
        </Button>
        &nbsp;
        <Button className="btn btn-primary btn-xs" onClick={this.downloadPdf}>
          <i className="fa fa-file-pdf-o" /> PDF
        </Button>
        &nbsp;
        <Button className="btn btn-primary btn-xs" onClick={this.downloadExcel}>
          <i className="fa fa-file-excel-o" /> Excel
        </Button>
      </>
    );
  }
}

DownloadReport.defaultProps = {
  isSummary: false
};

export default DownloadReport;
