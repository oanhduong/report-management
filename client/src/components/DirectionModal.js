import React from "react";
import { Modal, Button, Divider, Form, Select, Input, Radio } from "antd";
import { Utils, Constants } from "common";
import * as _ from "lodash";
const { TextArea } = Input;
const { Option } = Select;

const radioStyle = {
  display: "block",
  height: "30px",
  lineHeight: "30px"
};

const rejectStyle = {
  color: "#E57F7E"
};

const approveStyle = {
  color: "#6EC6AE"
};

const directionStyle = {
  color: "#9FC5F8"
};

class DirectionModal extends React.PureComponent {
  state = {
    visible: false,
    content: "",
    toUsers: [],
    title: "",
    type: 0,
    users: [],
    searchText: ""
  };

  close = () => {
    this.setState({ visible: false });
  };

  open = editor => {
    this.editor = editor;
    var selection = this.editor.getSelection();
    var element = selection.getStartElement();
    this.setState({
      content: "",
      toUsers: [],
      visible: true,
      title: element.getText(),
      type: 0
    });
    this.idDirection = null;
  };

  update = editor => {
    this.editor = editor;
    var selection = this.editor.getSelection();
    var element = selection.getStartElement();
    if (element.hasClass("direction")) {
      let content = element.getAttribute("content");
      let userIdStr = element.getAttribute("userId");
      let userIds = userIdStr.split(",");
      this.idDirection = element.getAttribute("idDirection");
      this.setState({
        content,
        toUsers: userIds,
        visible: true,
        title: element.getFirst().getText(),
        type: 2
      });
    } else if (element.hasClass("approve")) {
      let content = element.getAttribute("content");
      let userIdStr = element.getAttribute("userId");
      let userIds = userIdStr.split(",");
      this.idDirection = element.getAttribute("idDirection");
      this.setState({
        content,
        toUsers: userIds,
        visible: true,
        title: element.getFirst().getText(),
        type: 0
      });
    } else if (element.hasClass("reject")) {
      this.idDirection = element.getAttribute("idDirection");
      this.setState({
        content: "",
        toUsers: [],
        visible: true,
        title: element.getFirst().getText(),
        type: 1
      });
    }
  };

  _getElementSelection = () => {
    let selection = this.editor.getSelection();
    let element = selection.getStartElement();
    let e = element.findOne(".child");
    if (e) {
      e.remove();
    }
    element.removeAttribute("content");
    element.removeAttribute("idDirection");
    element.removeClass("ckDirection");
    element.removeClass("direction");
    element.removeClass("approve");
    element.removeClass("reject");
    return element;
  };

  _getUserName = userId => {
    const { users } = this.props;
    let userIndex = _.findIndex(users, user => user._id === userId);
    let result = "";
    if (userIndex >= 0) {
      result = users[userIndex].firstName + " " + users[userIndex].lastName;
    }
    return result;
  };

  onDirection = () => {
    const { content, toUsers } = this.state;
    if (Utils.isEmpty(content)) {
      return Utils.showWarning("Chưa nhập nội dung chỉ đạo ");
    }
    let users = "";
    if (!toUsers || toUsers.length === 0) {
      return Utils.showWarning("Chưa chọn người cần gởi đến");
    } else {
      toUsers.forEach((item, index) => {
        if (index === 0) {
          users += this._getUserName(item);
        } else {
          users += "," + this._getUserName(item);
        }
      });
    }
    let idDirection = new Date().getTime();
    var element = this._getElementSelection();
    element.setAttribute("userId", toUsers.toString());
    element.setAttribute("class", "ckDirection direction");
    element.setAttribute("content", content);
    element.setAttribute("idDirection", idDirection);
    var htmlStr =
      '<div class="child" style="margin-top: 5px; margin-bottom: 5px;"' +
      '"><div style="color: #9FC5F8;font-size: 14px;">Chỉ đạo!</div>' +
      '<div style="font-size: 14px;">' +
      "Gởi " +
      users +
      " : " +
      content +
      "</div>" +
      "</div>";
    element.appendHtml(htmlStr);
    let obj = {
      id: idDirection,
      users: toUsers,
      message: content,
      kienNghi: element.getFirst().getText(),
      status: "ChiDao"
    };
    this.onFireDirection(obj);
    this.close();
  };

  onApprove = () => {
    const { content, toUsers } = this.state;
    if (Utils.isEmpty(content)) {
      return Utils.showWarning("Chưa nhập nội dung chỉ đạo ");
    }
    let users = "";
    if (!toUsers || toUsers.length === 0) {
      return Utils.showWarning("Chưa chọn người cần gởi đến");
    } else {
      toUsers.forEach((item, index) => {
        if (index === 0) {
          users += this._getUserName(item);
        } else {
          users += "," + this._getUserName(item);
        }
      });
    }
    let idDirection = new Date().getTime();
    var element = this._getElementSelection();
    element.setAttribute("userId", toUsers.toString());
    var htmlStr =
      '<div class="child" style="margin-top: 5px; margin-bottom: 5px;"' +
      '"><div style="color: #6EC6AE;font-size: 14px;">Duyệt!</div>' +
      '<div style="font-size: 14px;">' +
      "Gởi " +
      users +
      " : " +
      content +
      "</div>" +
      "</div>";
    element.appendHtml(htmlStr);
    element.setAttribute("class", "ckDirection approve");
    element.setAttribute("idDirection", idDirection);
    element.setAttribute("content", content);
    let obj = {
      id: idDirection,
      users: toUsers,
      message: content,
      kienNghi: element.getFirst().getText(),
      status: "Duyet"
    };
    this.onFireDirection(obj);
    this.close();
  };

  onReject = () => {
    var element = this._getElementSelection();
    let idDirection = new Date().getTime();
    element.setAttribute("class", "ckDirection reject");
    element.setAttribute("idDirection", idDirection);
    var htmlStr =
      '<div class="child" style="font-size: 14px;margin-top: 5px; margin-bottom: 5px;color: #E57F7E;"> Không duyệt' +
      "</div>";
    element.appendHtml(htmlStr);
    let obj = {
      id: idDirection,
      kienNghi: element.getFirst().getText(),
      status: "KhongDuyet"
    };
    this.onFireDirection(obj);
    this.close();
  };

  onFireDirection = obj => {
    if (this.idDirection) {
      this.props.onDirection(obj, this.idDirection);
    } else {
      this.props.onDirection(obj);
    }
  };

  onRemove = () => {
    this._getElementSelection();
    if (this.idDirection) {
      this.props.onRemoveDirection(this.idDirection);
    }
    this.close();
  };

  onChangeContent = e => {
    this.setState({ content: e.target.value });
  };

  onChangeSelect = value => {
    this.setState({ toUsers: value });
  };

  onChangeType = e => {
    this.setState({
      type: e.target.value
    });
  };

  onOk = () => {
    const { type } = this.state;
    if (parseInt(type) === 0) {
      this.onApprove();
    } else if (parseInt(type) === 1) {
      this.onReject();
    } else if (parseInt(type) === 2) {
      this.onDirection();
    }
  };

  getRoleName = role => {
    let roleName = "";
    switch (role) {
      case Constants.Role.NV:
        roleName = "Nhân viên";
        break;
      case Constants.Role.TP:
        roleName = "Trường phòng";
        break;
      case Constants.Role.TPB:
        roleName = "Trường phòng ban";
        break;
      case Constants.Role.GDDV:
        roleName = "Giám đốc đơn vị";
        break;
      case Constants.Role.NVKT:
        roleName = "Nhân viên kỹ thuật";
        break;
      case Constants.Role.TPKT:
        roleName = "Trưởng phòng kỹ thuật";
        break;
      case Constants.Role.GD:
        roleName = "Giám đốc";
        break;
      case Constants.Role.ADMIN:
        roleName = "Quản trị viên";
        break;
    }
    return roleName;
  };

  handleSearch = value => {
    this.setState({ searchText: value });
    if (value) {
      const { users } = this.props;
      let data = _.filter(users, item => {
        let indexLastName = item.lastName ? item.lastName.indexOf(value) : -1;
        let indexFirstName = item.firstName
          ? item.firstName.indexOf(value)
          : -1;
        return indexLastName >= 0 || indexFirstName >= 0;
      });
      this.setState({ users: data });
    } else {
      this.setState({ users: this.props.users });
    }
  };

  onFocus = () => {
    const { users } = this.props;
    this.setState({ users });
  };

  render() {
    const {
      visible,
      content,
      toUsers,
      title,
      type,
      users,
      searchText
    } = this.state;
    let userData = searchText.length > 0 ? users : this.props.users;
    return (
      <Modal
        title={"Xử lý kiến nghị"}
        visible={visible}
        onCancel={this.close}
        footer={[
          <Button type="primary" onClick={this.onOk} key={1}>
            OK
          </Button>,
          <Button onClick={this.onRemove} key={4}>
            Xoá bỏ
          </Button>
        ]}
      >
        <div style={styles.title}>
          <b>{"Nội dung kiến nghị: "}</b>
          <Divider type={"horizontal"} style={{ margin: 0 }} />
          <p>{title}</p>
        </div>
        <br />
        <b>{"Hướng xử lý: "}</b>
        <Divider type={"horizontal"} style={{ margin: 0 }} />
        <Radio.Group onChange={this.onChangeType} value={type}>
          <Radio
            style={_.merge({}, radioStyle, type === 1 && rejectStyle)}
            value={1}
          >
            Không duyệt
          </Radio>
          <Radio
            style={_.merge({}, radioStyle, type === 0 && approveStyle)}
            value={0}
          >
            Duyệt
          </Radio>
          <Radio
            style={_.merge({}, radioStyle, type === 2 && directionStyle)}
            value={2}
          >
            Chỉ đạo
          </Radio>
        </Radio.Group>
        {(type === 2 || type === 0) && (
          <>
            <br />
            <br />
            <div>
              <b>{"Chỉ đạo nếu có: "}</b>
              <Divider type={"horizontal"} style={{ margin: 0 }} />
            </div>
            <br />
            <Form layout="horizontal">
              <Form.Item
                label="Kính gởi: "
                labelCol={{ span: 4 }}
                wrapperCol={{ span: 20 }}
              >
                <Select
                  mode="multiple"
                  style={{ width: "100%" }}
                  placeholder="Chọn người cần gửi đến"
                  value={toUsers}
                  onChange={this.onChangeSelect}
                  onSearch={this.handleSearch}
                  onFocus={this.onFocus}
                  defaultActiveFirstOption={false}
                  filterOption={false}
                  notFoundContent={null}
                >
                  {userData &&
                    userData.length > 0 &&
                    userData.map((item, index) => {
                      return (
                        <Option value={item._id} key={index}>
                          {item.firstName} {item.lastName} - {item.role} -{" "}
                          {item.workPlace.name}
                        </Option>
                      );
                    })}
                </Select>
              </Form.Item>
              <Form.Item>
                <TextArea
                  rows={4}
                  placeholder={"Nội dung chỉ đạo"}
                  value={content}
                  onChange={this.onChangeContent}
                />
              </Form.Item>
            </Form>
          </>
        )}
      </Modal>
    );
  }
}

const styles = {
  title: {}
};

export default DirectionModal;
