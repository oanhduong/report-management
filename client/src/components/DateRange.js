import React from "react";
import { DatePicker as ADatePicker } from "antd";
import moment from "moment";
const { RangePicker } = ADatePicker;

class DateRange extends React.PureComponent {
  onSubmit = date => {
    let data = {
      startDate: null,
      endDate: null
    };
    if (date && date.length === 2) {
      let d1 = date[0].toDate();
      d1.setMinutes(0);
      d1.setHours(0);
      d1.setMilliseconds(0);
      d1.setSeconds(0);
      let d2 = date[1].toDate();
      d2.setMinutes(59);
      d2.setHours(23);
      d2.setMilliseconds(0);
      d2.setSeconds(59);
      data = {
        startDate: d1.getTime(),
        endDate: d2.getTime()
      };
    }
    const { onDate, onChangeValue } = this.props;
    if (onDate) {
      onDate(data);
    } else {
      onChangeValue(date);
    }
  };

  render() {
    const { start, end } = this.props;
    return (
      <>
        <RangePicker
          ranges={{
            Today: [moment(), moment()]
          }}
          defaultValue={[start, end]}
          format="DD/MM/YYYY"
          onChange={this.onSubmit}
          placeholder={["Ngày bắt đầu", "Ngày kết thúc"]}
        />
      </>
    );
  }
}

export default DateRange;
