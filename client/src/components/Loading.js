import React from "react";
import { Spin } from "antd";

class Loading extends React.PureComponent {
  render() {
    return (
      <div className="container">
        <Spin tip="Loading..." />
      </div>
    );
  }
}

export default Loading;
