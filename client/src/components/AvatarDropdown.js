import React from "react";
import moment from "moment";

class AvatarDropdown extends React.PureComponent {
  render() {
    const { user, onProfile, onLogout } = this.props;
    let fullName = user.username;
    if (user && user.firstName && user.lastName) {
      fullName = user.firstName + " " + user.lastName;
    }
    return (
      <li className="dropdown messages-menu">
        <div className="navbar-custom-menu">
          <ul className="nav navbar-nav">
            <li className="dropdown user user-menu">
              <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                <img
                  src="/public/AdminLTE/img/ic_default_avatar.jpg"
                  className="user-image"
                  alt="User Image"
                />
                <span className="hidden-xs">{fullName}</span>
              </a>
              <ul className="dropdown-menu">
                <li className="user-header">
                  <img
                    src="/public/AdminLTE/img/ic_default_avatar.jpg"
                    className="img-circle"
                    alt="User Image"
                  />

                  <p style={{ textAlign: "center" }}>
                    {fullName} - {user.role}
                    <small>Ngày tạo {moment(user.createdAt).format("lll")}</small>
                  </p>
                </li>
                <li className="user-footer">
                  <div className="pull-left">
                    <a onClick={onProfile} className="btn btn-default btn-flat">
                      Thông tin cá nhân
                    </a>
                  </div>
                  <div className="pull-right">
                    <a onClick={onLogout} className="btn btn-default btn-flat">
                      Đăng xuất
                    </a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </li>
    );
  }
}

export default AvatarDropdown;
