import React from "react";
const loadScript = require("load-script");

const CK_EDITOR = "/public/ckeditor/ckeditor.js";
// const PLUGIN_TOOLTIP = "/public/ckeditor/plugins/comments/tooltip.js";
class CkEditor extends React.PureComponent {
  state = {
    loaded: false
  };

  componentDidMount = () => {
    const { loaded } = this.state;
    if (loaded) {
      this.initCkEditor();
    } else {
      this.loadCkEditor();
    }
  };

  initCkEditor = () => {
    if (window.CKEDITOR) {
      const { id, config, onInitEditor, data } = this.props;
      // window.CKEDITOR.scriptLoader.load(PLUGIN_TOOLTIP);
      let instance = window.CKEDITOR.replace(id, config);
      if (data) {
        instance.setData(data);
      }
      onInitEditor(instance);
    } else {
      console.error("Init ckeditor failed");
    }
  };

  loadCkEditor = () => {
    loadScript(CK_EDITOR, () => {
      this.setState(
        {
          loaded: true
        },
        () => {
          this.initCkEditor();
        }
      );
    });
  };
  render() {
    const { id } = this.props;
    const { loaded } = this.state;
    return (
      <div>
        {loaded && <textarea name={id} id={id} />}
      </div>
    );
  }
}

export default CkEditor;
