import React from "react";
import { Modal, Button, Upload, Icon } from "antd";
import * as Services from "services";
import { Utils } from "common";
import { Button as AButton } from "components";

class AttachmentModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      uploading: false,
      files: props.files
    };
  }

  close = () => {
    this.setState({ visible: false });
  };

  open = () => {
    this.setState({ visible: true });
  };

  onChangeFile = file => {
    this.setState({ uploading: true });
    Services.uploadAttachments([file])
      .then(response => {
        this.props.chooseFiles(response.data.ids);
        this.setState({ uploading: false });
      })
      .catch(error => {
        this.setState({ uploading: false });
        Utils.showError(error);
      });
    return false;
  };
  onRemove = item => {
    this.props.onRemove(item);
  };

  render() {
    const { visible, uploading } = this.state;
    const { loading, files, onRemove } = this.props;
    let ing = loading || uploading;
    return (
      <Modal
        title={"Danh sách file đính kèm "}
        visible={visible}
        onCancel={this.close}
        footer={[
          <Button onClick={this.close} key={1}>
            Đóng
          </Button>
        ]}
      >
        <Upload
          beforeUpload={this.onChangeFile}
          multiple={false}
          fileList={[]}
          showUploadList={{
            showPreviewIcon: false,
            showRemoveIcon: false
          }}
        >
          <Button loading={uploading}>
            <Icon type="upload" />
            &nbsp; Tải lên
          </Button>
        </Upload>
        <table className="table table-bordered" style={{ marginTop: "5px" }}>
          <tbody>
            <tr>
              <th style={{ width: "10px" }}>#</th>
              <th>Tên file</th>
              <th style={{ width: "100px" }}>Đơn vị</th>
              <th style={{ width: "50px" }}>Thực hiện</th>
            </tr>
            {files && files.length == 0 && (
              <tr>
                <td colSpan={4}>Chưa có file đính kèm...</td>
              </tr>
            )}
            {files &&
              files.length > 0 &&
              files.map((item, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>
                      <a href={item.url} target="_blank">
                        {item.name}
                      </a>
                    </td>
                    <td>{item.workPlace.shortName}</td>
                    <td>
                      <AButton
                        className="btn btn-danger btn-xs"
                        onClick={this.onRemove.bind(this, item)}
                      >
                        <i className="fa fa-trash" /> Xoá
                      </AButton>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </Modal>
    );
  }
}

export default AttachmentModal;
