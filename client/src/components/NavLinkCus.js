import React from "react";
import { withRouter, NavLink } from "react-router-dom";

class NavLinkCus extends React.PureComponent {
  active = path => {
    return this.props.location.pathname === path ? "active" : "";
  };

  render() {
    const { item } = this.props;
    return (
      <li className={this.active(item.path)}>
        <NavLink to={item.path} activeClassName="active">
          <i className={item.icon} /> <span>{item.name}</span>
        </NavLink>
      </li>
    );
  }
}

export default withRouter(NavLinkCus);
