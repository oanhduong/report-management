import React from "react";
import moment from "moment";

class NotificationItem extends React.PureComponent {
  renderItem = item => {
    let msg = "";
    let label = "";
    switch (item.action) {
      case "send":
        msg = " đang chờ xử lý";
        label = "label-info";
        break;
      case "reject":
        msg = " đã bị gửi trả";
        label = "label-danger";
        break;
      case "complete":
        msg = " đã được duyệt";
        label = "label-success";
        break;
      case "deadline":
        msg = " tới hạn cần xử lý";
        label = "label-warning";
        break;
    }
    let strLabel = "label " + label;
    return <span className={strLabel}>{msg}</span>;
  };

  render() {
    const { item } = this.props;
    return (
      <li>
        <span className="text">{item.report.name}</span>
        {this.renderItem(item)}
        <small className="label label-primary">
          <i className="fa fa-clock-o"></i> {moment(item.updatedAt).calendar()}
        </small>
      </li>
    );
  }
}

export default NotificationItem;
