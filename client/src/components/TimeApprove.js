import React from "react";
import * as _ from "lodash";

class TimeApprove extends React.PureComponent {
  render() {
    const { report, roles } = this.props;
    let times = [];
    for (let i = 0; i < roles.length; i++) {
      let role = roles[i];
      let time = _.filter(report.ngayDuyet, item => item.role === role);
      if (time) {
        times.concat(time);
      }
    }
    let max = _.maxBy(times, item => new Date(item.time).getTime());
    if (max) {
      return <span>{max.time}</span>;
    }
    return "";
  }
}

export default TimeApprove;
