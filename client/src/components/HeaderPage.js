import React from "react";

class HeaderPage extends React.PureComponent {
  render() {
    const { title } = this.props;
    return (
      <section className="content-header">
        <h1>{title}</h1>
        <ol className="breadcrumb">
          <li>
            <a href="#">
              <i className="fa fa-home" /> Trang chủ
            </a>
          </li>
          <li className="active">{title}</li>
        </ol>
      </section>
    );
  }
}

export default HeaderPage;
