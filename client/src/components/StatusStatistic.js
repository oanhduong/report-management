import React from "react";
import { Constants } from "common";
import { Tag } from "antd";

class StatusStatistic extends React.PureComponent {
  render() {
    const { report } = this.props;
    let status = "";
    let location = report.location;
    let ngayDuyet = report.ngayDuyet;
    if (report.isCompleted === false) {
      if (location === Constants.Role.NV && ngayDuyet.length === 0) {
        status = <Tag color="green">{"Đang Thiết lập Báo Cáo"}</Tag>;
      }
      if (location === Constants.Role.TP || location === Constants.Role.TPB) {
        status = <Tag color="purple">{"Đang kiểm tra"}</Tag>;
      }
      if (
        location === Constants.Role.GDDV ||
        location === Constants.Role.NVKT ||
        location === Constants.Role.TPKT ||
        location === Constants.Role.GD
      ) {
        status = <Tag color="blue">{"Đang duyệt"}</Tag>
      }
      if (location === Constants.Role.NV && ngayDuyet.length > 0) {
        status = <Tag color="tomato">{"Không đạt yêu cầu"}</Tag>;
      }
    } else {
      status = <Tag color="#35AF3D">{"Hoàn thành"}</Tag>;
    }
    return <span>{status}</span>;
  }
}

export default StatusStatistic;
