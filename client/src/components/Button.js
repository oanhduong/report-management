import React from "react";

class Button extends React.PureComponent {
  render() {
    const { className, type, loading, onClick, disabled, icon } = this.props;
    return (
      <button
        type={type}
        className={className}
        onClick={onClick}
        disabled={loading || disabled}
      >
        <>
          {loading && <i className="fa fa-circle-o-notch fa-spin" />}
          {!loading && icon}
          {this.props.children}
        </>
      </button>
    );
  }
}

Button.defaultProps = {
  type: "button"
};

export default Button;
