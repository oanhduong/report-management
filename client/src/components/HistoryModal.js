import React from "react";
import { Modal, Button as AButton, Table } from "antd";
import moment from "moment";
import * as Services from "services";
import { NetworkHelper, Constants } from "common";
import { Button } from "components";

class HistoryModal extends React.PureComponent {
  state = {
    visible: false,
    loading: false,
    data: []
  };
  columns = [
    {
      title: "Người cập nhật",
      dataIndex: "user",
      width: "30%",
      render: (name, record) => {
        return record.user.firstName + " " + record.user.lastName;
      }
    },
    {
      title: "Hành động",
      with: "30%",
      render: (name, record) => {
        let action = "";
        switch (record.action) {
          case "update":
            action = "Cập nhật báo cáo";
            break;
          case "send":
            action = "Gửi báo cáo";
            break;
          case "create":
            action = "Tạo báo cáo";
            break;
          case "reject":
            action = "Gửi trả báo cáo";
            break;
        }
        return action;
      }
    },
    {
      title: "Thời gian cập nhật",
      with: "30%",
      render: (name, record) => moment(record.timestamps).format("llll")
    },
    {
      title: "Tải xuống",
      with: "20%",
      render: (name, record) => {
        return (
          <>
            <Button
              className="btn btn-primary btn-xs"
              onClick={this.downloadFile.bind(this,record, Constants.DownloadType.DOCX)}
            >
              <i className="fa fa-file-word-o" /> Word
            </Button>
            &nbsp;
            <Button
              className="btn btn-primary btn-xs"
              onClick={this.downloadFile.bind(this, record, Constants.DownloadType.PDF)}
            >
              <i className="fa fa-file-pdf-o" /> Pdf
            </Button>
            &nbsp;
            <Button
              className="btn btn-primary btn-xs"
              onClick={this.downloadFile.bind(this, record, Constants.DownloadType.XLS)}
            >
              <i className="fa fa-file-excel-o" /> Excel
            </Button>
          </>
        );
      }
    }
  ];

  close = () => {
    this.setState({ visible: false });
  };

  downloadFile = (rpEvent, type) => {
    let token = localStorage.getItem("token");
    let url = "";
    let urlSuffix = rpEvent.reportId + "?eventid=" 
                    + rpEvent._id + "&type=" 
                    + type;
    if(rpEvent.reportType === "summary") {
      url = "/download/summary/" + urlSuffix;
    } else {
      url = "/download/normal/" + urlSuffix;
    }
    NetworkHelper.downloadFile(url, this.name + "." + type, token);
  }

  loadHistoryReport = reportId => {
    this.setState({ loading: true, data: [] });
    Services.getReportHistory(reportId)
      .then(response => {
        this.setState({ loading: false, data: response });
      })
      .catch(error => {
        this.setState({ loading: false, data: [] });
      });
  };

  open = report => {
    this.name = report.name;
    this.setState({ visible: true }, () => {
      this.loadHistoryReport(report._id);
    });
  };

  render() {
    const { visible, loading, data } = this.state;
    return (
      <Modal
        title={"Lịch sử báo cáo"}
        visible={visible}
        onCancel={this.close}
        width={750}
        footer={[
          <AButton key={1} onClick={this.close}>
            Đóng
          </AButton>
        ]}
      >
        <Table
          columns={this.columns}
          rowKey={record => record._id}
          dataSource={data}
          pagination={false}
          loading={loading}
        />
      </Modal>
    );
  }
}

export default HistoryModal;
