import React from "react";
import { Link } from "react-router-dom";

class Notification extends React.PureComponent {
  onClick = item => {
    this.props.onClick(item);
  };

  render() {
    const { reports } = this.props;
    return (
      <li className="dropdown notifications-menu">
        <a href="#" className="dropdown-toggle" data-toggle="dropdown">
          <i className="fa fa-bell-o" />
          {reports.length > 0 && (
            <span className="label label-warning">{reports.length}</span>
          )}
        </a>
        <ul className="dropdown-menu">
          <li className="header">
            Bạn có <span style={{ color: "tomato" }}>{reports.length}</span>{" "}
            thông báo
          </li>
          {reports.length > 0 && (
            <>
              <li>
                <ul className="menu">
                  {reports.map((item, index) => {
                    return (
                      <li
                        key={index}
                        title={item.message}
                        onClick={this.onClick.bind(this, item)}
                      >
                        <a>
                          <i className="fa fa-wechat text-red" /> {item.message}
                        </a>
                      </li>
                    );
                  })}
                </ul>
              </li>
            </>
          )}
          <li className="footer">
            <Link to={`/app/notifications`}>Xem tất cả</Link>
          </li>
        </ul>
      </li>
    );
  }
}

export default Notification;
