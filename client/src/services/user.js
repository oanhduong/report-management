import { NetworkHelper, Constants } from "common";

export const getUsers = async (page, filters) => {
  try {
    let token = localStorage.getItem("token");
    let url = "/users";
    if (page != -1) {
      url += "?index=" + page;
      url += "&count=" + Constants.Limit;
    }
    if (filters) {
      Object.keys(filters).forEach(item => {
        filters[item].forEach(value => {
          url += "&" + item + "=" + value;
        });
      });
    }
    let response = await NetworkHelper.requestGet(url, token);
    return response;
  } catch (error) {
    throw error;
  }
};

export const getWorkPlaces = async () => {
  try {
    let token = localStorage.getItem("token");
    let response = await NetworkHelper.requestGet("/workPlaces", token);
    return response;
  } catch (error) {
    throw error;
  }
};

export const getUsersByRole = async role => {
  try {
    let token = localStorage.getItem("token");
    let response = await NetworkHelper.requestGet("/users?role=" + role, token);
    return response;
  } catch (error) {
    throw error;
  }
};

export const createUser = async user => {
  try {
    let data = Object.assign({}, user);
    delete data._id;
    let token = localStorage.getItem("token");
    return await NetworkHelper.requestPost("/users/register", data, token);
  } catch (error) {
    throw error;
  }
};

export const updateUser = async user => {
  try {
    let token = localStorage.getItem("token");
    let data = Object.assign({}, user);
    let id = user._id;
    delete data.password;
    delete data.username;
    delete data.active;
    delete data.updatedAt;
    delete data.createdAt;
    delete data._id;
	delete data.messages;
    return await NetworkHelper.requestPut("/users/" + id, data, token);
  } catch (error) {
    throw error;
  }
};

export const deleteUser = async id => {
  try {
    let token = localStorage.getItem("token");
    let url = "/users/" + id;
    return await NetworkHelper.requestDelete(url, null, token);
  } catch (error) {
    throw error;
  }
};

export const getUserForApprove = async () => {
  try {
    let token = localStorage.getItem("token");
    let response = await NetworkHelper.requestGet(
      `/users?role=${Constants.Role.TPB}&role=${Constants.Role.TP}&role=${Constants.Role.GDDV}`,
      token
    );
    return response;
  } catch (error) {
    throw error;
  }
};

export const getUser = async userId => {
  try {
    let token = localStorage.getItem("token");
    let response = await NetworkHelper.requestGet("/users/" + userId, token);
    return response;
  } catch (error) {
    throw error;
  }
};

export const changePassword = async (id, oldPwd, newPwd) => {
  try {
    let token = localStorage.getItem("token");
    let data = { oldPwd, newPwd };
    return await NetworkHelper.requestPut(
      "/users/changepwd/" + id,
      data,
      token
    );
  } catch (error) {
    throw error;
  }
};

export const markSeenNotification = async (userId, messages) => {
  try {
    let token = localStorage.getItem("token");
    let url = "/users/" + userId + "/markSeen";
    return await NetworkHelper.requestPut(url, messages, token);
  } catch (error) {
    throw error;
  }
};

export const getNotifications = async (userId, page) => {
  try {
    let token = localStorage.getItem("token");
    let url = "/users/" + userId + "/messages";
    if (page != -1) {
      url += "?index=" + page;
      url += "&count=" + Constants.Limit;
    }
    return await NetworkHelper.requestGet(url, token);
  } catch (error) {
    throw error;
  }
};
