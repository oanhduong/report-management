import { NetworkHelper } from "common";
import { Constants } from "common";

export const getTemplates = async (page, filter) => {
  try {
    let token = localStorage.getItem("token");
    let url = "/templates/all";
    if (page != -1) {
      url += "?index=" + page;
      url += "&count=" + Constants.Limit;
    }
    if (filter) {
      for (let i in filter) {
        if (Array.isArray(filter[i])) {
          for (let j = 0; j < filter[i].length; j++) {
            url += "&" + i + "=" + filter[i][j];
          }
        } else {
          url += "&" + i + "=" + filter[i];
        }
      }
    }
    return await NetworkHelper.requestGet(url, token);
  } catch (error) {
    throw error;
  }
};

export const updateTemplate = async (templateId, data) => {
  try {
    let token = localStorage.getItem("token");
    return await NetworkHelper.requestPut(
      "/templates/" + templateId,
      data,
      token
    );
  } catch (error) {
    throw error;
  }
};

export const deleteTemplate = async items => {
  try {
    let token = localStorage.getItem("token");
    let data = {
      list: items
    };
    return await NetworkHelper.requestDelete("/templates", data, token);
  } catch (error) {
    throw error;
  }
};

export const getTemplate = async id => {
  try {
    let token = localStorage.getItem("token");
    return await NetworkHelper.requestGet("/templates/" + id, token);
  } catch (error) {
    throw error;
  }
};

export const getReportTemplates = async () => {
  try {
    let token = localStorage.getItem("token");
    let response = await NetworkHelper.requestGet("/templates", token);
    return response;
  } catch (error) {
    throw error;
  }
};

export const getReportTemplate = async id => {
  try {
    let token = localStorage.getItem("token");
    let response = await NetworkHelper.requestGet("/templates/" + id, token);
    return response;
  } catch (error) {
    throw error;
  }
};

export const uploadTemplate = async file => {
  try {
    let token = localStorage.getItem("token");
    return await NetworkHelper.uploadFile("/upload", "template", file, token);
  } catch (error) {
    throw error;
  }
};

export const createTemplate = async data => {
  try {
    let token = localStorage.getItem("token");
    return await NetworkHelper.requestPost("/templates", data, token);
  } catch (error) {
    throw error;
  }
};
