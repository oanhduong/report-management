import { NetworkHelper, Constants } from "common";

export const createReportNormal = async data => {
  try {
    let token = localStorage.getItem("token");
    let response = await NetworkHelper.requestPost(
      "/reports/normal",
      data,
      token
    );
    return response;
  } catch (error) {
    throw error;
  }
};

export const updateReportNormal = async (id, data) => {
  try {
    let token = localStorage.getItem("token");
    let response = await NetworkHelper.requestPut(
      "/reports/normal/" + id,
      data,
      token
    );
    return response;
  } catch (error) {
    throw error;
  }
};

export const getReportsNormal = async (status, page, name, dateRange) => {
  try {
    let token = localStorage.getItem("token");
    let url = "/reports/normal?status=" + status;
    if (name) {
      url += "&name=" + name;
    }
    if (dateRange && dateRange.startDate && dateRange.endDate) {
      url += "&startCreate=" + dateRange.startDate;
      url += "&endCreate=" + dateRange.endDate;
    }
    if (page != -1) {
      url += "&index=" + page;
      url += "&count=" + Constants.Limit;
    }
    return await NetworkHelper.requestGet(url, token);
  } catch (error) {
    throw error;
  }
};

export const getReportsNormalForUnit = async (page, name) => {
  try {
    let token = localStorage.getItem("token");
    let url =
      "/reports/normal?status=" +
      Constants.Status.NeedReview +
      "&status=" +
      Constants.Status.Updating;
    if (name) {
      url += "&name=" + name;
    }
    if (page != -1) {
      url += "&index=" + page;
      url += "&count=" + Constants.Limit;
    }
    return await NetworkHelper.requestGet(url, token);
  } catch (error) {
    throw error;
  }
};

export const getReportNormalDetail = async id => {
  try {
    let token = localStorage.getItem("token");
    let url = "/reports/normal/" + id;
    return await NetworkHelper.requestGet(url, token);
  } catch (error) {
    throw error;
  }
};

export const sendReport = async id => {
  try {
    let token = localStorage.getItem("token");
    let url = "/reports/normal/" + id + "/send";
    return await NetworkHelper.requestPost(url, null, token);
  } catch (error) {
    throw error;
  }
};

export const rejectReport = async id => {
  try {
    let token = localStorage.getItem("token");
    let url = "/reports/normal/" + id + "/reject";
    return await NetworkHelper.requestPost(url, null, token);
  } catch (error) {
    throw error;
  }
};

export const deleteReport = async ids => {
  try {
    let token = localStorage.getItem("token");
    let data = {
      list: ids
    };
    let url = "/reports/normal";
    return await NetworkHelper.requestDelete(url, data, token);
  } catch (error) {
    throw error;
  }
};

export const createReportSummary = async data => {
  try {
    let token = localStorage.getItem("token");
    let url = "/reports/summary";
    return await NetworkHelper.requestPost(url, data, token);
  } catch (error) {
    throw error;
  }
};

export const getReportSummary = async (status, page, name, dateRange) => {
  try {
    let token = localStorage.getItem("token");
    let url = "/reports/summary?status=" + status;
    if (name) {
      url += "&name=" + name;
    }
    if (dateRange && dateRange.startDate && dateRange.endDate) {
      url += "&startCreate=" + dateRange.startDate;
      url += "&endCreate=" + dateRange.endDate;
    }
    if (page != -1) {
      url += "&index=" + page;
      url += "&count=" + Constants.Limit;
    }
    return await NetworkHelper.requestGet(url, token);
  } catch (error) {
    throw error;
  }
};

export const getReportSummaryDetail = async id => {
  try {
    let token = localStorage.getItem("token");
    let url = "/reports/summary/" + id;
    return await NetworkHelper.requestGet(url, token);
  } catch (error) {
    throw error;
  }
};

export const updateReportSummary = async (id, data) => {
  try {
    let token = localStorage.getItem("token");
    let response = await NetworkHelper.requestPut(
      "/reports/summary/" + id,
      data,
      token
    );
    return response;
  } catch (error) {
    throw error;
  }
};

export const sendReportSummary = async (id, chidao) => {
  try {
    let token = localStorage.getItem("token");
    let url = "/reports/summary/" + id + "/send";
    let data = null;
    if (chidao) {
      data = chidao;
    }
    return await NetworkHelper.requestPost(url, data, token);
  } catch (error) {
    throw error;
  }
};

export const rejectReportSummary = async id => {
  try {
    let token = localStorage.getItem("token");
    let url = "/reports/summary/" + id + "/reject";
    return await NetworkHelper.requestPost(url, null, token);
  } catch (error) {
    throw error;
  }
};

export const getAllQuyTrinh = async () => {
  try {
    let token = localStorage.getItem("token");
    return await NetworkHelper.requestGet("/quytrinh", token);
  } catch (error) {
    throw error;
  }
};

export const getAllReportSummary = async page => {
  try {
    let token = localStorage.getItem("token");
    let url = "/reports/summary";
    if (page != -1) {
      url += "?index=" + page;
      url += "&count=" + Constants.Limit;
    }
    return await NetworkHelper.requestGet(url, token);
  } catch (error) {
    throw error;
  }
};

export const getReportsUnit = async page => {
  try {
    let token = localStorage.getItem("token");
    let url = "/reports/normal";
    if (page != -1) {
      url += "?index=" + page;
      url += "&count=" + Constants.Limit;
    }
    return await NetworkHelper.requestGet(url, token);
  } catch (error) {
    throw error;
  }
};

export const uploadAttachments = async files => {
  try {
    let token = localStorage.getItem("token");
    return await NetworkHelper.uploadFiles("/upload", files, token);
  } catch (err) {
    throw err;
  }
};

export const downloadReportNormal = (reportId, fileName, type) => {
  try {
    let token = localStorage.getItem("token");
    let uri = "/download/normal/" + reportId + "?type=" + type;
    NetworkHelper.downloadFile(uri, fileName, token);
  } catch (error) {
    throw error;
  }
};

export const downloadReportSummary = (reportId, fileName, type) => {
  try {
    let token = localStorage.getItem("token");
    let uri = "/download/summary/" + reportId + "?type=" + type;
    NetworkHelper.downloadFile(uri, fileName, token);
  } catch (error) {
    throw error;
  }
};

export const getReportHistory = async reportId => {
  try {
    let token = localStorage.getItem("token");
    let url = "/events/" + reportId;
    return await NetworkHelper.requestGet(url, token);
  } catch (error) {
    throw error;
  }
};

export const deleteReportSummary = async ids => {
  try {
    let token = localStorage.getItem("token");
    let data = {
      list: ids
    };
    let url = "/reports/summary";
    return await NetworkHelper.requestDelete(url, data, token);
  } catch (error) {
    throw error;
  }
};

export const getReportSummaryComplete = async (page, dateRange, name) => {
  try {
    let token = localStorage.getItem("token");
    let url = "/reports/summary?isCompleted=true";
    if (page != -1) {
      url += "&index=" + page;
      url += "&count=" + Constants.Limit;
    }
    if (dateRange && dateRange.startDate && dateRange.endDate) {
      url += "&startCreate=" + dateRange.startDate;
      url += "&endCreate=" + dateRange.endDate;
    }
    if (name) {
      url += `&name=${name}`;
    }
    return await NetworkHelper.requestGet(url, token);
  } catch (error) {
    throw error;
  }
};

export const getReportNormalComplete = async (page, dateRange, name) => {
  try {
    let token = localStorage.getItem("token");
    let url = "/reports/normal?isCompleted=true";
    if (page != -1) {
      url += "&index=" + page;
      url += "&count=" + Constants.Limit;
    }
    if (dateRange && dateRange.startDate && dateRange.endDate) {
      url += "&startCreate=" + dateRange.startDate;
      url += "&endCreate=" + dateRange.endDate;
    }
    if (name) {
      url += `&name=${name}`;
    }
    return await NetworkHelper.requestGet(url, token);
  } catch (error) {
    throw error;
  }
};

export const getStatistics = async (page, filters, dateRange) => {
  try {
    let token = localStorage.getItem("token");
    let url = "/thongke?";
    if (page != -1) {
      url += "index=" + page;
      url += "&count=" + Constants.Limit;
    }
    if (dateRange && dateRange.startDate && dateRange.endDate) {
      url += "&startCreate=" + dateRange.startDate;
      url += "&endCreate=" + dateRange.endDate;
    }
    if (filters) {
      Object.keys(filters).forEach(item => {
        filters[item].forEach(value => {
          url += "&" + item + "=" + value;
        });
      });
    }
    return await NetworkHelper.requestGet(url, token);
  } catch (error) {
    throw error;
  }
};
