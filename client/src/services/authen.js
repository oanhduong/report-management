import { NetworkHelper, Constants } from "common";

export const isSignIned = () => {
  let token = localStorage.getItem("token");
  let role = localStorage.getItem("role");
  if (token && token.length > 0) {
    let url = "";
    switch (role) {
      case Constants.Role.NV:
        url = "/app/reportSent";
        break;
      case Constants.Role.TP:
      case Constants.Role.TPB:
      case Constants.Role.GDDV:
        url = "/app/reportNew";
        break;
      case Constants.Role.NVKT:
        url = "/app/reportUnit";
        break;
      case Constants.Role.TPKT:
      case Constants.Role.GD:
        url = "/app/reportSummaryNew";
        break;
      case Constants.Role.ADMIN:
        url = "/app/templates";
        break;
      default:
        url = "/app/reportSent";
        break;
    }
    return url;
  } else {
    return false;
  }
};

export const login = async data => {
  try {
    let response = await NetworkHelper.requestPost("/login", data);
    return response;
  } catch (err) {
    throw err;
  }
};
