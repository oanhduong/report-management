import React from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";
import Root from "./main/Root";
import "antd/dist/antd.css";
// import style
import "./libs/bootstrap/css/bootstrap.min.css";
import "./libs/font-awesome/css/font-awesome.min.css";
import "./libs/Ionicons/css/ionicons.min.css";
import "./libs/AdminLTE/css/AdminLTE.min.css";
import "./libs/AdminLTE/css/skins/_all-skins.min.css";
import "./common/styles.css";

require("babel-core/register");
require("babel-polyfill");

ReactDOM.render(<Root />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
