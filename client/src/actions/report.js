import * as ActionTypes from "./ActionTypes";
import * as Services from "services";

export const getReportTemplates = () => {
  return (dispatch, getState) => {
    dispatch({ type: ActionTypes.GET_REPORT_TEMPLATES_PENDING });
    Services.getReportTemplates()
      .then(response => {
        dispatch({
          type: ActionTypes.GET_REPORT_TEMPLATES_SUCCESS,
          templates: response
        });
      })
      .catch(message => {
        dispatch({ type: ActionTypes.GET_REPORT_TEMPLATES_ERROR, message });
      });
  };
};

export const cleanTemplates = () => {
  return { type: ActionTypes.CLEAN_TEMPLATE };
};
