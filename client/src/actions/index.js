import * as AuthenCreators from "./auth";
import * as ReportCreators from "./report";
import * as UserCreators from "./user";

export const ActionCreators = Object.assign(
  {},
  AuthenCreators,
  ReportCreators,
  UserCreators
);
