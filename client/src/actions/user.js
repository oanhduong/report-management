import * as Services from "services";
import * as ActionTypes from "./ActionTypes";

export const getUsers = () => {
  return (dispatch, getState) => {
    dispatch({ type: ActionTypes.GET_USERS_PENDING });
    Services.getUsers()
      .then(response => {
        dispatch({ type: ActionTypes.GET_USERS_SUCCESS, users: response });
      })
      .catch(message => {
        dispatch({ type: ActionTypes.GET_USERS_FAIL, message });
      });
  };
};

export const getWorkPlaces = () => {
  return (dispatch, getState) => {
    dispatch({ type: ActionTypes.GET_WORKPLACES_PENDING });
    Services.getWorkPlaces()
      .then(response => {
        dispatch({
          type: ActionTypes.GET_WORKPLACES_SUCCESS,
          workplaces: response
        });
      })
      .catch(message => {
        dispatch({ type: ActionTypes.GET_WORKPLACES_FAIL, message });
      });
  };
};

export const updateUser = user => {
  return { type: ActionTypes.UPDATE_USER, user };
};
