import * as ActionTypes from "./ActionTypes";
import * as Services from "services";

export const login = data => {
  return (dispatch, getState) => {
    dispatch({ type: ActionTypes.SIGNIN_PENDING });
    Services.login(data)
      .then(response => {
        if (response.user && response.token) {
          localStorage.setItem("token", response.token);
          localStorage.setItem("role", response.user.role);
          dispatch({ type: ActionTypes.SIGNIN_SUCCESS, user: response.user });
        } else {
          dispatch({
            type: ActionTypes.SIGNIN_FAIL,
            message: "Tài khoản hoặc mật khấu không đúng!"
          });
        }
      })
      .catch(err => {
        dispatch({
          type: ActionTypes.SIGNIN_FAIL,
          message: "Tài khoản hoặc mật khấu không đúng!"
        });
      });
  };
};

export const logout = () => {
  return { type: ActionTypes.SIGN_OUT };
};
