import React from "react";
import AppRouter from "./Router";

class App extends React.PureComponent {
  render() {
    return <AppRouter />
  }
}

export default App;
