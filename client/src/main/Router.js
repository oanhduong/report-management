import React from "react";
import {
  HashRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Home from "pages/Home";
import Authen from "pages/Authen";
import * as Services from "services";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      Services.isSignIned() ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/authen/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

class AppRouter extends React.PureComponent {
  render() {
    let url = Services.isSignIned();
    let isCheck = url && url.length > 0;
    return (
      <Router>
        <Switch>
          <Route
            exact
            path="/"
            render={() =>
              isCheck ? <Redirect to={url} /> : <Redirect to="/authen" />
            }
          />
          <Route path="/authen" component={Authen} />
          <PrivateRoute path="/app" component={Home} />
        </Switch>
      </Router>
    );
  }
}

export default AppRouter;
