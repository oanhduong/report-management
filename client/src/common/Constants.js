export default {
  Role: {
    NV: "NV",
    TP: "TP",
    TPB: "TPB",
    GDDV: "GDDV",
    NVKT: "NVKT",
    TPKT: "TPKT",
    GD: "GD",
    ADMIN: "ADMIN"
  },
  RoleGD: {
    VHBD: "VHBD",
    AT: "AT"
  },
  Status: {
    Updating: "Updating",
    Sent: "Sent",
    Rejected: "Rejected",
    NeedReview: "Need-Review",
    Completed: "Completed"
  },
  Limit: 5,
  DownloadType: {
    DOCX: "docx",
    PDF: "pdf",
    XLS: "xls"
  }
};
