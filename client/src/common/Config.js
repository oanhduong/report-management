const menuNV = [
  {
    name: "Đã gửi",
    icon: "fa fa-send",
    path: "/app/reportSent"
  },
  {
    name: "Bị gửi trả",
    icon: "fa fa-ban",
    path: "/app/reportFailed"
  },
  {
    name: "Đang chỉnh sửa",
    icon: "fa fa-clone",
    path: "/app/reportDraft"
  },
  {
    name: "Báo cáo đã duyệt",
    icon: "fa fa-check-square-o",
    path: "/app/reportComplete"
  }
];
const menuManager = [
  {
    name: "Chờ phê duyệt",
    icon: "fa fa-mail-forward",
    path: "/app/reportNew"
  },
  {
    name: "Đã gửi",
    icon: "fa fa-send",
    path: "/app/reportSent"
  },
  {
    name: "Bị gửi trả",
    icon: "fa fa-ban",
    path: "/app/reportFailed"
  },
  {
    name: "Đang chỉnh sửa",
    icon: "fa fa-clone",
    path: "/app/reportDraft"
  },
  {
    name: "Báo cáo đã duyệt",
    icon: "fa fa-check-square-o",
    path: "/app/reportComplete"
  }
];

const menuNVKT = [
  {
    name: "BC đơn vị",
    icon: "fa fa-mail-forward",
    path: "/app/reportUnit"
  },
  {
    name: "BC đơn vị bị gửi trả",
    icon: "fa fa-ban",
    path: "/app/reportFailed"
  },
  {
    name: "BC tổng hợp đã gửi",
    icon: "fa fa-send",
    path: "/app/reportSummarySent"
  },
  {
    name: "BC tổng hợp đang sửa",
    icon: "fa fa-clone",
    path: "/app/reportSummaryUpdating"
  },
  {
    name: "BC tổng hợp bị gửi trả",
    icon: "fa fa-ban",
    path: "/app/reportSummaryReject"
  },
  {
    name: " Tổng hợp báo cáo",
    icon: "fa fa-map-o",
    path: "/app/reportMerge"
  }
];

const menuTPKT = [
  {
    name: "Chờ phê duyệt",
    icon: "fa fa-mail-forward",
    path: "/app/reportSummaryNew"
  },
  {
    name: "Đã gửi",
    icon: "fa fa-send",
    path: "/app/reportSummarySent"
  },
  {
    name: "Bị gửi trả",
    icon: "fa fa-ban",
    path: "/app/reportSummaryReject"
  },
  {
    name: "Đang chỉnh sửa",
    icon: "fa fa-clone",
    path: "/app/reportSummaryUpdating"
  },
  {
    name: "Báo cáo từ đơn vị",
    icon: "fa fa-clone",
    path: "/app/viewReportUnit"
  }
];

const menuGD = [
  {
    name: "Chờ phê duyệt",
    icon: "fa fa-mail-forward",
    path: "/app/reportSummaryNew"
  },
  {
    name: "Báo cáo đã duyệt",
    icon: "fa fa-check-square-o",
    path: "/app/reportSummarySent"
  },
  {
    name: "Bị gửi trả",
    icon: "fa fa-ban",
    path: "/app/reportSummaryReject"
  },
  {
    name: "Báo cáo từ đơn vị",
    icon: "fa fa-clone",
    path: "/app/viewReportUnit"
  }
];

const menuAdmin = [
  {
    name: "Quản lý template",
    icon: "fa fa-newspaper-o",
    path: "/app/templates"
  },
  {
    name: "Quản lý người dùng",
    icon: "fa fa-users",
    path: "/app/users"
  },
  {
    name: "Thống kê",
    icon: "fa fa-bar-chart",
    path: "/app/statistics"
  }
];
// const IP = "localhost";
const IP = "113.161.98.202";
const PORT = 3000;
const DOMAIN = `${IP}:${PORT}`;
export default {
  DOMAIN: DOMAIN,
  WEB_SOCKET: "http://" + DOMAIN,
  END_POINT: "http://" + DOMAIN + "/api",
  USER_MENU: {
    NV: menuNV,
    TP: menuManager,
    TPB: menuManager,
    GDDV: menuManager,
    NVKT: menuNVKT,
    TPKT: menuTPKT,
    GD: menuGD,
    ADMIN: menuAdmin
  },
  Roles: ["NV", "TP", "TPB", "GDDV", "NVKT", "TPKT", "GD", "ADMIN"],
  TemplateType: [
    {
      value: "AT",
      label: "An toàn"
    },
    {
      value: "VHBD",
      label: "Vận hành bảo dưỡng"
    }
  ],
  TemplateScope: [
    {
      value: "NoiBo",
      label: "Nội bộ"
    },
    {
      value: "BenNgoai",
      label: "Bên ngoài"
    }
  ],
  TimeType: [
    {
      value: "Thang",
      label: "Tháng"
    },
    {
      value: "Quy",
      label: "Quý"
    }
  ],
  ReportType: [
    {
      name: "Báo cáo bên ngoài",
      value: "NORMAL"
    },
    {
      name: "Báo cáo nội bộ",
      value: "SUMMARY"
    }
  ]
};
