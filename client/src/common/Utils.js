import { notification } from "antd";
import Constants from "./Constants";

export const showError = msg => {
  notification.error({
    message: "Thông báo lỗi",
    description: msg,
    placement: "bottomRight"
  });
};

export const showWarning = msg => {
  notification["warning"]({
    message: "Chú ý",
    description: msg,
    placement: "bottomRight"
  });
};

export const showSuccess = msg => {
  notification["success"]({
    message: "Thông báo",
    description: msg,
    placement: "bottomRight"
  });
};

export const findFirstDiffPos = (str1, str2) => {
  let l1 = str1.length;
  let l2 = str2.length;
  let size = l1 < l2 ? l1 : l2;
  let index = -1;
  for (let i = 0; i < size; i++) {
    if (str1.charAt(i) == str2.charAt(i)) {
      index++;
    } else {
      break;
    }
  }
  return index;
};

export const findLastIndex = (str, strMatch) => {
  let length = str.length;
  let index = length;
  for (let i = length; i > 0; i--) {
    if (str.charAt(i) == strMatch) {
      break;
    } else {
      index--;
    }
  }
  return index;
};

export const isNotEmpty = str => {
  return str && str.length > 0;
};

export const isEmpty = str => {
  return !isNotEmpty(str);
};

export const getRoleName = role => {
  switch (role) {
    case Constants.Role.ADMIN:
      return "Quản trị viên";
    case Constants.Role.GD:
      return "Giám đốc";
    case Constants.Role.GDDV:
      return "Giám đốc đơn vị";
    case Constants.Role.NV:
      return "Nhân viên";
    case Constants.Role.NVKT:
      return "Nhân viên kỹ thuật";
    case Constants.Role.TP:
      return "Trưởng phòng";
    case Constants.Role.TPB:
      return "Trưởng phòng ban";
    case Constants.Role.TPKT:
      return "Trưởng phòng kỹ thuật";
    default:
      return "Không có quyền";
  }
};

export const checkPermission = (role, status) => {
  switch (status) {
    case -1:
      //Trưởng phòng gửi trả
      return role === Constants.Role.NV;
    case 1:
      // Chờ trưởng phòng duyệt
      return role === Constants.Role.TP;
    case -2: //Giám đốc đơn vị gửi trả
    case -3: //Nhân viên phòng KTAT gửi trả
    case -4: // Trưởng phòng KTAT gửi trả
      return role === Constants.Role.NV;
    case 2: // Chờ giám đốc đơn vị duyệt
      return role === Constants.Role.GDDV;
    case 3: //Chờ phòng KTAT tổng hợp
      return role === Constants.Role.NVKT;
    case 4: // cho truong phong KT duyet
    case -5: // Giám đốc gửi trả
      return role === Constants.Role.TPKT;
    case 5: // chờ giám đốc duyệt
      return role === Constants.Role.GD;
    case 6: // Hoàn tất
    default:
      return false;
  }
};
