import Config from "./Config";
import axios from "axios";

class NetworkHelper {
  static requestPost(url, params, headers = null) {
    return NetworkHelper.requestHttp("POST", url, params, headers);
  }

  static requestGet(url, headers = null) {
    return NetworkHelper.requestHttp("GET", url, null, headers);
  }

  static requestPut(url, params, headers = null) {
    return NetworkHelper.requestHttp("PUT", url, params, headers);
  }

  static requestPatch(url, params, headers = null) {
    return NetworkHelper.requestHttp("PATCH", url, params, headers);
  }

  static requestDelete(url, params, headers = null) {
    return NetworkHelper.requestHttp("DELETE", url, params, headers);
  }

  static requestHttp(method, uri, params, headers) {
    return new Promise((resolve, reject) => {
      var options = {
        method,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      };
      if (params) {
        options.body = JSON.stringify(params);
      }
      if (headers) {
        options.headers["Authorization"] = headers;
      }
      let url = Config.END_POINT + uri;
      fetch(url, options)
        .then(response => {
          if (response.status !== 200) {
            response
              .json()
              .then(body => {
                if (body.message === "jwt expired") {
                  localStorage.clear();
                  location.reload();
                } else {
                  return reject(body.message);
                }
              })
              .catch(reject);
          } else {
            response
              .json()
              .then(body => {
                resolve(body);
              })
              .catch(error => {
                if (response.status === 200) {
                  resolve("ok");
                } else {
                  reject("Error: ", error);
                }
              });
          }
        })
        .catch(error => {
          console.log(error);
          reject("Can not connect to server");
        });
    });
  }

  static uploadFile(uri, paramName, file, token) {
    return new Promise((resolve, reject) => {
      var formData = new FormData();
      formData.append(paramName, file);
      let url = Config.END_POINT + uri;
      axios
        .post(url, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: token
          }
        })
        .then(responseJson => {
          resolve(responseJson);
        })
        .catch(error => {
          reject("Can not connect to server");
        });
    });
  }

  static uploadFiles = (uri, files, token) => {
    return new Promise((resolve, reject) => {
      var formData = new FormData();
      files.forEach((file, index) => {
        let param = "file" + (index + 1);
        formData.append(param, file);
      });
      let url = Config.END_POINT + uri;
      axios
        .post(url, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: token
          }
        })
        .then(responseJson => {
          resolve(responseJson);
        })
        .catch(error => {
          reject("Can not connect to server");
        });
    });
  };

  static downloadFile = (uri, fileName, headers) => {
    var options = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };
    if (headers) {
      options.headers["Authorization"] = headers;
    }
    let url = Config.END_POINT + uri;
    fetch(url, options)
      .then(response => response.blob())
      .then(blob => {
        var url = window.URL.createObjectURL(blob);
        var a = document.createElement("a");
        a.href = url;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();
        a.remove();
      });
  };
}

export default NetworkHelper;
