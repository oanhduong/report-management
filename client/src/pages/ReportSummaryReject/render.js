import React from "react";
import {
  Button,
  DownloadReport,
  HistoryModal,
  DateRange,
  HeaderPage
} from "components";
import moment from "moment";
import { Table, Button as AButton, Icon, Input } from "antd";
import { Link } from "react-router-dom";
import "moment/locale/vi";
moment.locale("vi");

class ReportSummaryRejectRender extends React.PureComponent {
  dateRange = null;
  searchText = "";
  getColumnSearchProps = () => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Tên báo cáo`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <AButton
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ marginRight: 8 }}
        >
          Tìm kiếm báo cáo
        </AButton>
        <AButton
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Huỷ bỏ
        </AButton>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    )
  });

  handleReset = clearFilters => {
    clearFilters();
    this.searchText = "";
  };

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.searchText = selectedKeys[0];
  };
  columns = [
    {
      title: "Tên báo cáo",
      dataIndex: "name",
      ...this.getColumnSearchProps(),
      width: "50%",
      render: (name, record) => (
        <Link to={`/app/reportSummaryDetail/${record._id}`}>{record.name}</Link>
      )
    },
    {
      title: "Trạng thái",
      width: "15%",
      render: () => {
        return "Bị gửi trả";
      }
    },
    {
      title: "Cập nhật lần cuối",
      with: "15%",
      render: (name, record) => moment(record.updatedAt).calendar()
    },
    {
      title: "Lịch sử",
      width: "5%",
      render: (name, report) => (
        <>
          <Button
            className="btn btn-info btn-xs"
            onClick={this.showHistory.bind(this, report)}
          >
            <i className="fa fa-search" /> Xem
          </Button>
        </>
      )
    },
    {
      title: "Tải xuống",
      with: "15%",
      render: (name, record) => (
        <>
          <DownloadReport report={record} isSummary={true} />
        </>
      )
    }
  ];

  handleTableChange = (pagination, filters, sorter, extra) => {
    let name = filters.name ? filters.name[0] : null;
    this.props.loadReportSummaryReject(
      pagination.current,
      name,
      this.dateRange
    );
  };

  showHistory = reportId => {
    this.refs.history.open(reportId);
  };

  onDateRange = date => {
    this.dateRange = date;
    this.props.loadReportSummaryReject(1, this.searchText, date);
  };

  render() {
    const { reports, pagination, loading } = this.props;
    return (
      <div>
        <HeaderPage title={"Báo cáo tổng hợp bị gửi trả"} />
        <section className="content v-content">
          <div className="box box-primary">
            <div className="box-header with-border">
              <h3 className="box-title">Danh sách cáo tổng hợp bị gửi trả</h3>
            </div>
            <div className="box-body">
              <DateRange onDate={this.onDateRange} />
              <Table
                columns={this.columns}
                rowKey={record => record._id}
                dataSource={reports}
                pagination={pagination}
                loading={loading}
                onChange={this.handleTableChange}
              />
            </div>
          </div>
        </section>
        <HistoryModal ref="history" />
      </div>
    );
  }
}

export default ReportSummaryRejectRender;
