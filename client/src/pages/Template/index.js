import React from "react";
import TemplateRender from "./render";
import * as Services from "services";
import { Utils, Constants } from "common";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";

class Template extends React.PureComponent {
  state = {
    process: [],
    uploading: false,
    template: null,
    loading: false,
    userNVKT: [],
    userNV: []
  };

  componentDidMount = () => {
    this.getAllQuyTrinh();
    this.getUsersByNVKT();
    this.getUsersByNV();
  };

  getAllQuyTrinh = () => {
    Services.getAllQuyTrinh()
      .then(response => {
        this.setState({ process: response.list });
      })
      .catch(error => {
        Utils.showError(error);
      });
  };

  uploadTemplate = file => {
    this.setState({ uploading: true });
    Services.uploadTemplate(file)
      .then(response => {
        this.setState({ uploading: false, template: response });
      })
      .catch(error => {
        this.setState({ uploading: false, template: null });
        Utils.showError(error);
      });
  };

  createTemplate = (data, manage) => {
    this.setState({ loading: true });
    Services.createTemplate(data)
      .then(response => {
        Services.updateTemplate(response._id, { manage })
          .then(res => {
            this.setState({ loading: false });
            Utils.showSuccess("Tạo template thành công");
            this.props.history.replace("/app/templates");
          })
          .catch(error => {
            this.setState({ loading: false });
            Utils.showError(error);
          });
      })
      .catch(error => {
        this.setState({ loading: false });
        Utils.showError(error);
      });
  };

  getUsersByNVKT = () => {
    Services.getUsersByRole(Constants.Role.NVKT)
      .then(response => {
        this.setState({ userNVKT: response.list });
      })
      .catch(error => {
        Utils.showError(error);
      });
  };

  getUsersByNV = () => {
    Services.getUsersByRole(Constants.Role.NV)
      .then(response => {
        this.setState({ userNV: response.list });
      })
      .catch(error => {
        Utils.showError(error);
      });
  };

  render() {
    const {
      process,
      uploading,
      template,
      loading,
      userNVKT,
      userNV
    } = this.state;
    const { workplaces } = this.props;
    return (
      <TemplateRender
        ref="templateRender"
        process={process}
        uploading={uploading}
        template={template}
        loading={loading}
        userNVKT={userNVKT}
        userNV={userNV}
        workplaces={workplaces}
        uploadTemplate={this.uploadTemplate}
        createTemplate={this.createTemplate}
      />
    );
  }
}

Template.defaultProps = {
  user: {},
  users: [],
  workplaces: []
};
function mapStateToProps({ usersReducers }) {
  return {
    user: usersReducers.user,
    users: usersReducers.users,
    workplaces: usersReducers.workplaces
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Template));
