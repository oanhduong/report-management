import React from "react";
import { Button, CkEditor } from "components";
import { Utils, Config, Constants } from "common";
import {
  Upload,
  Button as AButton,
  Icon,
  Select,
  Input,
  Spin,
  Collapse,
  Form,
  Row,
  Col,
  Radio,
  Tooltip
} from "antd";
import "./styles.css";
const { Option } = Select;
const { Panel } = Collapse;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 }
  }
};

const formTailLayout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 }
};
const radioStye = {
  height: "30px",
  width: "400px",
  lineHeight: "30px",
  display: "block"
};

class TemplateRender extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      processId: undefined,
      templateScope: Config.TemplateScope[0].value,
      templateType: Config.TemplateType[0].value,
      timeType: Config.TimeType[0].value,
      data: "",
      name: "",
      dataSourceNV: [],
      dataSourceNVKT: [],
      selectNV: [],
      selectNVKT: undefined,
      startDate: undefined,
      endDate: undefined,
      startMonth: undefined,
      endMonth: undefined
    };
  }

  indexNV = -1;
  indexNVKT = -1;

  onChangeFile = file => {
    this.props.uploadTemplate(file);
    return false;
  };

  onAddTemplate = () => {
    const {
      templateScope,
      templateType,
      processId,
      name,
      timeType,
      selectNV,
      selectNVKT,
      startDate,
      endDate,
      startMonth,
      endMonth
    } = this.state;
    if (!processId) {
      return Utils.showWarning("Chưa chọn quy trình ");
    }
    let data = null;
    if (this.editor) {
      data = this.editor.getData();
    }
    if (Utils.isEmpty(data)) {
      return Utils.showWarning("Chưa upload biểu mẫu");
    }
    if (Utils.isEmpty(name)) {
      return Utils.showWarning("Chưa nhập tên biễu mẫu");
    }

    if (!selectNV || selectNV.length === 0) {
      return Utils.showWarning("Chọn nhân viên khởi tạo ");
    }

    if (
      Utils.isEmpty(selectNVKT) &&
      templateScope === Config.TemplateScope[0].value
    ) {
      return Utils.showWarning("Chọn nhân viên tổng hợp ");
    }
    if (!startDate || !endDate) {
      return Utils.showWarning("Nhập ngày hết hạn báo cáo");
    }
    if (timeType === Config.TimeType[1].value) {
      if (!startMonth || !endMonth) {
        return Utils.showWarning("Nhập tháng hết hạn báo cáo");
      }
    }

    let manage = {
      nv: selectNV.map(item => {
        return {
          userId: item.key
        };
      })
    };
    if (templateScope === Config.TemplateScope[0].value) {
      manage.nvkt = selectNVKT;
    }
    let deadline = {
      startDate: parseInt(startDate),
      endDate: parseInt(endDate)
    };
    if (timeType === Config.TimeType[1].value) {
      deadline.startMonth = parseInt(startMonth);
      deadline.endMonth = parseInt(endMonth);
    }
    let obj = {
      name,
      data,
      scope: templateScope,
      type: templateType,
      quytrinh: processId,
      timeType,
      deadline
    };
    this.props.createTemplate(obj, manage);
  };

  onChangeTemplateScope = e => {
    if (e.target.value === Config.TemplateScope[1].value) {
      this.setState({ selectNVKT: null, templateScope: e.target.value });
    } else {
      this.setState({ templateScope: e.target.value });
    }
  };

  onChangeTemplateType = e => {
    this.setState({ templateType: e.target.value });
  };

  onChangeTimeType = e => {
    this.setState({ timeType: e.target.value });
  };

  onChangeProcess = value => {
    this.setState({ processId: value });
  };

  onChangeName = e => {
    this.setState({ name: e.target.value });
  };

  initEditor = editor => {
    this.editor = editor;
    this.editor.setData(this.props.template.data);
  };

  _searchAutoComplete = (value, role) => {
    let user = [];
    let users = [];
    if (role == Constants.Role.NV) {
      users = this.props.userNV;
    } else if (role == Constants.Role.NVKT) {
      users = this.props.userNVKT;
    }
    user = _.filter(users, item => {
      let indexLastName = item.lastName ? item.lastName.indexOf(value) : -1;
      let indexFirstName = item.firstName ? item.firstName.indexOf(value) : -1;
      return indexLastName >= 0 || indexFirstName >= 0;
    });
    return user;
  };

  handleSearchNV = value => {
    this.indexNV = -1;
    let search = this._searchAutoComplete(value, Constants.Role.NV);
    if (search) {
      this.setState({
        dataSourceNV: search
      });
    }
  };

  onFocusNV = () => {
    this.handleSearchNV("");
  };

  onFocusNVKT = () => {
    this.handleSearchNVKT("");
  };

  handleSearchNVKT = value => {
    this.indexNVKT = -1;
    let search = this._searchAutoComplete(value, Constants.Role.NVKT);
    if (search && search.length > 0) {
      this.setState({
        dataSourceNVKT: search
      });
    } else {
      const { userNVKT } = this.props;
      this.setState({
        dataSourceNVKT: userNVKT
      });
    }
  };

  onChangeNV = items => {
    this.setState({ selectNV: items });
  };

  onChangeNVKT = items => {
    this.setState({ selectNVKT: items });
  };

  onChangeStartDate = value => {
    this.setState({ startDate: value });
  };

  onChangeEndDate = value => {
    this.setState({ endDate: value });
  };

  onChangeStartMonth = value => {
    this.setState({ startMonth: value });
  };

  onChangeEndMonth = value => {
    this.setState({ endMonth: value });
  };

  render() {
    const { process, uploading, loading, template, userNV } = this.props;
    const {
      processId,
      templateScope,
      templateType,
      name,
      timeType,
      dataSourceNV,
      dataSourceNVKT,
      selectNV,
      selectNVKT,
      startDate,
      endDate,
      startMonth,
      endMonth
    } = this.state;
    return (
      <div className="template-container">
        <div className="detail-content template-content">
          <section className="content-header" />
          <Button
            className="report-row btn btn-success"
            onClick={this.onAddTemplate}
            loading={loading}
          >
            <Icon
              type="save"
              theme="outlined"
              style={{ color: "white", fontSize: "24px" }}
            />
            &nbsp; Save
          </Button>
        </div>
        <div className="detail-content template-content">
          <section className="content-header" />
          <Collapse defaultActiveKey={["1", "2"]} bordered={false}>
            <Panel header="Thiết lập" key="1">
              <Form {...formItemLayout} labelAlign={"right"}>
                <Form.Item label="Tên biểu mẫu">
                  <Input
                    value={name}
                    placeholder="Tên biểu mẫu..."
                    onChange={this.onChangeName}
                  />
                </Form.Item>
                <Form.Item label="Quy trình">
                  {process && process.length > 0 && (
                    <Select
                      placeholder="Quy trình"
                      value={processId}
                      onChange={this.onChangeProcess}
                    >
                      {process.map((item, index) => {
                        return (
                          <Option key={index} value={item._id}>
                            {item.name}
                          </Option>
                        );
                      })}
                    </Select>
                  )}
                </Form.Item>
                <Row gutter={16}>
                  <Col span={12}>
                    <Form.Item label="Nhân viên khởi tạo" {...formTailLayout}>
                      <Select
                        mode="multiple"
                        labelInValue
                        value={selectNV}
                        placeholder="Chọn nhân viên khởi tạo"
                        onChange={this.onChangeNV}
                        onSearch={this.handleSearchNV}
                        defaultActiveFirstOption={false}
                        filterOption={false}
                        notFoundContent={null}
                        onFocus={this.onFocusNV}
                      >
                        {dataSourceNV.map((item, index) => {
                          return (
                            <Option key={item._id}>
                              {item.firstName} {item.lastName} -{" "}
                              {item.workPlace.name}
                            </Option>
                          );
                        })}
                      </Select>
                    </Form.Item>
                    <Form.Item label="Nhân viên tổng hợp" {...formTailLayout}>
                      <Select
                        showSearch
                        value={selectNVKT}
                        placeholder="Chọn nhân viên tổng hợp"
                        onChange={this.onChangeNVKT}
                        onSearch={this.handleSearchNVKT}
                        defaultActiveFirstOption={false}
                        showArrow={false}
                        notFoundContent={null}
                        filterOption={false}
                        onFocus={this.onFocusNVKT}
                        disabled={
                          templateScope === Config.TemplateScope[1].value
                        }
                      >
                        {dataSourceNVKT.map((item, index) => {
                          return (
                            <Option key={item._id}>
                              {item.firstName} {item.lastName} -{" "}
                              {item.workPlace.name}
                            </Option>
                          );
                        })}
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col span={10}>
                    <fieldset>
                      <legend>Phân loại biểu mẫu:</legend>
                      <Radio.Group
                        style={radioStye}
                        options={Config.TemplateScope}
                        value={templateScope}
                        onChange={this.onChangeTemplateScope}
                      />
                      <Radio.Group
                        style={radioStye}
                        options={Config.TemplateType}
                        value={templateType}
                        onChange={this.onChangeTemplateType}
                      />
                      <Radio.Group
                        style={radioStye}
                        options={Config.TimeType}
                        value={timeType}
                        onChange={this.onChangeTimeType}
                      />
                    </fieldset>
                  </Col>
                </Row>
                <Row gutter={16}>
                  <Col span={12}>
                    <Form.Item label="Thời hạn báo cáo" {...formTailLayout}>
                      <Tooltip placement="top" title={'Ngày bắt đầu tạo báo cáo trong tháng'}>
                        <Select
                          style={{ width: "49%" }}
                          value={startDate}
                          placeholder={"Ngày bắt đầu"}
                          onChange={this.onChangeStartDate}
                        >
                          {[...Array(28)].map((item, index) => {
                            if (endDate) {
                              let disabled = index + 1 > endDate;
                              return (
                                <Option key={index + 1} disabled={disabled}>
                                  {index + 1}
                                </Option>
                              );
                            } else {
                              return <Option key={index + 1}>{index + 1}</Option>;
                            }
                          })}
                        </Select>
                      </Tooltip>
                      &nbsp;
                      <Tooltip placement="top" title={'Ngày kết thúc tạo báo cáo trong tháng'}>
                        <Select
                          style={{ width: "49%" }}
                          value={endDate}
                          placeholder={"Ngày kết thúc"}
                          onChange={this.onChangeEndDate}
                        >
                          {[...Array(28)].map((item, index) => {
                            if (startDate) {
                              let disabled = index + 1 < startDate;
                              return (
                                <Option key={index + 1} disabled={disabled}>
                                  {index + 1}
                                </Option>
                              );
                            } else {
                              return <Option key={index + 1}>{index + 1}</Option>;
                            }
                          })}
                        </Select>
                      </Tooltip>
                    </Form.Item>
                  </Col>
                  {timeType === Config.TimeType[1].value && (
                    <Col span={12}>
                      <Form.Item>
                        <Tooltip placement="top" title={'Tháng thứ mấy trong quý bắt đầu tạo báo cáo'}>
                          <Select
                            style={{ width: "49%" }}
                            value={startMonth}
                            placeholder={"Tháng bắt đầu"}
                            onChange={this.onChangeStartMonth}
                          >
                            {[...Array(3)].map((item, index) => {
                              if (endMonth) {
                                let disabled = index + 1 > endMonth;
                                return (
                                  <Option key={index + 1} disabled={disabled}>
                                    {index + 1}
                                  </Option>
                                );
                              } else {
                                return (
                                  <Option key={index + 1}>{index + 1}</Option>
                                );
                              }
                            })}
                          </Select>
                        </Tooltip>
                        &nbsp;
                        <Tooltip placement="top" title={'Tháng thứ mấy trong quý kết thúc tạo báo cáo'}>
                          <Select
                            style={{ width: "49%" }}
                            value={endMonth}
                            placeholder={"Tháng kết thúc"}
                            onChange={this.onChangeEndMonth}
                          >
                            {[...Array(3)].map((item, index) => {
                              if (startMonth) {
                                let disabled = index + 1 < startMonth;
                                return (
                                  <Option key={index + 1} disabled={disabled}>
                                    {index + 1}
                                  </Option>
                                );
                              } else {
                                return (
                                  <Option key={index + 1}>{index + 1}</Option>
                                );
                              }
                            })}
                          </Select>
                        </Tooltip>
                      </Form.Item>
                    </Col>
                  )}
                </Row>
              </Form>
            </Panel>
            <Panel header="Nội dung biểu mẫu" key="2">
              <Form.Item>
                <Upload
                  beforeUpload={this.onChangeFile}
                  multiple={false}
                  fileList={[]}
                  showUploadList={{
                    showPreviewIcon: false,
                    showRemoveIcon: false
                  }}
                  className={"report-row"}
                >
                  <AButton>
                    <Icon type="upload" />
                    &nbsp; Tải lên
                  </AButton>
                </Upload>
              </Form.Item>
              {uploading && (
                <div style={{ textAlign: "center" }}>
                  <Spin
                    tip="Biểu mẫu đang được tải lên..."
                    indicator={
                      <Icon type="loading" style={{ fontSize: 40 }} spin />
                    }
                  />
                </div>
              )}
              {template && !uploading && (
                <CkEditor
                  id={"template"}
                  onInitEditor={this.initEditor}
                  config={{
                    allowedContent: true,
                    height: 500,
                    basicEntities: false,
                    entities_latin: false,
                    readOnly: true,
                    removePlugins:
                      "sourcearea, a11yhelp,about,basicstyles,blockquote,clipboard,contextmenu,elementspath,enterkey,entities,filebrowser,floatingspace,format,horizontalrule,htmlwriter,image,indentlist,link,list, magicline,maximize,pastefromword,pastetext,removeformat,resize,scayt,showborders,sourcearea, specialchar,stylescombo,tab,table,tableselection,tabletools,toolbar,undo,uploadimage,wsc",
                    removeButtons:
                      "Copy,Cut,Paste,Undo,Redo,Print,Form,TextField,Textarea,Button,SelectAll,NumberedList,BulletedList,Indent,Outdent,CreateDiv,Table,PasteText,PasteFromWord,Select,HiddenField"
                  }}
                />
              )}
            </Panel>
          </Collapse>
        </div>
      </div>
    );
  }
}

export default TemplateRender;
