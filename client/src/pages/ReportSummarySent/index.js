import React from "react";
import ReportSummarySentRender from "./render";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { Constants, Utils } from "common";
import * as Services from "services";

class ReportSummarySent extends React.PureComponent {
  state = {
    loading: false,
    pagination: {
      pageSize: Constants.Limit,
      current: 1,
      total: 0
    },
    reports: []
  };

  componentDidMount = () => {
    this.loadReportSummarySent(1);
  };

  loadReportSummarySent = (page, name, date) => {
    this.setState({ loading: true, reports: [] });
    let status = Constants.Status.Sent;
    if (this.props.user.role === "GD") {
      status = Constants.Status.Completed;
    }
    Services.getReportSummary(status, page, name, date)
      .then(response => {
        let pagination = { ...this.state.pagination };
        pagination.current = page;
        pagination.total = response.total;
        this.setState({ loading: false, reports: response.list, pagination });
      })
      .catch(error => {
        this.setState({ loading: false, reports: [] });
        Utils.showError(error);
      });
  };

  render() {
    const { user } = this.props;
    const { pagination, reports, loading } = this.state;
    return (
      <ReportSummarySentRender
        reports={reports}
        user={user}
        pagination={pagination}
        loading={loading}
        loadReportSummarySent={this.loadReportSummarySent}
      />
    );
  }
}

function mapStateToProps({ reportReducers, usersReducers }) {
  return {
    user: usersReducers.user
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ReportSummarySent));
