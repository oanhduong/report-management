import React from "react";
import ReportFailedRender from "./render";
import { withRouter } from "react-router-dom";
import { Constants } from "common";
import * as Services from 'services'

class ReportFailed extends React.PureComponent {
  state = {
    loading: false,
    pagination: {
      pageSize: Constants.Limit,
      current: 1,
      total: 0
    },
    reports: []
  };

  componentDidMount = () => {
    this.loadReportReject(1);
  };

  loadReportReject = (page, name, date) => {
    this.setState({ loading: true, reports: [] });
    Services.getReportsNormal(Constants.Status.Rejected, page, name, date)
      .then(response => {
        let pagination = { ...this.state.pagination };
        pagination.current = page;
        pagination.total = response.total;
        this.setState({ loading: false, reports: response.list, pagination });
      })
      .catch(error => {
        this.setState({ loading: false, reports: [] });
        Utils.showError(error);
      });
  };

  render() {
    const { reports, pagination, loading } = this.state;
    return (
      <ReportFailedRender
        reports={reports}
        loading={loading}
        pagination={pagination}
        loadReportReject={this.loadReportReject}
      />
    );
  }
}

export default withRouter(ReportFailed);
