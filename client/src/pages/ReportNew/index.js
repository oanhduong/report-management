import React from "react";
import ReportNewRender from "./render";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { Constants, Utils } from "common";
import * as Services from "services";

class ReportNew extends React.PureComponent {
  state = {
    loading: false,
    pagination: {
      pageSize: Constants.Limit,
      current: 1,
      total: 0
    },
    reports: []
  };

  componentDidMount = () => {
    this.loadReportNew(1);
  };

  loadReportNew = (page, name, date) => {
    this.setState({ loading: true, reports: [] });
    Services.getReportsNormal(Constants.Status.NeedReview, page, name, date)
      .then(response => {
        let pagination = { ...this.state.pagination };
        pagination.current = page;
        pagination.total = response.total;
        this.setState({ loading: false, reports: response.list, pagination });
      })
      .catch(error => {
        this.setState({ loading: false, reports: [] });
        Utils.showError(error);
      });
  };

  render() {
    const { user } = this.props;
    const { pagination, reports, loading } = this.state;
    return (
      <ReportNewRender
        reports={reports}
        user={user}
        loading={loading}
        pagination={pagination}
        loadReportNew={this.loadReportNew}
      />
    );
  }
}

ReportNew.defaultProps = {
  reports: [],
  user: {}
};

function mapStateToProps({ reportReducers, usersReducers }) {
  return {
    type: reportReducers.type,
    reports: reportReducers.reports,
    user: usersReducers.user
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ReportNew));
