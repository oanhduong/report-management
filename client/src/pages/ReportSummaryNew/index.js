import React from "react";
import ReportSummaryNewRender from "./render";
import { Constants, Utils } from "common";
import * as Services from "services";

class ReportSummaryNew extends React.PureComponent {
  state = {
    loading: false,
    pagination: {
      pageSize: Constants.Limit,
      current: 1,
      total: 0
    },
    reports: []
  };

  componentDidMount = () => {
    this.loadReportSummaryNew(1);
  };

  loadReportSummaryNew = (page, name) => {
    this.setState({ loading: true, reports: [] });
    Services.getReportSummary(Constants.Status.NeedReview, page, name)
      .then(response => {
        let pagination = { ...this.state.pagination };
        pagination.current = page;
        pagination.total = response.total;
        this.setState({ loading: false, reports: response.list, pagination });
      })
      .catch(error => {
        this.setState({ loading: false, reports: [] });
        Utils.showError(error);
      });
  };

  render() {
    const { pagination, reports, loading } = this.state;
    return (
      <ReportSummaryNewRender
        reports={reports}
        loading={loading}
        pagination={pagination}
        loadReportSummaryNew={this.loadReportSummaryNew}
      />
    );
  }
}

export default ReportSummaryNew;
