import React from "react";
import TemplatesRender from "./render";
import { withRouter } from "react-router-dom";

import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";
import * as ActionTypes from "actions/ActionTypes";

import * as Services from "services";
import { Constants, Utils } from "common";

class Templates extends React.PureComponent {
  state = {
    pagination: {
      pageSize: Constants.Limit,
      current: 1,
      total: 0,
      deleting: false
    },
    templates: [],
    loading: false
  };
  onCreateTemplate = () => {
    this.props.history.push("/app/template");
  };

  getTemplates = (page, filter) => {
    this.setState({ loading: true });
    Services.getTemplates(page, filter)
      .then(response => {
        let pagination = { ...this.state.pagination };
        pagination.current = page;
        pagination.total = response.total;
        this.setState({ loading: false, templates: response.list, pagination });
      })
      .catch(err => {
        this.setState({ loading: false });
        Utils.showError(err);
      });
  };

  componentDidMount = () => {
    this.getTemplates(1);
  };

  onDeleteTemplate = async items => {
    this.setState({ deleting: true });
    try {
      await Services.deleteTemplate(items);
      this.getTemplates(1);
      Utils.showSuccess("Xoá biểu mẫu thành công");
      this.setState({ deleting: false });
    } catch (error) {
      Utils.showSuccess(error);
      this.setState({ deleting: false });
    }
  };

  render() {
    const { pagination, loading, templates, deleting } = this.state;
    return (
      <TemplatesRender
        loading={loading}
        onCreateTemplate={this.onCreateTemplate}
        pagination={pagination}
        getTemplates={this.getTemplates}
        templates={templates}
        onDeleteTemplate={this.onDeleteTemplate}
        deleting={deleting}
      />
    );
  }
}

Templates.defaultProps = {
  message: false
};

function mapStateToProps({ usersReducers }) {
  return {
    type: usersReducers.type,
    message: usersReducers.message,
    user: usersReducers.user
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Templates));
