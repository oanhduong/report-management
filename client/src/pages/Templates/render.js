import React from "react";
import { Button, HeaderPage } from "components";
import {
  Table,
  Tag,
  Modal,
  Input,
  Button as AButton,
  Checkbox,
  Row,
  Col
} from "antd";
import moment from "moment";
import { Link } from "react-router-dom";
import { Config, Utils } from "common";
import { findIndex } from "lodash";

const FilterType = {
  Name: "NAME",
  Process: "PROCESS",
  Type: "TYPE"
};

class TemplatesRender extends React.PureComponent {
  constructor(props) {
    super(props);
    let filterChecked = [];
    filterChecked = filterChecked.concat(
      Config.TemplateScope.map(item => item.value)
    );
    filterChecked = filterChecked.concat(
      Config.TemplateType.map(item => item.value)
    );
    filterChecked = filterChecked.concat(
      Config.TimeType.map(item => item.value)
    );
    this.state = {
      selectedRowKeys: [],
      nameSearch: "",
      processSearch: "",
      filterChecked: filterChecked
    };
  }

  onChangeNameSearch = e => {
    this.setState({ nameSearch: e.target.value });
  };

  onChangeProcessSearch = e => {
    this.setState({ processSearch: e.target.value });
  };

  onCheckFilter = items => {
    this.setState({ filterChecked: items });
  };

  getColumnSearchNameProps = () => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.nameInput = node;
          }}
          placeholder={"Nhập tên biểu mẫu"}
          value={this.state.nameSearch}
          onChange={this.onChangeNameSearch}
          style={{ width: 250, marginBottom: 8, display: "block" }}
        />
        <AButton
          type="primary"
          size="small"
          style={{ width: 90, marginRight: 8 }}
          onClick={this.onFilter.bind(this, confirm)}
        >
          OK
        </AButton>
        <AButton
          size="small"
          style={{ width: 90 }}
          onClick={this.onCancelFilter.bind(
            this,
            clearFilters,
            FilterType.Name
          )}
        >
          Huỷ bỏ
        </AButton>
      </div>
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.nameInput.select());
      }
    }
  });

  getColumnSearchProcessProps = () => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.processInput = node;
          }}
          placeholder={"Nhập tên quy trình"}
          value={this.state.processSearch}
          onChange={this.onChangeProcessSearch}
          style={{ width: 250, marginBottom: 8, display: "block" }}
        />
        <AButton
          type="primary"
          size="small"
          style={{ width: 90, marginRight: 8 }}
          onClick={this.onFilter.bind(this, confirm)}
        >
          OK
        </AButton>
        <AButton
          size="small"
          style={{ width: 90 }}
          onClick={this.onCancelFilter.bind(
            this,
            clearFilters,
            FilterType.Process
          )}
        >
          Huỷ bỏ
        </AButton>
      </div>
    ),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.processInput.select());
      }
    }
  });

  getColumnFilterType = () => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Checkbox.Group
          style={{ width: "500px", display: "block", marginBottom: "5px" }}
          value={this.state.filterChecked}
          onChange={this.onCheckFilter}
        >
          <Row>
            {Config.TemplateType.map((item, index) => {
              return (
                <Col span={8} key={index}>
                  <Checkbox value={item.value}>{item.label}</Checkbox>
                </Col>
              );
            })}
          </Row>
          <Row>
            {Config.TemplateScope.map((item, index) => {
              return (
                <Col span={8} key={index}>
                  <Checkbox value={item.value}>{item.label}</Checkbox>
                </Col>
              );
            })}
          </Row>
          <Row>
            {Config.TimeType.map((item, index) => {
              return (
                <Col span={8} key={index}>
                  <Checkbox value={item.value}>{item.label}</Checkbox>
                </Col>
              );
            })}
            <Col span={8} />
          </Row>
        </Checkbox.Group>
        <AButton
          type="primary"
          size="small"
          style={{ width: 90, marginRight: 8 }}
          onClick={this.onFilter.bind(this, confirm)}
        >
          OK
        </AButton>
        <AButton
          size="small"
          style={{ width: 90 }}
          onClick={this.onCancelFilter.bind(
            this,
            clearFilters,
            FilterType.Type
          )}
        >
          Huỷ bỏ
        </AButton>
      </div>
    )
  });

  getFilter = () => {
    const { nameSearch, processSearch, filterChecked } = this.state;
    let filter = {};
    if (Utils.isNotEmpty(nameSearch)) {
      filter.name = nameSearch;
    }
    if (Utils.isNotEmpty(processSearch)) {
      filter.quytrinh = processSearch;
    }
    if (filterChecked && filterChecked.length > 0) {
      filter.scope = [];
      filter.type = [];
      filter.timeType = [];
      for (let i = 0; i < filterChecked.length; i++) {
        let type = filterChecked[i];
        if (findIndex(Config.TemplateScope, item => item.value === type) >= 0) {
          filter.scope.push(type);
          if (filter.scope.length === Config.TemplateScope.length) {
            filter.scope = [];
          }
          continue;
        }
        if (findIndex(Config.TemplateType, item => item.value === type) >= 0) {
          filter.type.push(type);
          if (filter.type.length === Config.TemplateType.length) {
            filter.type = [];
          }
          continue;
        }
        if (findIndex(Config.TimeType, item => item.value === type) >= 0) {
          filter.timeType.push(type);
          if (filter.timeType.length === Config.TimeType.length) {
            filter.timeType = [];
          }
          continue;
        }
      }
    }
    for (var i in filter) {
      if (filter[i].length === 0) {
        delete filter[i];
      }
    }
    return filter;
  };

  onFilter = confirm => {
    confirm();
    this.props.getTemplates(1, this.getFilter());
  };

  onCancelFilter = (clearFilters, type) => {
    clearFilters();
    if (type === FilterType.Name) {
      this.setState({ nameSearch: "" }, () => {
        this.props.getTemplates(1, this.getFilter());
      });
    } else if (type === FilterType.Process) {
      this.setState({ processSearch: "" }, () => {
        this.props.getTemplates(1, this.getFilter());
      });
    } else if (type === FilterType.Type) {
      this.setState({ filterChecked: [] }, () => {
        this.props.getTemplates(1, this.getFilter());
      });
    }
  };

  columns = [
    {
      title: "Biểu mẫu",
      dataIndex: "name",
      width: "35%",
      render: (name, record) => (
        <Link to={`/app/templateDetail/${record._id}`}>{record.name}</Link>
      ),
      ...this.getColumnSearchNameProps()
    },
    {
      title: "Quy trình",
      dataIndex: "quytrinh",
      width: "25%",
      ...this.getColumnSearchProcessProps(),
      render: (name, record) => <small>{record.quytrinh.name}</small>
    },
    {
      title: "Loại",
      dataIndex: "type",
      width: "20%",
      render: (name, record) => (
        <div>
          <Tag color="#8CB7F1">{record.scope}</Tag>{" "}
          <Tag color="#35AF3D">{record.type}</Tag>{" "}
          <Tag color="#F90FFF">{record.timeType}</Tag>
        </div>
      ),
      ...this.getColumnFilterType()
    },
    {
      title: "Cập nhật lần cuối",
      dataIndex: "updatedDate",
      with: "20%",
      render: (name, record) => {
        return <small>{moment(record.updatedAt).calendar()}</small>;
      }
    }
  ];

  handleTableChange = (pagination, filters, sorter, extra) => {
    this.props.getTemplates(pagination.current, this.getFilter());
  };

  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };

  onDeleteTemplate = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn xoá biểu mẫu không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        const { selectedRowKeys } = this.state;
        this.props.onDeleteTemplate(selectedRowKeys);
      }
    });
  };

  render() {
    const {
      onCreateTemplate,
      loading,
      templates,
      pagination,
      deleting
    } = this.props;
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    };
    return (
      <div>
        <HeaderPage title={"Quản lý biểu mẫu"} />
        <section className="content v-content">
          <div className="box box-primary">
            <div className="box-header with-border">
              <Button
                onClick={onCreateTemplate}
                icon={<i className="fa fa-file-excel-o" />}
                className="btn btn-primary report-row btn-sm"
              >
                &nbsp; Tạo mới
              </Button>
              &nbsp;
              <Button
                disabled={selectedRowKeys.length === 0}
                icon={<i className="fa fa-trash" />}
                className="btn btn-danger report-row btn-sm"
                onClick={this.onDeleteTemplate}
                loading={deleting}
              >
                &nbsp; Xoá
              </Button>
            </div>
            <div className="box-body">
              <Table
                loading={loading}
                columns={this.columns}
                rowKey={record => record._id}
                dataSource={templates}
                pagination={pagination}
                onChange={this.handleTableChange}
                rowSelection={rowSelection}
              />
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default TemplatesRender;
