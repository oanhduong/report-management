import React from "react";
import { Button, DownloadReport, HistoryModal, HeaderPage } from "components";
import moment from "moment";
import { Table, Modal, Input, Button as AButton, Icon } from "antd";
import { Link } from "react-router-dom";
import { findIndex } from "lodash";
import "moment/locale/vi";
moment.locale("vi");

class ReportUnitRender extends React.PureComponent {
  state = {
    selectedRowKeys: [],
    visible: false,
    submiting: false,
    nameReport: ""
  };
  selectRow = [];

  getColumnSearchProps = () => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Tên báo cáo`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <AButton
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ marginRight: 8 }}
        >
          Tìm kiếm báo cáo
        </AButton>
        <AButton
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Huỷ bỏ
        </AButton>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    )
  });

  handleReset = clearFilters => {
    clearFilters();
  };

  handleSearch = (selectedKeys, confirm) => {
    confirm();
  };

  onSelect = (record, selected, selectedRows) => {
    let selectedRowKeys = this.state.selectedRowKeys.slice();
    if (selected) {
      if (!selectedRowKeys.includes(record._id)) {
        selectedRowKeys.push(record._id);
        this.setState({ selectedRowKeys }, () => {
          this.props.checkReport(record);
        });
      }
    } else {
      let index = findIndex(selectedRowKeys, item => item === record._id);
      if (index >= 0) {
        selectedRowKeys.splice(index, 1);
        this.setState({ selectedRowKeys }, () => {
          this.props.uncheckReport(record);
        });
      }
    }
  };

  onSelectAll = (selected, selectedRows, changeRows) => {
    let selectedRowKeys = this.state.selectedRowKeys.slice();
    if (selected) {
      this.props.checkAllReport(selectedRows, selected => {
        let isChange = false;
        selected.forEach(element => {
          if (!selectedRowKeys.includes(element._id)) {
            selectedRowKeys.push(element._id);
            isChange = true;
          }
        });
        if (isChange) {
          this.setState({ selectedRowKeys });
        }
      });
    } else {
      this.props.unCheckAllReport(changeRows, selected => {
        selected.forEach(row => {
          let index = findIndex(selectedRowKeys, item => item === row._id);
          if (index >= 0) {
            selectedRowKeys.splice(index, 1);
          }
        });
      });
    }
    this.setState({ selectedRowKeys });
  };

  handleTableChange = (pagination, filters, sorter) => {
    let name = filters.name ? filters.name[0] : null;
    this.props.loadReportUnit(pagination.current, name);
  };

  summaryReport = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muón tổng hợp báo cáo không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.summaryReport();
      }
    });
  };

  showHistory = reportId => {
    this.refs.history.open(reportId);
  };

  render() {
    const {
      pagination,
      reports,
      loading,
      templateFilter,
      submiting,
      nameReport,
      onChangeNameReport
    } = this.props;
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      hideDefaultSelections: true,
      onSelect: this.onSelect,
      onSelectAll: this.onSelectAll,
      getCheckboxProps: record => {
        return {
          disabled: record.disabled
        };
      }
    };

    // info table
    const columns = [
      {
        title: "Tên báo cáo",
        dataIndex: "name",
        ...this.getColumnSearchProps(),
        width: "35%",
        render: (name, record) => (
          <Link to={`/app/reportDetail/${record._id}`}>{record.name}</Link>
        )
      },
      {
        title: "Mẫu báo cáo",
        width: "20%",
        dataIndex: "templateName",
        filters: templateFilter,
        filterMultiple: false,
        onFilter: (value, record) => {
          return record.template._id === value;
        },
        render: (name, record) => {
          return record.template.name;
        }
      },
      {
        title: "Cập nhật lần cuối",
        dataIndex: "updatedDate",
        with: "10%",
        render: (name, record) => moment(record.updatedAt).calendar()
      },
      {
        title: "Lịch sử",
        width: "5%",
        render: (name, report) => (
          <>
            <Button
              className="btn btn-info btn-xs"
              onClick={this.showHistory.bind(this, report)}
            >
              <i className="fa fa-search" /> Xem
            </Button>
          </>
        )
      },
      {
        title: "Tải xuống",
        with: "20%",
        render: (name, record) => (
          <>
            <DownloadReport report={record} />
          </>
        )
      }
    ];
    //end info table
    return (
      <div>
        <HeaderPage title={"Báo cáo từ đơn vị"} />
        <section className="content v-content">
          <div className="box box-primary">
            <div className="box-header with-border">
              <div className="form-group">
                <label className="pr0 pf0 mt5 col-sm-2 control-label">
                  Tên báo cáo:
                </label>
                <div className="pr0 pf0 col-sm-7">
                  <input
                    type="text"
                    className="form-control"
                    value={nameReport}
                    onChange={onChangeNameReport}
                    id="nameReport"
                    placeholder="Nhập tên báo cáo...."
                  />
                </div>
                <div className="col-sm-3 pr0">
                  <Button
                    className="btn btn-primary btn-block"
                    onClick={this.summaryReport}
                    loading={submiting}
                    icon={<i className="fa fa-newspaper-o" />}
                  >
                    &nbsp; Tổng hợp báo cáo
                  </Button>
                </div>
              </div>
            </div>
            <div className="box-body">
              <Table
                columns={columns}
                rowKey={record => record._id}
                dataSource={reports}
                pagination={pagination}
                onChange={this.handleTableChange}
                loading={loading}
                rowSelection={rowSelection}
              />
            </div>
          </div>
        </section>
        <HistoryModal ref="history" />
      </div>
    );
  }
}

export default ReportUnitRender;
