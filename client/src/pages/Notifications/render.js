import React from "react";
import { Pagination, Spin } from "antd";
import { NotificationItem, HeaderPage } from "components";
import "./styles.css";

class NotificationsRender extends React.PureComponent {
  onChangePage = page => {
    this.props.loadNotifications(page);
  };

  render() {
    const { pagination, loading, notifications } = this.props;
    return (
      <div>
        <HeaderPage title={"Thống kê"} />
        <section className="content v-content">
          <div className="box box-primary">
            <div className="box-header with-border">
              <h3 className="box-title">Danh sách thông báo </h3>
            </div>
            <div className="box-body">
              {notifications.length !== 0 && (
                <>
                  <Pagination
                    style={{ textAlign: "right" }}
                    size="small"
                    {...pagination}
                    onChange={this.onChangePage}
                  />
                  <br />
                </>
              )}
              {loading && (
                <>
                  {" "}
                  <Spin /> <span>Đang tải thông báo...</span>
                </>
              )}
              {!loading && notifications && notifications.length > 0 && (
                <ul className="todo-list">
                  {notifications.map((item, index) => {
                    return <NotificationItem item={item} key={index} />;
                  })}
                </ul>
              )}
              {!loading && notifications && notifications.length === 0 && (
                <span>Không có thông báo...</span>
              )}
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default NotificationsRender;
