import React from "react";
import NotificationsRender from "./render";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import * as Services from "services";
import { Constants, Utils } from "common";

class Notifications extends React.PureComponent {
  state = {
    loading: false,
    pagination: {
      pageSize: Constants.Limit,
      current: 1,
      total: 0
    },
    notifications: []
  };

  componentDidMount = () => {
    this.loadNotifications(1);
  };

  loadNotifications = page => {
    const { user } = this.props;
    this.setState({ loading: true });
    Services.getNotifications(user._id, page)
      .then(response => {
        let pagination = { ...this.state.pagination };
        pagination.current = page;
        pagination.total = response.total;
        this.setState({
          loading: false,
          notifications: response.list,
          pagination
        });
      })
      .catch(error => {
        this.setState({ loading: false });
        Utils.showError(error);
      });
  };

  render() {
    const { notifications, pagination, loading } = this.state;
    return (
      <NotificationsRender
        loading={loading}
        pagination={pagination}
        notifications={notifications}
        loadNotifications={this.loadNotifications}
      />
    );
  }
}

function mapStateToProps({ usersReducers }) {
  return {
    user: usersReducers.user
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Notifications));
