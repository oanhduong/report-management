import React from "react";
import {
  Button,
  DownloadReport,
  HistoryModal,
  HeaderPage,
  DateRange
} from "components";
import moment from "moment";
import { Table, Select, Input, Button as AButton, Icon } from "antd";
import { Link } from "react-router-dom";
import "moment/locale/vi";
import { Config } from "common";
const { Option } = Select;
moment.locale("vi");

class ReportCompleteRender extends React.PureComponent {

  showHistory = reportId => {
    this.refs.history.open(reportId);
  };
  dateRange = null;

  handleTableChange = (pagination, filters, sorter, extra) => {
    let name = filters.name ? filters.name[0] : null;
    this.props.loadReport(pagination.current, this.dateRange, name);
  };

  onDateRange = date => {
    this.dateRange = date;
    this.props.loadReport(1, date);
  };

  getColumnSearchProps = () => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Tên báo cáo`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <AButton
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ marginRight: 8 }}
        >
          Tìm kiếm báo cáo
        </AButton>
        <AButton
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Huỷ bỏ
        </AButton>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    )
  });

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  render() {
    const { reports, pagination, loading, scope, onChangeScope } = this.props;
    const columns = [
      {
        title: "Tên báo cáo",
        dataIndex: "name",
        ...this.getColumnSearchProps(),
        width: "40%",
        render: (name, record) => {
          let url = "";
          if (scope === Config.ReportType[0].value) {
            url = "/app/reportDetail/";
          } else {
            url = "/app/reportSummaryDetail/";
          }
          return <Link to={`${url}${record._id}`}>{record.name}</Link>;
        }
      },
      {
        title: "Cập nhật lần cuối",
        dataIndex: "updatedAt",
        with: "20%",
        render: (name, record) => {
          return moment(new Date(record.updatedAt)).calendar();
        }
      },
      {
        title: "Ngày tạo",
        dataIndex: "createdAt",
        width: "15%",
        render: (name, record) => {
          return moment(new Date(record.createdAt)).calendar();
        }
      },
      {
        title: "Lịch sử",
        with: "5%",
        render: (text, record) => (
          <>
            <Button
              className="btn btn-info btn-xs"
              onClick={this.showHistory.bind(this, record)}
            >
              <i className="fa fa-search" /> Xem
            </Button>
          </>
        )
      },
      {
        title: "Tải xuống",
        with: "15%",
        render: (name, record) => (
          <>
            <DownloadReport report={record} />
          </>
        )
      }
    ];
    return (
      <div>
        <HeaderPage title={"Báo cáo đã duyệt"} />
        <section className="content v-content">
          <div className="box box-primary">
            <div className="box-header with-border">
              <h3 className="box-title">Danh sách báo cáo đã duyệt</h3>
            </div>
            <div className="box-body">
              <Select
                style={{ width: 200 }}
                value={scope}
                onChange={onChangeScope}
              >
                {Config.ReportType.map((item, index) => {
                  return (
                    <Option key={index} value={item.value}>
                      {item.name}
                    </Option>
                  );
                })}
              </Select>
              &nbsp;
              <DateRange onDate={this.onDateRange} />
              <br />
              <Table
                columns={columns}
                rowKey={record => record._id}
                dataSource={reports}
                pagination={pagination}
                loading={loading}
                onChange={this.handleTableChange}
              />
            </div>
          </div>
        </section>
        <HistoryModal ref={"history"} />
      </div>
    );
  }
}

export default ReportCompleteRender;
