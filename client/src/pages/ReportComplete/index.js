import React from "react";
import ReportCompleteRender from "./render";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { Constants, Utils, Config } from "common";
import * as Services from "services";

class ReportComplete extends React.PureComponent {
  state = {
    scope: Config.ReportType[0].value,
    loading: false,
    pagination: {
      pageSize: Constants.Limit,
      current: 1,
      total: 0
    },
    reports: []
  };
  dateRange = null;

  onChangeScope = scope => {
    this.setState({ scope }, () => {
      this.loadReport(1, this.dateRange);
    });
  };

  componentDidMount = () => {
    this.loadReport(1);
  };

  loadReport = (page, date, name) => {
    const { scope } = this.state;
    this.dateRange = date;
    if (scope === Config.ReportType[0].value) {
      Services.getReportNormalComplete(page, date, name)
        .then(response => {
          let pagination = { ...this.state.pagination };
          pagination.current = page;
          pagination.total = response.total;
          this.setState({ loading: false, reports: response.list, pagination });
        })
        .catch(error => {
          Utils.showError(error);
        });
    } else {
      Services.getReportSummaryComplete(page, date, name)
        .then(response => {
          let pagination = { ...this.state.pagination };
          pagination.current = page;
          pagination.total = response.total;
          this.setState({ loading: false, reports: response.list, pagination });
        })
        .catch(error => {
          Utils.showError(error);
        });
    }
  };
  render() {
    const { scope, loading, reports, pagination } = this.state;
    return (
      <ReportCompleteRender
        scope={scope}
        onChangeScope={this.onChangeScope}
        loading={loading}
        reports={reports}
        pagination={pagination}
        loadReport={this.loadReport}
      />
    );
  }
}

function mapStateToProps({ reportReducers, usersReducers }) {
  return {
    user: usersReducers.user
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ReportComplete));
