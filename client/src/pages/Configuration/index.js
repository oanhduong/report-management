import React from "react";
import ConfigurationRender from "./render";

class Configuration extends React.PureComponent {
  render() {
    return <ConfigurationRender />;
  }
}

export default Configuration;
