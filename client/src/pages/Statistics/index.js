import React from "react";
import StatisticRender from "./render";
import * as Services from "services";
import { Utils, Constants } from "common";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";

class Statistics extends React.PureComponent {
  state = {
    pagination: {
      pageSize: Constants.Limit,
      current: 1,
      total: 0
    },
    data: [],
    loading: false
  };
  componentDidMount = () => {
    this.loadStatistics(1);
  };

  loadStatistics = (page, filters, date) => {
    this.setState({ loading: true });
    Services.getStatistics(page, filters, date)
      .then(response => {
        let pagination = { ...this.state.pagination };
        pagination.current = page;
        pagination.total = response.total;
        this.setState({ loading: false, data: response.list, pagination });
      })
      .catch(error => {
        this.setState({ loading: false });
        Utils.showError(error);
      });
  };

  render() {
    const { data, loading, pagination } = this.state;
    const { workplaces } = this.props;
    return (
      <StatisticRender
        workplaces={workplaces}
        data={data}
        loading={loading}
        pagination={pagination}
        loadStatistics={this.loadStatistics}
      />
    );
  }
}

Statistics.defaultProps = {
  user: {},
  workplaces: []
};
function mapStateToProps({ usersReducers }) {
  return {
    user: usersReducers.user,
    workplaces: usersReducers.workplaces
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Statistics));
