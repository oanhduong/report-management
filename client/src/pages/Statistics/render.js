import React from "react";
import { HeaderPage, StatusStatistic, DateRange } from "components";
import { Table, Button as AButton, Input, Icon } from "antd";
import moment from "moment";
import { Constants, Utils, Config } from "common";
import { Link } from "react-router-dom";

class StatisticRender extends React.PureComponent {
  dateRange = null;
  filters = null;
  handleTableChange = (pagination, filters, sorter, extra) => {
    this.filters = filters;
    this.props.loadStatistics(pagination.current, filters, this.dateRange);
  };

  onDateRange = date => {
    this.dateRange = date;
    this.props.loadStatistics(1, this.filters, this.dateRange);
  };

  getColumnSearchProps = name => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`${name}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <AButton
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ marginRight: 8 }}
        >
          Tìm kiếm
        </AButton>
        <AButton
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Huỷ bỏ
        </AButton>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    )
  });

  handleReset = clearFilters => {
    clearFilters();
  };

  handleSearch = (selectedKeys, confirm) => {
    confirm();
  };

  getTimeApprove = (report, roles) => {
    let times = [];
    for (let i = 0; i < roles.length; i++) {
      let role = roles[i];
      let time = _.filter(report.ngayDuyet, item => item.role === role);
      if (time) {
        times = times.concat(time);
      }
    }
    times = times.map(item => new Date(item.time).getTime());
    let value = undefined;
    if (roles.length === 1 && roles[0] === Constants.Role.NV) {
      value = _.min(times);
    } else {
      value = _.max(times);
    }
    if (value) {
      return <span>{moment(new Date(value)).calendar()}</span>;
    }
    return "";
  };

  renderColumn = () => {
    return [
      {
        title: "Tên",
        dataIndex: "name",
        ...this.getColumnSearchProps("Tên báo cáo"),
        fixed: "left",
        width: 300,
        render: (name, record) => {
          let prefixUrl = "/app/reportDetail";
          if (record.type === "summary") {
            prefixUrl = "/app/reportSummaryDetail";
          }
          return <Link to={`${prefixUrl}/${record._id}`}>{record.name}</Link>;
        }
      },
      {
        title: "",
        width: 5
      },
      {
        title: "Danh mục",
        dataIndex: "type",
        width: 150,
        render: (name, record) => {
          let object = _.find(
            Config.TemplateType,
            item => item.value === record.template.type
          );
          return object.label;
        },
        filterMultiple: false,
        filters: Config.TemplateType.map(item => {
          return {
            value: item.value,
            text: item.label
          };
        })
      },
      {
        title: "Loại báo cáo",
        dataIndex: "report",
        width: 150,
        render: (name, record) => {
          return record.type === "normal"
            ? "Báo cáo đơn vị"
            : "Báo cáo tổng hợp";
        },
        filterMultiple: true,
        filters: [
          {
            value: "normal",
            text: "Báo cáo đơn vị"
          },
          {
            value: "summary",
            text: "Báo cáo tổng hợp"
          }
        ]
      },
      {
        title: "Phòng ban",
        width: 150,
        dataIndex: "PhongBan",
        filterMultiple: true,
        filters: this.props.workplaces.map(item => {
          return {
            value: item.shortName,
            text: item.name
          };
        })
      },
      {
        title: "Trạng thái",
        dataIndex: "isCompleted",
        width: 200,
        filterMultiple: true,
        filters: [
          {
            value: true,
            text: "Đã hoàn thành"
          },
          {
            value: false,
            text: "Chưa hoàn thành"
          }
        ],
        render: (name, record) => {
          return <StatusStatistic report={record} />;
        }
      },
      {
        title: "Người tạo",
        dataIndex: "user",
        width: 200,
        render: (name, record) => {
          return record.createdBy.firstName + " " + record.createdBy.lastName;
        }
      },
      {
        title: "Ngày khởi tạo",
        dataIndex: "createdAt",
        width: 200,
        render: (name, record) => {
          return moment(record.createdAt).calendar();
        }
      },
      {
        title: "Ngày TP/ TPB kiểm tra",
        width: 200,
        render: (name, record) => {
          return this.getTimeApprove(record, [
            Constants.Role.TP,
            Constants.Role.TPB
          ]);
        }
      },
      {
        title: "Ngày GĐĐV phê duyệt",
        width: 200,
        render: (name, record) => {
          return this.getTimeApprove(record, [Constants.Role.GDDV]);
        }
      },
      {
        title: "Ngày Ban GĐ phê duyệt",
        width: 200,
        render: (name, record) => {
          return this.getTimeApprove(record, [Constants.Role.GD]);
        }
      }
    ];
  };
  render() {
    const { data, loading, pagination } = this.props;
    return (
      <div>
        <HeaderPage title={"Thống kê báo cáo"} />
        <section className="content v-content">
          <div className="box box-primary">
            <div className="box-body" style={{ minHeight: 600 }}>
              <DateRange onDate={this.onDateRange} />
              <br />
              <Table
                loading={loading}
                columns={this.renderColumn()}
                rowKey={record => record._id}
                dataSource={data}
                pagination={pagination}
                onChange={this.handleTableChange}
                scroll={{ x: 1800 }}
              />
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default StatisticRender;
