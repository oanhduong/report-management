import React from "react";
import { Table } from "antd";
import { Link } from "react-router-dom";
import moment from "moment";
import { DownloadReport } from "components";

class ViewReportUnitRender extends React.PureComponent {
  columns = [
    {
      title: "Tên báo cáo",
      dataIndex: "name",
      sorter: true,
      width: "40%",
      render: (name, record) => (
        <Link to={`/app/reportDetail/${record._id}`}>{record.name}</Link>
      )
    },
    {
      title: "Mẫu báo cáo",
      width: "30%",
      dataIndex: "templateName",
      render: (name, record) => {
        return record.template.name;
      }
    },
    {
      title: "Cập nhật lần cuối",
      dataIndex: "updatedDate",
      with: "15%",
      render: (name, record) => moment(record.updatedAt).calendar()
    },
    {
      title: "Tải xuống",
      render: (name, record) => {
        return <DownloadReport report={record} />;
      }
    }
  ];

  handleTableChange = (pagination, filters, sorter) => {
    this.props.loadReportUnit(pagination.current);
  };

  render() {
    const { pagination, reports, loading } = this.props;
    return (
      <div>
        <section className="content-header">
          <h1>Báo cáo từ đơn vị </h1>
          <ol className="breadcrumb">
            <li>
              <a href="#">
                <i className="fa fa-home" /> Trang chủ
              </a>
            </li>
            <li className="active">Báo cáo từ đơn vị</li>
          </ol>
        </section>
        <section className="content v-content">
          <div className="box box-primary">
            <div className="box-body">
              <Table
                columns={this.columns}
                rowKey={record => record._id}
                dataSource={reports}
                pagination={pagination}
                onChange={this.handleTableChange}
                loading={loading}
              />
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default ViewReportUnitRender;
