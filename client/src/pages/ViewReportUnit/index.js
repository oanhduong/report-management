import React from "react";
import ViewReportUnitRender from "./render";
import * as Services from "services";
import { Utils, Constants } from "common";

class ViewReportUnit extends React.PureComponent {
  state = {
    loading: false,
    pagination: {
      pageSize: Constants.Limit,
      current: 1,
      total: 0
    },
    reports: []
  };

  componentDidMount = () => {
    this.loadReportUnit(1);
  };

  loadReportUnit = page => {
    Services.getReportsUnit(page)
      .then(response => {
        let pagination = { ...this.state.pagination };
        pagination.current = page;
        pagination.total = response.total;
        this.setState({ loading: false, reports: response.list, pagination });
      })
      .catch(error => {
        this.setState({ loading: false, reports: [] });
        Utils.showError(error);
      });
  };

  render() {
    const { pagination, reports, loading } = this.state;
    return (
      <ViewReportUnitRender
        reports={reports}
        loading={loading}
        pagination={pagination}
        loadReportUnit={this.loadReportUnit}
      />
    );
  }
}

export default ViewReportUnit;
