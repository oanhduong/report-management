import React from "react";
import ReportSummaryUpdatingRender from "./render";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";
import * as ActionTypes from "actions/ActionTypes";
import { withRouter } from "react-router-dom";
import { Constants, Utils } from "common";
import * as Services from "services";

class ReportSummaryUpdating extends React.PureComponent {
  state = {
    loading: false,
    pagination: {
      pageSize: Constants.Limit,
      current: 1,
      total: 0
    },
    reports: [],
    deleting: false
  };

  componentDidMount = () => {
    this.loadReportUpdating(1);
  };

  loadReportUpdating = (page, name, date) => {
    this.setState({ loading: true, reports: [] });
    Services.getReportSummary(Constants.Status.Updating, page, name, date)
      .then(response => {
        let pagination = { ...this.state.pagination };
        pagination.current = page;
        pagination.total = response.total;
        this.setState({ loading: false, reports: response.list, pagination });
      })
      .catch(error => {
        this.setState({ loading: false, reports: [] });
        Utils.showError(error);
      });
  };

  onDeleteReport = id => {
    this.setState({ deleting: true });
    Services.deleteReportSummary([id])
      .then(response => {
        this.loadReportUpdating(1);
        Utils.showSuccess("Xoá báo cáo thành công");
      })
      .catch(error => {
        this.setState({ deleting: false });
        Utils.showError(error);
      });
  };

  render() {
    const { loading, pagination, reports } = this.state;
    return (
      <ReportSummaryUpdatingRender
        loading={loading}
        pagination={pagination}
        reports={reports}
        loadReportUpdating={this.loadReportUpdating}
        onDeleteReport={this.onDeleteReport}
      />
    );
  }
}

function mapStateToProps({ reportReducers }) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ReportSummaryUpdating));
