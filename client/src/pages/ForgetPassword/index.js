import React from "react";
import ForgetPasswordRender from './render'

class ForgetPassword extends React.PureComponent {
  render() {
    return <ForgetPasswordRender />;
  }
}

export default ForgetPassword;
