import React from "react";
import { Button } from "components";
import { Link } from "react-router-dom";

class ForgetPasswordRender extends React.PureComponent {
  render() {
    return (
      <div className="login-page login-container">
        <div className="login-card">
          <div className="login-logo">
            <b>PVGasD </b>Report
          </div>
          <div className="login-box-body">
            <p className="login-box-msg">Reset mật khẩu</p>
            <form>
              <div className="form-group has-feedback">
                <input
                  type="email"
                  className="form-control"
                  placeholder="Địa chỉ email..."
                />
                <span className="glyphicon glyphicon-envelope form-control-feedback" />
              </div>
              <Button
                type="button"
                className="btn btn-primary btn-block btn-flat"
              >
                Quên mật khẩu
              </Button>
              <Link to={`/authen/login`}>
                Quay lại
              </Link>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default ForgetPasswordRender;
