import React from "react";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";
import * as ActionTypes from "actions/ActionTypes";
import { withRouter } from "react-router-dom";
import ReportMergeRender from "./render";
import { Constants, Utils } from "common";
import * as Services from "services";
import { findIndex } from "lodash";

class ReportMerge extends React.PureComponent {
  state = {
    loading: false,
    pagination: {
      pageSize: Constants.Limit,
      current: 1,
      total: 0
    },
    reports: [],
    templateFilter: [],
    submiting: false,
    nameReport: ""
  };
  selectedReport = [];

  componentDidMount = () => {
    const { templates } = this.props;
    if (!templates || templates.length === 0) {
      this.props.getReportTemplates();
    } else {
      this.loadReportMerge(1);
    }
  };

  loadReportMerge = page => {
    this.setState({ loading: true });
    Services.getAllReportSummary(page)
      .then(response => {
        let pagination = { ...this.state.pagination };
        pagination.current = page;
        pagination.total = response.total;
        let reports = response.list;
        let { templateFilter } = this.state;
        reports = reports.map((item, index) => {
          let templateName = item.template.name;
          let indexFilter = findIndex(
            templateFilter,
            filter => filter.value === item.template._id
          );
          if (indexFilter < 0) {
            templateFilter.push({
              value: item.template._id,
              text: templateName
            });
          }
          if (this.selectedReport.length > 0) {
            if (item.template._id === this.selectedReport[0].template._id) {
              item.disabled = false;
            } else {
              item.disabled = true;
            }
          }
          return item;
        });
        this.setState({ loading: false, reports, pagination, templateFilter });
      })
      .catch(error => {
        this.setState({ loading: false, reports: [] });
        Utils.showError(error);
      });
  };

  setEmptyReport = () => {
    this.setState({ reports: [] });
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps.type === ActionTypes.GET_REPORT_TEMPLATES_SUCCESS) {
      this.loadReportMerge(1);
    }
  };

  checkAllReport = (selectRows, callback) => {
    if (this.selectedReport.length > 0) {
      let selected = [];
      selectRows.forEach(row => {
        if (row.template._id === this.selectedReport[0].template._id) {
          selected.push(row);
          let index = findIndex(
            this.selectedReport,
            item => item._id === row._id
          );
          if (index < 0) {
            this.selectedReport.push(row);
          }
        }
      });
      callback(selected);
    } else {
      if (selectRows && selectRows.length > 0) {
        let row = selectRows[0];
        let selected = [];
        selectRows.forEach(item => {
          if (item.template._id === row.template._id) {
            selected.push(item);
            let index = findIndex(
              this.selectedReport,
              a => a._id === item._id
            );
            if (index < 0) {
              this.selectedReport.push(item);
            }
          }
        });
        callback(selected);
      }
    }
    this.reCheckDisableReport();
  };

  reCheckDisableReport = () => {
    let reports = this.state.reports.slice();
    reports.map(item => {
      if (this.selectedReport.length > 0) {
        if (item.template._id === this.selectedReport[0].template._id) {
          item.disabled = false;
        } else {
          item.disabled = true;
        }
      } else {
        item.disabled = false;
      }
      return item;
    });
    this.setState({ reports }, () => {
      if (this.selectedReport.length > 1) {
        let report1 = this.selectedReport[0];
        let report2 = this.selectedReport[1];
        let index = Utils.findFirstDiffPos(report1.name, report2.name);
        let nameReport = report1.name.substring(0, index + 1);
        this.setState({ nameReport });
      } else {
        this.setState({ nameReport: "" });
      }
    });
  };

  checkReport = report => {
    let index = findIndex(
      this.selectedReport,
      item => item._id === report._id
    );
    if (index < 0) {
      this.selectedReport.push(report);
      this.reCheckDisableReport();
    }
  };

  unCheckAllReport = (reportRows, callback) => {
    let isChange = false;
    let selected = [];
    reportRows.forEach(report => {
      let index = findIndex(
        this.selectedReport,
        item => item._id === report._id
      );
      if (index >= 0) {
        selected.push(report);
        this.selectedReport.splice(index, 1);
        isChange = true;
      }
    });
    if (isChange) {
      this.reCheckDisableReport();
      callback(selected);
    }
  };

  uncheckReport = report => {
    let index = findIndex(
      this.selectedReport,
      item => item._id === report._id
    );
    if (index >= 0) {
      this.selectedReport.splice(index, 1);
      this.reCheckDisableReport();
    }
  };

  summaryReport = () => {
    const { nameReport } = this.state;
    if (this.selectedReport.length <= 1) {
      return Utils.showWarning("Vui lòng chọn 2 báo cáo cùng mẫu trở lên");
    }
    if (!nameReport || nameReport.length === 0) {
      return Utils.showWarning("Chưa nhập tên báo cáo");
    }
    let listId = this.selectedReport.map(item => {
      return item._id;
    });
    let data = {
      listReportIds: listId,
      name: nameReport
    };
    this.setState({ submiting: true });
    Services.createReportSummary(data)
      .then(response => {
        this.setState({ submiting: false });
        Utils.showSuccess("Tổng hợp báo cáo thành công");
        this.props.history.push("/app/reportSummaryDetail/" + response._id);
      })
      .catch(error => {
        this.setState({ submiting: false });
        Utils.showError(error);
      });
  };

  onChangeNameReport = e => {
    this.setState({ nameReport: e.target.value });
  };

  _getUnit = () => {
    const { user, workplaces } = this.props;
    let unit = "";
    if (user && workplaces && workplaces.length > 0) {
      let index = findIndex(
        workplaces,
        item => item._id === user.workPlace._id
      );
      if (index >= 0) {
        return workplaces[index].shortName;
      }
    }
    return unit;
  };

  render() {
    const {
      reports,
      pagination,
      loading,
      templateFilter,
      submiting,
      nameReport
    } = this.state;
    const { templates } = this.props;
    return (
      <ReportMergeRender
        reports={reports}
        pagination={pagination}
        loading={loading}
        templates={templates}
        templateFilter={templateFilter}
        submiting={submiting}
        loadReportMerge={this.loadReportMerge}
        setEmptyReport={this.setEmptyReport}
        uncheckReport={this.uncheckReport}
        summaryReport={this.summaryReport}
        checkAllReport={this.checkAllReport}
        unCheckAllReport={this.unCheckAllReport}
        checkReport={this.checkReport}
        nameReport={nameReport}
        onChangeNameReport={this.onChangeNameReport}
      />
    );
  }
}

ReportMerge.defaultProps = {
  reports: []
};
function mapStateToProps({ reportReducers, usersReducers }) {
  return {
    type: reportReducers.type,
    user: usersReducers.user,
    templates: reportReducers.templates,
    workplaces: usersReducers.workplaces
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ReportMerge));
