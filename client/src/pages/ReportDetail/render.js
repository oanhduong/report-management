import React from "react";
import { Button, CkEditor, AttachmentModal } from "components";
import "./styles.css";
import { Modal } from "antd";
import { Constants } from "common";

class ReportDetailRender extends React.PureComponent {
  saveReport = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn lưu báo cáo không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.saveReport(this.editor.getData());
      }
    });
  };

  refreshReport = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn làm mới báo cáo không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.refreshReport();
      }
    });
  };

  sendReport = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn gửi báo cáo không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.sendReport(this.editor.getData());
      }
    });
  };

  onApprove = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn duyệt báo cáo không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.sendReport(this.editor.getData());
      }
    });
  };

  onReject = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn gửi trả báo cáo không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.onReject(this.editor.getData());
      }
    });
  };

  isPermissionNV = (report, user) => {
    if (report && report.status && report.status.location && user) {
      return (
        report.status.location.username === user.username &&
        user.role === Constants.Role.NV
      );
    }
    return false;
  };

  isPermissionManager = (report, user) => {
    if (report && report.status && report.status.location && user) {
      return (
        report.status.location.username === user.username &&
        (user.role === Constants.Role.TP ||
          user.role === Constants.Role.TPB ||
          user.role === Constants.Role.GDDV)
      );
    }
    return false;
  };

  isPermissionNVKT = (report, user) => {
    if (report && report.status && report.status.location && user) {
      return (
        report.status.location.username === user.username &&
        user.role === Constants.Role.NVKT
      );
    }
    return false;
  };

  openModalAttachment = () => {
    this.refs.attachment.open();
  };

  chooseFiles = files => {
    this.props.onChangeAttachment(files);
  };

  onRemoveFile = file => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn xoá file đính kèm không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.onRemoveAttachment(file);
      }
    });
  };

  render() {
    const {
      report,
      loading,
      user,
      saveLoading,
      sendLoading,
      rejectLoading,
      uploading
    } = this.props;
    let attachments = report ? report.attachments : [];
    return (
      <div>
        <div className="detail-content">
          <section className="content-header" />
          {this.isPermissionNV(report, user) && (
            <>
              <Button
                className="btn btn-default report-row"
                onClick={this.openModalAttachment}
                icon={<i className="fa fa-file-text-o" />}
              >
                &nbsp; Đính kèm
              </Button>
              <Button
                className="btn btn-default report-row"
                onClick={this.refreshReport}
                icon={<i className="fa fa-refresh" />}
              >
                &nbsp; Làm mới
              </Button>
              <Button
                icon={<i className="fa fa-save" />}
                className="btn btn-default report-row"
                onClick={this.saveReport}
                loading={saveLoading}
              >
                &nbsp; Lưu
              </Button>
              <Button
                onClick={this.sendReport}
                className="btn btn-primary report-row"
                icon={<i className="fa fa-send-o" />}
                loading={sendLoading}
              >
                &nbsp; Gửi báo cáo
              </Button>
            </>
          )}
          {this.isPermissionManager(report, user) && (
            <>
              <Button
                className="btn btn-default report-row"
                onClick={this.openModalAttachment}
                icon={<i className="fa fa-file-text-o" />}
              >
                &nbsp; Đính kèm
              </Button>
              <Button
                className="btn btn-danger report-row"
                onClick={this.onReject}
                loading={rejectLoading}
                icon={<i className="fa fa-close" />}
              >
                &nbsp; Gửi trả
              </Button>
              <Button
                className="btn btn-primary report-row"
                onClick={this.onApprove}
                loading={sendLoading}
                icon={<i className="fa fa-check-square-o" />}
              >
                &nbsp; Duyệt
              </Button>
              <Button
                icon={<i className="fa fa-save" />}
                className="btn btn-default report-row"
                onClick={this.saveReport}
                loading={saveLoading}
              >
                &nbsp; Lưu
              </Button>
            </>
          )}
          {this.isPermissionNVKT(report, user) && (
            <>
              <Button
                className="btn btn-default report-row"
                onClick={this.openModalAttachment}
                icon={<i className="fa fa-file-text-o" />}
              >
                &nbsp; Đính kèm
              </Button>
              <Button
                className="btn btn-danger report-row"
                onClick={this.onReject}
                loading={rejectLoading}
                icon={<i className="fa fa-close" />}
              >
                &nbsp; Gửi trả
              </Button>
              <Button
                icon={<i className="fa fa-save" />}
                className="btn btn-default report-row"
                onClick={this.saveReport}
                loading={saveLoading}
              >
                &nbsp; Lưu
              </Button>
            </>
          )}
        </div>
        <section className="content v-content">
          <div className="box box-primary">
            <div className="box-header with-border">
              {!loading && !report && (
                <h3 className="box-title">Không tìm thấy báo cáo</h3>
              )}
              {loading && (
                <h3 className="box-title">
                  <i className="fa fa-fw fa-spinner fa-spin" />
                  Đang tải báo cáo....
                </h3>
              )}
              {!loading && report && (
                <h3 className="box-title">{report.name}</h3>
              )}
            </div>
            <div className="box-body">
              {!loading && report && (
                <CkEditor
                  data={report.data}
                  id={"reportDetail"}
                  onInitEditor={editor => {
                    this.editor = editor;
                  }}
                  config={{
                    allowedContent: true,
                    extraPlugins: "comments",
                    height: 500,
                    removePlugins: "sourcearea",
                    basicEntities: false,
                    entities_latin: false
                  }}
                />
              )}
            </div>
          </div>
        </section>
        <AttachmentModal
          ref="attachment"
          loading={uploading}
          files={attachments}
          chooseFiles={this.chooseFiles}
          onRemove={this.onRemoveFile}
        />
      </div>
    );
  }
}
export default ReportDetailRender;
