import React from "react";
import ReportDetailRender from "./render";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { findIndex } from "lodash";
import * as Services from "services";
import { Utils } from "common";

class ReportDetail extends React.PureComponent {
  state = {
    report: null,
    loading: false,
    saveLoading: false,
    sendLoading: false,
    rejectLoading: false,
    uploading: false
  };

  onReject = data => {
    const { report } = this.state;
    if (report == null) {
      return Utils.showError("Chưa tải báo cáo");
    }
    const { match } = this.props;
    let id = match.params.id;
    this.setState({ rejectLoading: true });
    Services.updateReportNormal(id, { data })
      .then(response => {
        Services.rejectReport(id)
          .then(response => {
            this.props.history.replace("/app/reportNew");
            this.setState({ rejectLoading: false });
            Utils.showSuccess("Gửi trả báo cáo thành công");
          })
          .catch(error => {
            Utils.showError(error);
            this.setState({ rejectLoading: false });
          });
      })
      .catch(error => {
        Utils.showError(error);
        this.setState({ rejectLoading: false });
      });
  };

  componentDidMount = () => {
    const { match } = this.props;
    let id = match.params.id;
    this.loadReport(id);
  };

  loadReport = id => {
    this.setState({ loading: true });
    Services.getReportNormalDetail(id)
      .then(response => {
        this.setState({ loading: false, report: response });
      })
      .catch(error => {
        Utils.showError(error);
        this.setState({ loading: false, report: null });
      });
  };

  saveReport = dataHtml => {
    const { match } = this.props;
    let id = match.params.id;
    this.setState({ saveLoading: true });
    Services.updateReportNormal(id, { data: dataHtml })
      .then(response => {
        this.setState({ saveLoading: false });
        this.refreshReport();
        Utils.showSuccess("Lưu báo cáo thành công");
      })
      .catch(error => {
        Utils.showError(error);
        this.setState({ saveLoading: false });
      });
  };

  sendReport = data => {
    const { report } = this.state;
    if (report == null) {
      return Utils.showError("Chưa tải báo cáo");
    }
    const { match } = this.props;
    let id = match.params.id;
    this.setState({ sendLoading: true });
    Services.updateReportNormal(id, { data })
      .then(response => {
        Services.sendReport(id)
          .then(response => {
            this.props.history.replace("/app/reportSent");
            this.setState({ sendLoading: false });
            Utils.showSuccess("Gửi báo cáo thành công");
          })
          .catch(error => {
            Utils.showError(error);
            this.setState({ sendLoading: false });
          });
      })
      .catch(error => {
        Utils.showError(error);
        this.setState({ sendLoading: false });
      });
  };

  refreshReport = () => {
    const { match } = this.props;
    let id = match.params.id;
    this.loadReport(id);
  };

  onChangeAttachment = files => {
    let report = { ...this.state.report };
    let attachments = report.attachments.concat(files);
    const { match } = this.props;
    let id = match.params.id;
    this.setState({ uploading: true });
    Services.updateReportNormal(id, { attachments })
      .then(response => {
        this.setState({ uploading: false });
        report.attachments = response.attachments;
        this.setState({ report });
      })
      .catch(error => {
        this.setState({ uploading: false });
        Utils.showError(error);
      });
  };

  onRemoveAttachment = file => {
    let report = { ...this.state.report };
    let attachments = report.attachments;
    let index = findIndex(attachments, att => att._id === file._id);
    if (index < 0) {
      return;
    }
    attachments.splice(index, 1);
    const { match } = this.props;
    let id = match.params.id;
    this.setState({ uploading: true });
    Services.updateReportNormal(id, { attachments })
      .then(response => {
        this.setState({ uploading: false });
        report.attachments = response.attachments;
        this.setState({ report });
      })
      .catch(error => {
        this.setState({ uploading: false });
        Utils.showError(error);
      });
  };

  render() {
    const {
      report,
      loading,
      saveLoading,
      sendLoading,
      rejectLoading,
      uploading
    } = this.state;
    const { user } = this.props;
    return (
      <ReportDetailRender
        report={report}
        user={user}
        loading={loading}
        sendLoading={sendLoading}
        saveLoading={saveLoading}
        rejectLoading={rejectLoading}
        saveReport={this.saveReport}
        refreshReport={this.refreshReport}
        sendReport={this.sendReport}
        onReject={this.onReject}
        onChangeAttachment={this.onChangeAttachment}
        uploading={uploading}
        onRemoveAttachment={this.onRemoveAttachment}
      />
    );
  }
}

ReportDetail.defaultProps = {
  reports: [],
  user: {}
};
function mapStateToProps({ reportReducers, usersReducers }) {
  return {
    type: reportReducers.type,
    reports: reportReducers.reports,
    reportDrafts: reportReducers.reportDrafts,
    user: usersReducers.user
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ReportDetail)
);
