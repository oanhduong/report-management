import React from "react";
import { Button, CkEditor } from "components";
import {
  Icon,
  Select,
  Input,
  Spin,
  Collapse,
  Form,
  Row,
  Col,
  Radio,
  Modal,
  Tooltip
} from "antd";
import { Constants, Config } from "common";
const { Panel } = Collapse;
const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 }
  }
};

const formTailLayout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 }
};
const radioStye = {
  height: "30px",
  width: "400px",
  lineHeight: "30px",
  display: "block"
};

class TemplateDetailRender extends React.PureComponent {
  state = {
    activeKey: "1",
    dataSourceNV: [],
    dataSourceNVKT: []
  };

  setChangeActiveKey = key => {
    this.setState({ activeKey: key });
  };

  onChangeActiveKey = key => {
    this.setState({ activeKey: key[1] });
  };

  _searchAutoComplete = (value, role) => {
    let user = [];
    let users = [];
    if (role == Constants.Role.NV) {
      users = this.props.userNV;
    } else if (role == Constants.Role.NVKT) {
      users = this.props.userNVKT;
    }
    user = _.filter(users, item => {
      let indexLastName = item.lastName ? item.lastName.indexOf(value) : -1;
      let indexFirstName = item.firstName ? item.firstName.indexOf(value) : -1;
      return indexLastName >= 0 || indexFirstName >= 0;
    });
    return user;
  };

  handleSearchNV = value => {
    this.indexNV = -1;
    let search = this._searchAutoComplete(value, Constants.Role.NV);
    if (search) {
      this.setState({
        dataSourceNV: search
      });
    }
  };

  onFocusNV = () => {
    this.handleSearchNV("");
  };

  onFocusNVKT = () => {
    this.handleSearchNVKT("");
  };

  handleSearchNVKT = value => {
    this.indexNVKT = -1;
    let search = this._searchAutoComplete(value, Constants.Role.NVKT);
    if (search) {
      this.setState({
        dataSourceNVKT: search
      });
    }
  };

  initEditor = editor => {
    this.editor = editor;
  };

  onDeleteTemplate = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn xoá biểu mẫu không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.onDeleteTemplate();
      }
    });
  };

  onChangeStartDate = value => {
    this.props.onChangeTime("startDate", value);
  };

  onChangeEndDate = value => {
    this.props.onChangeTime("endDate", value);
  };

  onChangeStartMonth = value => {
    this.props.onChangeTime("startMonth", value);
  };

  onChangeEndMonth = value => {
    this.props.onChangeTime("endMonth", value);
  };

  render() {
    const { activeKey, dataSourceNV, dataSourceNVKT } = this.state;
    const {
      template,
      loading,
      process,
      onChangeName,
      onChangeProcess,
      userNV,
      userNVKT,
      onChangeNV,
      onChangeNVKT,
      onChangeTimeType,
      onChangeScope,
      onChangeType,
      updateTemplate,
      uploading,
      deleting,
      onChangeDateRange
    } = this.props;
    if (loading) {
      return (
        <div className="template-container">
          <div className="detail-content template-content">
            <section className="content-header" />
            <Spin
              indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
            />
            {"   "}
            Đang tải biểu mẫu...
          </div>
        </div>
      );
    }
    let nvs = dataSourceNV && dataSourceNV.length > 0 ? dataSourceNV : userNV;
    let selectNV = template.manage
      ? template.manage.nv.map(item => {
          let value = item;
          if (_.has(value, "userId")) {
            value = item.userId;
          }
          return { key: value };
        })
      : [];
    let selectNVKT = template.manage ? template.manage.nvkt : undefined;
    let nvkts =
      dataSourceNVKT && dataSourceNVKT.length > 0 ? dataSourceNVKT : userNVKT;
    template.deadline = template.deadline ? template.deadline : {};
    return (
      <div className="template-container">
        <div className="detail-content template-content">
          <section className="content-header" />
          <Button
            className="btn report-row btn-primary btn-sm"
            onClick={updateTemplate}
            loading={uploading}
            icon={<i className="fa fa-save" />}
          >
            &nbsp; Lưu
          </Button>
          <Button
            icon={<i className="fa fa-trash" />}
            loading={deleting}
            onClick={this.onDeleteTemplate}
            className="btn btn-danger report-row btn-sm"
          >
            &nbsp; Xoá
          </Button>
        </div>
        <div className="detail-content template-content">
          <section className="content-header" />
          <Collapse
            defaultActiveKey={["1"]}
            bordered={false}
            activeKey={[activeKey]}
            onChange={this.onChangeActiveKey}
          >
            <Panel header="Thiết lập" key="1">
              <Form {...formItemLayout} labelAlign={"left"}>
                <Form.Item label="Tên biểu mẫu">
                  <Input
                    placeholder="Tên biểu mẫu..."
                    value={template.name}
                    onChange={onChangeName}
                  />
                </Form.Item>
                <Form.Item label="Quy trình">
                  {process && process.length > 0 && (
                    <Select
                      placeholder="Quy trình"
                      onChange={onChangeProcess}
                      value={template.quytrinh ? template.quytrinh._id : null}
                    >
                      {process.map((item, index) => {
                        return (
                          <Option key={index} value={item._id}>
                            {item.name}
                          </Option>
                        );
                      })}
                    </Select>
                  )}
                </Form.Item>
                <Row gutter={16}>
                  <Col span={12}>
                    <Form.Item label="Nhân viên khởi tạo" {...formTailLayout}>
                      <Select
                        mode="multiple"
                        labelInValue
                        value={selectNV}
                        placeholder="Chọn nhân viên khởi tạo"
                        onChange={onChangeNV}
                        onSearch={this.handleSearchNV}
                        defaultActiveFirstOption={false}
                        filterOption={false}
                        notFoundContent={null}
                        onFocus={this.onFocusNV}
                      >
                        {nvs.map((item, index) => {
                          return (
                            <Option key={item._id}>
                              {item.firstName} {item.lastName} -{" "}
                              {item.workPlace.name}
                            </Option>
                          );
                        })}
                      </Select>
                    </Form.Item>
                    <Form.Item label="Nhân viên tổng hợp" {...formTailLayout}>
                      <Select
                        showSearch
                        value={selectNVKT}
                        placeholder="Chọn nhân viên tổng hợp"
                        onChange={onChangeNVKT}
                        onSearch={this.handleSearchNVKT}
                        defaultActiveFirstOption={false}
                        showArrow={false}
                        filterOption={false}
                        onFocus={this.onFocusNVKT}
                        disabled={
                          template.scope === Config.TemplateScope[1].value
                        }
                      >
                        {nvkts.map((item, index) => {
                          return (
                            <Option key={item._id}>
                              {item.firstName} {item.lastName} -{" "}
                              {item.workPlace.name}
                            </Option>
                          );
                        })}
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <fieldset>
                      <legend>Phân loại biểu mẫu:</legend>
                      <Radio.Group
                        style={radioStye}
                        options={Config.TemplateScope}
                        value={template.scope}
                        onChange={onChangeScope}
                      />
                      <Radio.Group
                        style={radioStye}
                        options={Config.TemplateType}
                        value={template.type}
                        onChange={onChangeType}
                      />
                      <Radio.Group
                        style={radioStye}
                        options={Config.TimeType}
                        value={template.timeType}
                        onChange={onChangeTimeType}
                      />
                    </fieldset>
                  </Col>
                </Row>
                <Row gutter={16}>
                  <Col span={12}>
                    <Form.Item label="Thời hạn báo cáo" {...formTailLayout}>
                      <Tooltip placement="top" title={'Ngày bắt đầu tạo báo cáo trong tháng'}>
                        <Select
                          style={{ width: "49%" }}
                          value={template.deadline.startDate}
                          placeholder={"Ngày bắt đầu"}
                          onChange={this.onChangeStartDate}
                        >
                          {[...Array(28)].map((item, index) => {
                            if (template.deadline.endDate) {
                              let disabled =
                                index + 1 > template.deadline.endDate;
                              return (
                                <Option key={index + 1} disabled={disabled}>
                                  {index + 1}
                                </Option>
                              );
                            } else {
                              return <Option key={index + 1}>{index + 1}</Option>;
                            }
                          })}
                        </Select>
                      </Tooltip>
                      &nbsp;
                      <Tooltip placement="top" title={'Ngày kết thúc tạo báo cáo trong tháng'}>
                        <Select
                          style={{ width: "49%" }}
                          value={template.deadline.endDate}
                          placeholder={"Ngày kết thúc"}
                          onChange={this.onChangeEndDate}
                        >
                          {[...Array(28)].map((item, index) => {
                            if (template.deadline.startDate) {
                              let disabled =
                                index + 1 < template.deadline.startDate;
                              return (
                                <Option key={index + 1} disabled={disabled}>
                                  {index + 1}
                                </Option>
                              );
                            } else {
                              return <Option key={index + 1}>{index + 1}</Option>;
                            }
                          })}
                        </Select>
                      </Tooltip>
                    </Form.Item>
                  </Col>
                  {template.timeType === Config.TimeType[1].value && (
                    <Col span={12}>
                      <Form.Item>
                        <Tooltip placement="top" title={'Tháng thứ mấy trong quý bắt đầu tạo báo cáo'}>
                          <Select
                            style={{ width: "49%" }}
                            value={template.deadline.startMonth}
                            placeholder={"Tháng bắt đầu"}
                            onChange={this.onChangeStartMonth}
                          >
                            {[...Array(3)].map((item, index) => {
                              if (template.deadline.endMonth) {
                                let disabled =
                                  index + 1 > template.deadline.endMonth;
                                return (
                                  <Option key={index + 1} disabled={disabled}>
                                    {index + 1}
                                  </Option>
                                );
                              } else {
                                return (
                                  <Option key={index + 1}>{index + 1}</Option>
                                );
                              }
                            })}
                          </Select>
                        </Tooltip>
                        &nbsp;
                        <Tooltip placement="top" title={'Tháng thứ mấy trong quý kết thúc tạo báo cáo'}>
                          <Select
                            style={{ width: "49%" }}
                            value={template.deadline.endMonth}
                            placeholder={"Tháng kết thúc"}
                            onChange={this.onChangeEndMonth}
                          >
                            {[...Array(3)].map((item, index) => {
                              if (template.deadline.startMonth) {
                                let disabled =
                                  index + 1 <
                                  parseInt(template.deadline.startMonth);
                                return (
                                  <Option key={index + 1} disabled={disabled}>
                                    {index + 1}
                                  </Option>
                                );
                              } else {
                                return (
                                  <Option key={index + 1}>{index + 1}</Option>
                                );
                              }
                            })}
                          </Select>
                        </Tooltip>
                      </Form.Item>
                    </Col>
                  )}
                </Row>
              </Form>
            </Panel>
            <Panel header="Nội dung biểu mẫu" key="2">
              {template && (
                <CkEditor
                  id={"templateDetail"}
                  onInitEditor={this.initEditor}
                  data={template.data}
                  config={{
                    allowedContent: true,
                    height: 500,
                    basicEntities: false,
                    entities_latin: false,
                    readOnly: true
                  }}
                />
              )}
            </Panel>
          </Collapse>
        </div>
      </div>
    );
  }
}

TemplateDetailRender.defaultProps = {
  userNV: []
};

export default TemplateDetailRender;
