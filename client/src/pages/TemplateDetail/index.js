import React from "react";
import TemplateDetailRender from "./render";
import * as Services from "services";
import { Utils, Constants, Config } from "common";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";

class TemplateDetail extends React.PureComponent {
  state = {
    template: {},
    loading: false,
    process: [],
    userNVKT: [],
    userNV: [],
    updating: false,
    deleting: false
  };

  componentDidMount = () => {
    this.loadData();
  };

  getUsersByNVKT = () => {
    Services.getUsersByRole(Constants.Role.NVKT)
      .then(response => {
        this.setState({ userNVKT: response.list });
      })
      .catch(error => {
        Utils.showError(error);
      });
  };

  getUsersByNV = () => {
    Services.getUsersByRole(Constants.Role.NV)
      .then(response => {
        this.setState({ userNV: response.list });
      })
      .catch(error => {
        Utils.showError(error);
      });
  };

  loadData = async () => {
    const { match } = this.props;
    let id = match.params.id;
    this.setState({ loading: true });
    await this.getUsersByNV();
    await this.getUsersByNVKT();
    let responseProcess = await Services.getAllQuyTrinh();
    let templates = await Services.getTemplate(id);
    if (templates && templates.length > 0) {
      this.setState({ template: templates[0], process: responseProcess.list });
    } else {
      Utils.showWarning("Không tìm thấy biểu mẫu");
    }
    this.setState({ loading: false });
  };

  onChangeName = e => {
    let template = { ...this.state.template };
    template.name = e.target.value;
    this.setState({ template });
  };

  onChangeProcess = value => {
    let template = { ...this.state.template };
    template.quytrinh._id = value;
    this.setState({ template });
  };

  onChangeNV = items => {
    let template = { ...this.state.template };
    template.manage.nv = items.map(item => {
      return item.key;
    });
    this.setState({ template });
  };

  onChangeNVKT = item => {
    let template = { ...this.state.template };
    template.manage.nvkt = item;
    this.setState({ template });
  };

  onChangeType = e => {
    let template = { ...this.state.template };
    template.type = e.target.value;
    this.setState({ template });
  };

  onChangeScope = e => {
    let template = { ...this.state.template };
    template.scope = e.target.value;
    this.setState({ template });
  };

  onChangeTimeType = e => {
    let template = { ...this.state.template };
    template.timeType = e.target.value;
    this.setState({ template });
  };

  updateTemplate = async () => {
    const { template } = this.state;
    if (Utils.isEmpty(template.name)) {
      return Utils.showWarning("Chưa nhập tên biễu mẫu");
    }
    if (
      !template.manage ||
      !template.manage.nv ||
      template.manage.nv.length === 0
    ) {
      return Utils.showWarning("Chưa chọn nhân viên tạo báo cáo");
    }
    if (
      (!template.manage ||
        !template.manage.nvkt ||
        template.manage.nvkt.length === 0) &&
      template.scope === Config.TemplateScope[0].value
    ) {
      return Utils.showWarning("Chưa chọn nhân viên tổng hợp báo cáo");
    }
    if (!template.deadline.startDate || !template.deadline.endDate) {
      return Utils.showWarning("Nhập ngày hết hạn báo cáo");
    }
    if (template.timeType === Config.TimeType[1].value) {
      if (!template.deadline.startMonth || !template.deadline.endMonth) {
        return Utils.showWarning("Nhập tháng hết hạn báo cáo");
      }
    }

    let deadline = {
      startDate: parseInt(template.deadline.startDate),
      endDate: parseInt(template.deadline.endDate)
    };
    if (template.timeType === Config.TimeType[1].value) {
      deadline.startMonth = parseInt(template.deadline.startMonth);
      deadline.endMonth = parseInt(template.deadline.endMonth);
    }


    let data = {
      scope: template.scope,
      type: template.type,
      timeType: template.timeType,
      name: template.name,
      quytrinh: template.quytrinh._id,
      deadline,
      manage: {
        nv: template.manage.nv.map(item => {
          let value = item;
          if (_.has(value, "userId")) {
            value = item.userId;
          }
          return {
            userId: value
          };
        })
      }
    };
    if (template.scope === Config.TemplateScope[0].value) {
      data.manage.nvkt = template.manage.nvkt;
    }
    try {
      this.setState({ uploading: true });
      await Services.updateTemplate(template._id, data);
      this.setState({ uploading: false });
      Utils.showSuccess("Cập nhật biểu mẫu thành công");
    } catch (error) {
      Utils.showError(error);
      this.setState({ uploading: false });
    }
  };

  onDeleteTemplate = async () => {
    const { match } = this.props;
    let id = match.params.id;
    try {
      this.setState({ deleting: true });
      await Services.deleteTemplate([id]);
      this.setState({ deleting: false });
      Utils.showSuccess("Xoá biểu mẫu thành công");
      this.props.history.push("/app/template");
    } catch (error) {
      Utils.showError(error);
      this.setState({ deleting: false });
    }
  };

  onChangeTime = (field, value) => {
    let template = { ...this.state.template };
    template.deadline[field] = value;
    this.setState({ template });
  };

  render() {
    const {
      loading,
      template,
      process,
      userNVKT,
      userNV,
      uploading,
      deleting
    } = this.state;
    const { workplaces } = this.props;
    return (
      <TemplateDetailRender
        template={template}
        loading={loading}
        uploading={uploading}
        deleting={deleting}
        process={process}
        onChangeName={this.onChangeName}
        onChangeProcess={this.onChangeProcess}
        workplaces={workplaces}
        userNVKT={userNVKT}
        userNV={userNV}
        onChangeNV={this.onChangeNV}
        onChangeNVKT={this.onChangeNVKT}
        onChangeTimeType={this.onChangeTimeType}
        onChangeScope={this.onChangeScope}
        onChangeType={this.onChangeType}
        updateTemplate={this.updateTemplate}
        onDeleteTemplate={this.onDeleteTemplate}
        onChangeTime={this.onChangeTime}
      />
    );
  }
}

TemplateDetail.defaultProps = {
  user: {},
  users: [],
  workplaces: []
};
function mapStateToProps({ usersReducers }) {
  return {
    user: usersReducers.user,
    users: usersReducers.users,
    workplaces: usersReducers.workplaces
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(TemplateDetail));
