import React from "react";
import { Button, HeaderPage } from "components";
import { Utils } from "common";

class ProfileRender extends React.PureComponent {
  render() {
    const {
      userChange,
      user,
      workplaceName,
      onChangeEmail,
      onChangeLastName,
      onChangeFirstName,
      updateUser,
      loading,
      pass,
      onChangePassOld,
      onChangePassNew,
      onChangePassNew1,
      changePassword
    } = this.props;
    let fullName =
      user.firstName && user.lastName
        ? user.firstName + " " + user.lastName
        : user.username;
    return (
      <div>
        <HeaderPage title={"Thông tin cá nhân"} />
        <section className="content">
          <div className="row">
            <div className="col-md-4">
              <div className="box box-primary">
                <div className="box-body box-profile">
                  <img
                    className="profile-user-img img-responsive img-circle"
                    src="/public/AdminLTE/img/ic_default_avatar.jpg"
                    alt="User profile picture"
                  />
                  <h3 className="profile-username text-center">{fullName}</h3>
                  <p className="text-muted text-center">{workplaceName}</p>
                  <ul className="list-group list-group-unbordered">
                    <li className="list-group-item">
                      <b>Tài khoản</b>{" "}
                      <a className="pull-right">{user.username}</a>
                    </li>
                    <li className="list-group-item">
                      <b>Email</b> <a className="pull-right">{user.email}</a>
                    </li>
                    <li className="list-group-item">
                      <b>Chức vụ</b>{" "}
                      <a className="pull-right">
                        {Utils.getRoleName(user.role)}
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div className="col-md-8">
              <div className="nav-tabs-custom">
                <ul className="nav nav-tabs">
                  <li className="active">
                    <a href="#userInfo" data-toggle="tab" aria-expanded="true">
                      Thay đổi thông tin
                    </a>
                  </li>
                  <li className="">
                    <a
                      href="#changePass"
                      data-toggle="tab"
                      aria-expanded="false"
                    >
                      Đổi mật khẩu
                    </a>
                  </li>
                </ul>
                <div className="tab-content">
                  <div className="tab-pane active" id="userInfo">
                    <div className="form-group">
                      <label>Họ</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập họ "
                        value={userChange.firstName}
                        onChange={onChangeFirstName}
                      />
                    </div>
                    <div className="form-group">
                      <label>Tên</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập tên"
                        value={userChange.lastName}
                        onChange={onChangeLastName}
                      />
                    </div>
                    <div className="form-group">
                      <label>Địa chỉ email</label>
                      <input
                        type="email"
                        className="form-control"
                        placeholder="Nhập địa chỉ email"
                        value={userChange.email}
                        onChange={onChangeEmail}
                      />
                    </div>
                    <div className="form-group">
                      <label>Chức vụ</label>
                      <input
                        type="text"
                        className="form-control"
                        disabled
                        defaultValue={Utils.getRoleName(userChange.role)}
                      />
                    </div>
                    <div className="form-group">
                      <label>Phòng ban</label>
                      <input
                        type="email"
                        className="form-control"
                        defaultValue={workplaceName}
                        disabled
                      />
                    </div>
                    <div className="row">
                      <div className="col-md-4" />
                      <div className="col-md-4">
                        <Button
                          className="btn btn-primary btn-block"
                          onClick={updateUser}
                          loading={loading}
                        >
                          Cập nhật
                        </Button>
                      </div>
                    </div>
                  </div>

                  <div className="tab-pane" id="changePass">
                    <div className="form-group">
                      <label>Mật khẩu cũ</label>
                      <input
                        type="password"
                        className="form-control"
                        placeholder="Nhập mật khẩu cũ"
                        value={pass.oldPwd}
                        onChange={onChangePassOld}
                      />
                    </div>
                    <div className="form-group">
                      <label>Mật khẩu mới</label>
                      <input
                        type="password"
                        className="form-control"
                        placeholder="Nhập mật khẩu mới"
                        value={pass.newPwd}
                        onChange={onChangePassNew}
                      />
                    </div>
                    <div className="form-group">
                      <label>Nhập lại mật khẩu</label>
                      <input
                        type="password"
                        className="form-control"
                        placeholder="Nhập lại mật khẩu"
                        value={pass.newPwd1}
                        onChange={onChangePassNew1}
                      />
                    </div>
                    <div className="row">
                      <div className="col-md-4" />
                      <div className="col-md-4">
                        <Button
                          className="btn btn-primary btn-block"
                          loading={loading}
                          onClick={changePassword}
                        >
                          Thay đổi mật khẩu
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default ProfileRender;
