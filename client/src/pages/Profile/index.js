import React from "react";
import ProfileRender from "./render";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { Utils } from "common";
import * as Services from "services";

class Profile extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      user: Object.assign({}, props.user),
      loading: false,
      pass: {}
    };
  }

  onChangePwd = (e, field) => {
    let pass = { ...this.state.pass };
    pass[field] = e.target.value;
    this.setState({ pass });
  };

  onChangePassOld = e => {
    this.onChangePwd(e, "oldPwd");
  };

  onChangePassNew = e => {
    this.onChangePwd(e, "newPwd");
  };

  onChangePassNew1 = e => {
    this.onChangePwd(e, "newPwd1");
  };

  onChangeUser = (e, field) => {
    let user = { ...this.state.user };
    user[field] = e.target.value;
    this.setState({ user });
  };

  onChangeFirstName = e => {
    this.onChangeUser(e, "firstName");
  };

  onChangeLastName = e => {
    this.onChangeUser(e, "lastName");
  };

  onChangeEmail = e => {
    this.onChangeUser(e, "email");
  };

  updateUser = () => {
    const { user } = this.state;
    if (Utils.isEmpty(user.firstName)) {
      return Utils.showWarning("Nhập họ người dùng");
    }
    if (Utils.isEmpty(user.lastName)) {
      return Utils.showWarning("Nhập tên người dùng");
    }
    if (Utils.isEmpty(user.email)) {
      return Utils.showWarning("Nhập địa chỉ email");
    }
    this.setState({ loading: true });
    let data = {
      _id: user._id,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email
    };
    Services.updateUser(data)
      .then(response => {
        this.setState({ loading: false });
        Utils.showSuccess("Cập nhật thành công");
        this.props.updateUser(user);
      })
      .catch(error => {
        this.setState({ loading: false });
        Utils.showError(error);
      });
  };

  changePassword = () => {
    const { pass } = this.state;
    if (Utils.isEmpty(pass.oldPwd)) {
      return Utils.showWarning("Nhập mật khẩu cũ ");
    }
    if (Utils.isEmpty(pass.newPwd)) {
      return Utils.showWarning("Nhập mật khẩu mới ");
    }
    if (Utils.isEmpty(pass.newPwd1)) {
      return Utils.showWarning("Nhập lại mật khẩu mới ");
    }

    if (pass.newPwd !== pass.newPwd1) {
      return Utils.showWarning("Nhập lại mật khẩu không đúng");
    }
    this.setState({ loading: true });
    const { user } = this.props;
    Services.changePassword(user._id, pass.oldPwd, pass.newPwd)
      .then(response => {
        this.setState({
          pass: { oldPwd: "", newPwd: "", newPwd1: "" },
          loading: false
        });
        Utils.showSuccess("Đổi mật khẩu thành công");
      })
      .catch(error => {
        this.setState({ loading: false });
        Utils.showError(error);
      });
  };

  render() {
    const { user } = this.props;
    const { loading, pass } = this.state;
    let workplaceName = user.workPlace.name;
    return (
      <ProfileRender
        userChange={this.state.user}
        user={user}
        workplaceName={workplaceName}
        onChangeEmail={this.onChangeEmail}
        onChangeLastName={this.onChangeLastName}
        onChangeFirstName={this.onChangeFirstName}
        updateUser={this.updateUser}
        loading={loading}
        pass={pass}
        onChangePassOld={this.onChangePassOld}
        onChangePassNew={this.onChangePassNew}
        onChangePassNew1={this.onChangePassNew1}
        changePassword={this.changePassword}
      />
    );
  }
}

Profile.defaultProps = {
  templates: [],
  workplaces: [],
  user: {}
};

function mapStateToProps({ usersReducers }) {
  return {
    type: usersReducers.type,
    workplaces: usersReducers.workplaces,
    user: usersReducers.user
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Profile));
