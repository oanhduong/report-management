import React from "react";
import { Button, CkEditor, DirectionModal, AttachmentModal } from "components";
import { Modal } from "antd";
import { Constants, Utils } from "common";
import { findIndex } from "lodash";

class ReportSummaryDetailRender extends React.PureComponent {
  chidao = [];
  isPermissionNVKT = (report, user) => {
    if (report && user) {
      return (
        report.status.location.username === user.username &&
        user.role === Constants.Role.NVKT
      );
    }
    return false;
  };

  isPermissionTPKT = (report, user) => {
    if (report && user) {
      return (
        report.status.location.username === user.username &&
        user.role === Constants.Role.TPKT
      );
    }
    return false;
  };

  isPermissionGD = (report, user) => {
    if (report && user) {
      return (
        report.status.location.username === user.username &&
        user.role === Constants.Role.GD &&
        (report.status.code === Constants.Status.Updating ||
          report.status.code === Constants.Status.NeedReview)
      );
    }
    return false;
  };

  saveReport = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn lưu báo cáo không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.saveReport(this.editor.getData());
      }
    });
  };

  sendReport = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn gửi báo cáo không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.sendReport(this.editor.getData());
      }
    });
  };

  refreshReport = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn làm mới báo cáo không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.refreshReport();
      }
    });
  };

  onReject = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn gửi trả báo cáo tổng hợp không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.onReject(this.editor.getData());
      }
    });
  };

  onApprove = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn duyệt báo cáo tổng hợp không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        const { user } = this.props;
        if (user.role === Constants.Role.GD) {
          let chid = this.chidao.map(item => {
            delete item.id;
            return item;
          });
          this.props.sendReport(this.editor.getData(), { chidao: chid });
        } else {
          this.props.sendReport(this.editor.getData());
        }
      }
    });
  };

  initEditor = editor => {
    this.editor = editor;
    this.editor.on("openDirection", () => {
      this.refs.direction.open(this.editor);
    });
    this.editor.on("updateDirection", () => {
      this.refs.direction.update(this.editor);
    });
  };

  openDirection = () => {
    this.refs.direction.open(this.editor);
  };

  onDirection = (obj, id) => {
    let index = findIndex(this.chidao, c => c.id == id);
    if (index >= 0) {
      this.chidao[index] = obj;
    } else {
      this.chidao.push(obj);
    }
  };

  onRemoveDirection = id => {
    let index = findIndex(this.chidao, c => c.id === id);
    this.chidao.splice(index, 1);
  };

  openModalAttachment = () => {
    this.refs.attachment.open();
  };

  chooseFiles = files => {
    this.props.onChangeAttachment(files);
  };

  onRemoveFile = file => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn xoá file đính kèm không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.onRemoveAttachment(file);
      }
    });
  };

  render() {
    const {
      report,
      loading,
      user,
      saveLoading,
      sendLoading,
      rejectLoading,
      users,
      workplaces,
      uploading
    } = this.props;
    let plugin = "comments";
    if (user.role === Constants.Role.GD) {
      plugin = "direction";
    }
    let attachments = report ? report.attachments : [];
    return (
      <div>
        <div className="detail-content">
          <section className="content-header" />
          {this.isPermissionNVKT(report, user) && (
            <>
              <Button
                className="btn btn-default report-row"
                onClick={this.openModalAttachment}
                icon={<i className="fa fa-file-text-o" />}
              >
                &nbsp; Đính kèm
              </Button>
              <Button
                className="btn btn-default report-row"
                onClick={this.refreshReport}
                loading={loading}
                icon={<i className="fa fa-refresh" />}
              >
                &nbsp; Làm mới
              </Button>
              <Button
                icon={<i className="fa fa-save" />}
                className="btn btn-default report-row"
                onClick={this.saveReport}
                loading={saveLoading}
              >
                &nbsp; Lưu
              </Button>
              <Button
                onClick={this.sendReport}
                className="btn btn-primary report-row"
                icon={<i className="fa fa-send-o" />}
                loading={sendLoading}
              >
                &nbsp; Gửi báo cáo
              </Button>
            </>
          )}
          {this.isPermissionTPKT(report, user) && (
            <>
              <Button
                className="btn btn-default report-row"
                onClick={this.openModalAttachment}
                icon={<i className="fa fa-file-text-o" />}
              >
                &nbsp; Đính kèm
              </Button>
              <Button
                className="btn btn-danger report-row"
                onClick={this.onReject}
                loading={rejectLoading}
                icon={<i className="fa fa-close" />}
              >
                &nbsp; Gửi trả
              </Button>
              <Button
                className="btn btn-primary report-row"
                onClick={this.onApprove}
                loading={sendLoading}
                icon={<i className="fa fa-check-square-o" />}
              >
                &nbsp; Duyệt
              </Button>
              <Button
                icon={<i className="fa fa-save" />}
                className="btn btn-default report-row"
                onClick={this.saveReport}
                loading={saveLoading}
              >
                &nbsp; Lưu
              </Button>
            </>
          )}
          {this.isPermissionGD(report, user) && (
            <>
              <Button
                className="btn btn-default report-row"
                onClick={this.openModalAttachment}
                icon={<i className="fa fa-file-text-o" />}
              >
                &nbsp; Đính kèm
              </Button>
              <Button
                className="btn btn-primary report-row"
                onClick={this.openDirection}
                icon={<i className="fa fa-hourglass-2" />}
              >
                &nbsp; Xử lý kiến nghị
              </Button>
              <Button
                className="btn btn-danger report-row"
                onClick={this.onReject}
                loading={rejectLoading}
                icon={<i className="fa fa-close" />}
              >
                &nbsp; Gửi trả
              </Button>
              <Button
                className="btn btn-primary report-row"
                onClick={this.onApprove}
                loading={sendLoading}
                icon={<i className="fa fa-check-square-o" />}
              >
                &nbsp; Duyệt
              </Button>
            </>
          )}
        </div>
        <section className="content v-content">
          <div className="box box-primary">
            <div className="box-header with-border">
              {!loading && !report && (
                <h3 className="box-title">Không tìm thấy báo cáo</h3>
              )}
              {loading && (
                <h3 className="box-title">
                  <i className="fa fa-fw fa-spinner fa-spin" />
                  Đang tải báo cáo....
                </h3>
              )}
              {!loading && report && (
                <h3 className="box-title">{report.name}</h3>
              )}
            </div>
            <div className="box-body">
              {!loading && report && (
                <CkEditor
                  data={report.data}
                  id={"reportDetail"}
                  onInitEditor={this.initEditor}
                  config={{
                    allowedContent: true,
                    extraPlugins: plugin,
                    height: 500,
                    removePlugins: "sourcearea",
                    basicEntities: false,
                    entities_latin: false
                  }}
                />
              )}
            </div>
          </div>
        </section>
        <DirectionModal
          ref="direction"
          users={users}
          workplaces={workplaces}
          onDirection={this.onDirection}
          onRemoveDirection={this.onRemoveDirection}
        />
        <AttachmentModal
          ref="attachment"
          loading={uploading}
          files={attachments}
          chooseFiles={this.chooseFiles}
          onRemove={this.onRemoveFile}
        />
      </div>
    );
  }
}

export default ReportSummaryDetailRender;
