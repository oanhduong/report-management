import React from "react";
import ReportSummaryDetailRender from "./render";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";
import * as Services from "services";
import { Utils } from "common";

class ReportSummaryDetail extends React.PureComponent {
  state = {
    report: null,
    loading: false,
    saveLoading: false,
    sendLoading: false,
    rejectLoading: false,
    users: [],
    uploading: false

  };

  componentDidMount = () => {
    const { match, user, users } = this.props;
    let id = match.params.id;
    this.loadReportSummary(id);
    this.loadUsers();
  };

  loadUsers = () => {
    Services.getUserForApprove()
      .then(response => {
        this.setState({ users: response.list });
      })
      .catch(error => {
        console.log(error);
      });
  };

  loadReportSummary = id => {
    this.setState({ loading: true });
    Services.getReportSummaryDetail(id)
      .then(response => {
        this.setState({ loading: false, report: response });
      })
      .catch(error => {
        Utils.showError(error);
        this.setState({ loading: false, report: null });
      });
  };

  refreshReport = () => {
    const { match } = this.props;
    let id = match.params.id;
    this.loadReportSummary(id);
  };

  saveReport = dataHtml => {
    this.setState({ saveLoading: true });
    this.saveReportSummary(dataHtml, {
      success: response => {
        this.setState({ saveLoading: false });
        Utils.showSuccess("Lưu báo cáo tổng hợp thành công");
      },
      error: err => {
        Utils.showError(err);
        this.setState({ saveLoading: false });
      }
    });
  };

  saveReportSummary = (dataHtml, { success, error }) => {
    const { match } = this.props;
    let id = match.params.id;
    Services.updateReportSummary(id, { data: dataHtml })
      .then(response => {
        success(response);
      })
      .catch(err => {
        error(err);
      });
  };

  onReject = data => {
    const { report } = this.state;
    if (report == null) {
      return Utils.showError("Chưa tải báo cáo");
    }
    const { match } = this.props;
    let id = match.params.id;
    this.setState({ rejectLoading: true });
    this.saveReportSummary(data, {
      success: () => {
        Services.rejectReportSummary(id)
          .then(response => {
            this.props.history.replace("/app/reportSummaryReject");
            this.setState({ rejectLoading: false });
            Utils.showSuccess("Gửi trả báo cáo tổng hợp thành công");
          })
          .catch(error => {
            Utils.showError(error);
            this.setState({ rejectLoading: false });
          });
      },
      error: err => {
        Utils.showError(err);
        this.setState({ rejectLoading: false });
      }
    });
  };

  sendReport = (data, chidao) => {
    const { report } = this.state;
    if (report == null) {
      return Utils.showError("Chưa tải báo cáo");
    }
    const { match } = this.props;
    let id = match.params.id;
    this.setState({ sendLoading: true });
    this.saveReportSummary(data, {
      success: () => {
        Services.sendReportSummary(id, chidao)
          .then(response => {
            this.props.history.replace("/app/reportSummarySent");
            this.setState({ sendLoading: false });
            Utils.showSuccess("Gửi báo cáo tổng hợp thành công");
          })
          .catch(error => {
            Utils.showError(error);
            this.setState({ sendLoading: false });
          });
      },
      error: err => {
        Utils.showError(err);
        this.setState({ sendLoading: false });
      }
    });
  };

  onChangeAttachment = files => {
    let report = { ...this.state.report };
    let attachments = report.attachments.concat(files);
    const { match } = this.props;
    let id = match.params.id;
    this.setState({ uploading: true });
    Services.updateReportSummary(id, { attachments })
      .then(response => {
        this.setState({ uploading: false });
        report.attachments = response.attachments;
        this.setState({ report });
      })
      .catch(error => {
        this.setState({ uploading: false });
        Utils.showError(error);
      });
  };

  onRemoveAttachment = file => {
    let report = { ...this.state.report };
    let attachments= report.attachments;
    let index = _.findIndex(attachments, att => att._id === file._id);
    if (index < 0) {
      return;
    }
    attachments.splice(index, 1);
    const { match } = this.props;
    let id = match.params.id;
    this.setState({ uploading: true });
    Services.updateReportSummary(id, { attachments })
      .then(response => {
        this.setState({ uploading: false });
        report.attachments = response.attachments;
        this.setState({ report });
      })
      .catch(error => {
        this.setState({ uploading: false });
        Utils.showError(error);
      });
  };

  render() {
    const {
      report,
      loading,
      saveLoading,
      sendLoading,
      rejectLoading,
      users,
      uploading
    } = this.state;
    const { user, workplaces } = this.props;
    return (
      <ReportSummaryDetailRender
        report={report}
        user={user}
        loading={loading}
        sendLoading={sendLoading}
        saveLoading={saveLoading}
        rejectLoading={rejectLoading}
        users={users}
        workplaces={workplaces}
        refreshReport={this.refreshReport}
        saveReport={this.saveReport}
        sendReport={this.sendReport}
        onReject={this.onReject}
        onChangeAttachment={this.onChangeAttachment}
        uploading={uploading}
        onRemoveAttachment={this.onRemoveAttachment}
      />
    );
  }
}

ReportSummaryDetail.defaultProps = {
  user: {},
  users: [],
  workplaces: []
};
function mapStateToProps({ usersReducers }) {
  return {
    user: usersReducers.user,
    users: usersReducers.users,
    workplaces: usersReducers.workplaces
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ReportSummaryDetail));
