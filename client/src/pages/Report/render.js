import React from "react";
import { Cascader, Modal } from "antd";
import { Button, CkEditor } from "components";

import { Utils, Config } from "common";
import * as _ from "lodash";

import "./styles.css";

class ReportRender extends React.PureComponent {
  state = {
    contentReport: "",
    reportType: "",
    templateLoading: false,
    timeTypeNumber: "",
    year: new Date().getFullYear(),
    timeType: "Thang",
    nameTimeType: "Tháng"
  };

  templateId = null;

  onChangeReport = (value, selected) => {
    let name = "";
    let timeType = selected[1].timeType;
    selected.forEach((element, index) => {
      if (index != 0) {
        name += "_" + element.name;
      } else {
        name += element.name;
      }
    });
    const { unit } = this.props;
    let nameTimeType = "";
    if (timeType === "Quy") {
      nameTimeType = "Quý";
      name += "_" + unit + "_Q";
    } else {
      nameTimeType = "Tháng";
      name += "_" + unit + "_T";
    }
    this.setState({ reportType: name, nameTimeType, timeType }, () => {
      this.templateId = value[1];
      this.props.onLoadReportTemplate(value[1]);
    });
  };

  refresh = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muón làm mới không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.refreshReport(this.templateId);
      }
    });
  };

  saveReport = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muón lưu báo cáo không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        const { reportType, timeTypeNumber, year } = this.state;
        if (Utils.isEmpty(timeTypeNumber)) {
          return Utils.showWarning("Vui lòng nhập thời gian");
        }
        if (Utils.isEmpty(year + "")) {
          return Utils.showWarning("Vui lòng nhập năm báo cáo");
        }
        if (parseInt(timeTypeNumber) <= 0) {
          return Utils.showWarning("Vui lòng nhập quý không định dạng");
        }
        if (parseInt(year) <= 0) {
          return Utils.showWarning("Vui lòng nhập năm không định dạng");
        }
        if (this.templateId === null) {
          return Utils.showWarning("Vui lòng chọn một mãu báo cáo");
        }
        let data = {
          name: reportType + "_" + timeTypeNumber + "_" + year,
          data: this.editor.getData(),
          template: this.templateId
        };
        this.props.onSaveReport(data);
      }
    });
  };

  sendReport = () => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muón gửi báo cáo không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        const { reportType, timeTypeNumber, year, timeType } = this.state;
        if (Utils.isEmpty(timeTypeNumber)) {
          return Utils.showWarning("Vui lòng nhập thời gian");
        }
        if (Utils.isEmpty(year + "")) {
          return Utils.showWarning("Vui lòng nhập năm báo cáo");
        }
        if (parseInt(timeTypeNumber) <= 0) {
          return Utils.showWarning("Vui lòng nhập quý không định dạng");
        } else {
          if (timeType === "Thang") {
            if (timeTypeNumber <= 0 || timeTypeNumber > 12) {
              return Utils.showWarning("Vui lòng nhập tháng giá trị từ 1 - 12");
            }
          } else {
            if (timeTypeNumber <= 0 || timeTypeNumber > 4) {
              return Utils.showWarning("Vui lọng nhập quý giá trị từ 1 - 4");
            }
          }
        }
        if (parseInt(year) <= 0) {
          return Utils.showWarning("Vui lòng nhập năm không định dạng");
        }
        if (this.templateId === null) {
          return Utils.showWarning("Vui lòng chọn một mãu báo cáo");
        }
        let data = {
          name: reportType + "_" + timeTypeNumber + "_" + year,
          data: this.editor.getData(),
          template: this.templateId
        };
        this.props.onReportSent(data);
      }
    });
  };

  onChangeNameReport = e => {
    this.setState({ timeTypeNumber: e.target.value });
  };

  onChangeYear = e => {
    this.setState({ year: e.target.value });
  };

  render() {
    const { reportType, timeTypeNumber, year, nameTimeType } = this.state;
    let disabled = !reportType || reportType.length == 0;
    const {
      templates,
      templateLoading,
      reportTemplate,
      saveLoading,
      sending
    } = this.props;
    return (
      <div>
        <section className="content-header" />
        <div className="report-title report-row">Biểu mẫu</div>
        <div className="report-row">
          <Cascader
            fieldNames={{ label: "name", value: "_id", children: "items" }}
            options={templates}
            style={{ width: 600 }}
            onChange={this.onChangeReport}
            placeholder="Chọn mẫu báo cáo"
          />
        </div>
        <Button
          className="btn btn-default report-row"
          onClick={this.refresh}
          disabled={disabled}
          icon={<i className="fa fa-refresh" />}
        >
          &nbsp; Làm mới
        </Button>
        <Button
          className="btn btn-default report-row"
          onClick={this.saveReport}
          loading={saveLoading}
          disabled={disabled}
          icon={<i className="fa fa-save" />}
        >
          &nbsp; Lưu
        </Button>
        <Button
          className="btn btn-primary report-row"
          onClick={this.sendReport}
          disabled={disabled}
          loading={sending}
          icon={<i className="fa fa-send-o" />}
        >
          &nbsp; Gửi báo cáo
        </Button>
        <section className="content v-content">
          <div className="box box-primary">
            <div className="box-header with-border">
              {!templateLoading && !reportTemplate && (
                <h3 className="box-title">Vui lòng chọn mẫu báo cáo</h3>
              )}
              {templateLoading && (
                <h3 className="box-title">
                  <i className="fa fa-fw fa-spinner fa-spin" />
                  Đang tải mẫu báo cáo...
                </h3>
              )}
              {!templateLoading && reportTemplate && (
                <div className="form-group">
                  <label className="pr0 pf0 col-sm-2 control-label ">
                    Tên báo cáo:
                  </label>
                  <div className="pr0 pf0 col-sm-10">
                    {reportType} &nbsp;
                    <input
                      type="number"
                      className="form-control"
                      value={timeTypeNumber}
                      placeholder={nameTimeType}
                      style={{ width: "100px", display: "inline-block" }}
                      onChange={this.onChangeNameReport}
                    />
                    &nbsp;
                    <input
                      type="number"
                      className="form-control"
                      value={year}
                      placeholder="Năm"
                      style={{ width: "100px", display: "inline-block" }}
                      onChange={this.onChangeYear}
                    />
                  </div>
                </div>
              )}
            </div>
            <div className="box-body">
              {!templateLoading && reportTemplate && (
                <CkEditor
                  data={reportTemplate.data}
                  id={"reportType"}
                  onInitEditor={editor => {
                    this.editor = editor;
                  }}
                  config={{
                    allowedContent: true,
                    extraPlugins: "comments",
                    height: 500,
                    fullPage: true
                  }}
                />
              )}
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default ReportRender;
