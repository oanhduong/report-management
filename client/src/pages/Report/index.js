import React from "react";
import { withRouter } from "react-router-dom";
import ReportRender from "./render";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";
import * as Services from "services";
import { findIndex } from "lodash";

import "./styles.css";
import { Utils } from "common";

class Report extends React.PureComponent {
  state = {
    templateLoading: false,
    reportTemplate: null,
    saveLoading: false,
    sending: false
  };
  report = null;

  onReportSent = data => {
    this.setState({ sending: true });
    if (this.report && this.report._id) {
      Services.updateReportNormal(this.report._id, data)
        .then(response => {
          this.report = response;
          Services.sendReport(this.report._id)
            .then(response => {
              this.setState({ sending: false });
              this.props.history.replace("/app/reportSent");
            })
            .catch(error => {
              this.setState({ sending: false });
              Utils.showError(error);
            });
        })
        .catch(error => {
          this.setState({ sending: false });
          Utils.showError(error);
        });
    } else {
      Services.createReportNormal(data)
        .then(response => {
          this.report = response;
          Services.sendReport(this.report._id)
            .then(res => {
              this.setState({ sending: false });
              this.props.history.replace("/app/reportSent");
            })
            .catch(err => {
              this.setState({ sending: false });
              Utils.showError(err);
            });
        })
        .catch(error => {
          this.setState({ sending: false });
          Utils.showError(error);
        });
    }
  };

  onSaveReport = data => {
    this.setState({ saveLoading: true });
    if (this.report) {
      Services.updateReportNormal(this.report._id, data)
        .then(response => {
          this.report = response;
          this.setState({ saveLoading: false });
          this.refreshReport();
          Utils.showSuccess("Lưu báo cáo thành công");
        })
        .catch(error => {
          this.setState({ saveLoading: false });
          Utils.showError(error);
        });
    } else {
      Services.createReportNormal(data)
        .then(response => {
          this.report = response;
          this.setState({ saveLoading: false });
          this.refreshReport();
          Utils.showSuccess("Tạo báo cáo thành công");
        })
        .catch(error => {
          this.setState({ saveLoading: false });
          Utils.showError(error);
        });
    }
  };

  onLoadReportTemplate = id => {
    this.setState({ templateLoading: true });
    Services.getReportTemplate(id)
      .then(response => {
        this.report = null;
        let reportTemplate = response;
        this.setState({ templateLoading: false, reportTemplate });
      })
      .catch(error => {
        Utils.showError(error);
        this.setState({ templateLoading: false });
      });
  };

  getReportNormal = id => {
    this.setState({ templateLoading: true });
    Services.getReportNormalDetail(id)
      .then(response => {
        this.report = response;
        this.setState({ templateLoading: false, reportTemplate: response });
      })
      .catch(error => {
        Utils.showError(error);
        this.setState({ templateLoading: false });
      });
  };

  refreshReport = templateId => {
    if (this.report && this.report._id) {
      this.getReportNormal(this.report._id);
    } else {
      this.onLoadReportTemplate(templateId);
    }
  };

  componentDidMount = () => {
    const { templates } = this.props;
    if (!templates || templates.length === 0) {
      this.props.getReportTemplates();
    } else {
    }
  };

  _getUnit = () => {
    const { user, workplaces } = this.props;
    let unit = "";
    if (user && workplaces && workplaces.length > 0) {
      let index = findIndex(
        workplaces,
        item => item._id === user.workPlace._id
      );
      if (index >= 0) {
        return workplaces[index].shortName;
      }
    }
    return unit;
  };

  render() {
    const { templates } = this.props;
    const {
      templateLoading,
      reportTemplate,
      saveLoading,
      sending
    } = this.state;
    let unit = this._getUnit();
    return (
      <ReportRender
        templates={templates}
        onReportSent={this.onReportSent}
        onSaveReport={this.onSaveReport}
        onLoadReportTemplate={this.onLoadReportTemplate}
        refreshReport={this.refreshReport}
        templateLoading={templateLoading}
        reportTemplate={reportTemplate}
        saveLoading={saveLoading}
        sending={sending}
        unit={unit}
      />
    );
  }
}

Report.defaultProps = {
  templates: [],
  workplaces: [],
  user: {}
};

function mapStateToProps({ reportReducers, usersReducers }) {
  return {
    type: reportReducers.type,
    reports: reportReducers.type,
    templates: reportReducers.templates,
    workplaces: usersReducers.workplaces,
    user: usersReducers.user
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Report));
