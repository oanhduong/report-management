import React from "react";
import {
  Button,
  StatusReportNormal,
  DownloadReport,
  HistoryModal,
  DateRange,
  HeaderPage
} from "components";
import moment from "moment";
import { Table, Input, Button as AButton, Icon } from "antd";
import { Link } from "react-router-dom";
import "moment/locale/vi";
moment.locale("vi");

class ReportSentRender extends React.PureComponent {
  state = {
    searchText: ""
  };
  dateRange = null;

  handleTableChange = (pagination, filters, sorter, extra) => {
    let name = filters.name ? filters.name[0] : null;
    this.props.loadReportSent(pagination.current, name, this.dateRange);
  };

  showHistory = reportId => {
    this.refs.history.open(reportId);
  };

  getColumnSearchProps = () => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Tên báo cáo`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <AButton
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ marginRight: 8 }}
        >
          Tìm kiếm báo cáo
        </AButton>
        <AButton
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Huỷ bỏ
        </AButton>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    )
  });

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  onDateRange = data => {
    this.dateRange = data;
    const { searchText } = this.state;
    this.props.loadReportSent(1, searchText, data);
  };

  render() {
    const { reports, pagination, loading, user } = this.props;
    const columns = [
      {
        title: "Tên báo cáo",
        dataIndex: "name",
        ...this.getColumnSearchProps("name"),
        width: "40%",
        render: (name, record) => (
          <Link to={`/app/reportDetail/${record._id}`}>{record.name}</Link>
        )
      },
      {
        title: "Trạng thái",
        with: "20%",
        render: (name, record) => {
          return <StatusReportNormal report={record} userRole={user.role} />;
        }
      },
      {
        title: "Cập nhật lần cuối",
        with: "20%",
        render: (name, record) => moment(record.updatedAt).calendar()
      },
      {
        title: "Lịch sử",
        with: "5%",
        render: (name, record) => (
          <>
            <Button
              className="btn btn-info btn-xs"
              onClick={this.showHistory.bind(this, record)}
            >
              <i className="fa fa-search" /> Xem
            </Button>
          </>
        )
      },
      {
        title: "Tải xuống",
        with: "15%",
        render: record => (
          <>
            <DownloadReport report={record} />
          </>
        )
      }
    ];
    return (
      <div>
        <HeaderPage title={"Báo cáo đã gửi"} />
        <section className="content v-content">
          <div className="box box-primary">
            <div className="box-header with-border">
              <h3 className="box-title">Danh sách báo cáo đã gửi </h3>
            </div>
            <div className="box-body">
              <DateRange onDate={this.onDateRange} />
              <Table
                columns={columns}
                rowKey={record => record._id}
                dataSource={reports}
                pagination={pagination}
                loading={loading}
                onChange={this.handleTableChange}
              />
            </div>
          </div>
        </section>
        <HistoryModal ref={"history"} />
      </div>
    );
  }
}

export default ReportSentRender;
