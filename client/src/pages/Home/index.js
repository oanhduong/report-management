import React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";
import HomeRender from "./render";
import socketIOClient from "socket.io-client";
import { Config, Utils } from "common";
import * as Services from "services";
import { Modal } from "antd";

class Home extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      notifications: []
    };
  }

  componentDidMount = () => {
    this.props.getWorkPlaces();
    this.initSocket();
    this.getUser();
  };

  _convertMessge = message => {
    let msg = "";
    switch (message.action) {
      case "send":
        msg = " đang chờ xử lý";
        break;
      case "reject":
        msg = " đã bị gửi trả";
        break;
      case "complete":
        msg = " đã được duyệt";
        break;
      case "deadline":
        msg = " tới hạn cần xử lý";
        break;
    }

    let m = message.report.name + msg;
    let type = message.report.listReportIds ? "summary" : "normal";
    let data = {
      id: message._id,
      message: m,
      reportId: message.report._id,
      type,
      action: message.action
    };
    return data;
  };

  initSocket = () => {
    this.socket = socketIOClient(Config.WEB_SOCKET);
    if (this.socket) {
      const { user } = this.props;
      this.socket.on(user._id, message => {
        let notifications = this.state.notifications.slice();
        notifications.push(this._convertMessge(message));
        this.setState({ notifications });
      });
    }
  };

  getUser = () => {
    const { user } = this.props;
    Services.getUser(user._id)
      .then(response => {
        if (
          response.user &&
          response.user.messages &&
          response.user.messages.length > 0
        ) {
          let dealine = null;
          let messages = response.user.messages.filter(item => {
            if (item.action === "deadline") {
              dealine = item;
            }
            return !item.seen;
          });
          if (dealine) {
            Modal.warning({
              title: "Cảnh báo",
              content: "Tới hạn cần xử lý báo cáo: " + dealine.report.name,
              okText: "Đồng ý",
              onOk: () => {
                this.props.history.replace("/app/report");
              }
            });
          }
          messages = messages.map(item => {
            return this._convertMessge(item);
          });
          this.setState({ notifications: messages });
        }
      })
      .catch(error => {
        Utils.showError(error);
      });
  };

  markSeenNotification = obj => {
    let url = "/app/reportDetail/";
    if (obj.type === "summary") {
      url = "/app/reportSummaryDetail/";
    }
    const { user } = this.props;
    Services.markSeenNotification(user._id, { messageIds: [obj.id] })
      .then(() => {
        let notifications = this.state.notifications.slice();
        notifications = notifications.filter(item => {
          return item.id !== obj.id;
        });
        if (obj.action !== "deadline") {
          if (
            this.props.location.pathname.indexOf("/app/reportDetail") >= 0 ||
            this.props.location.pathname.indexOf("/app/reportSummaryDetail") >=
              0
          ) {
            this.props.history.replace(url + obj.reportId);
            location.reload();
          } else {
            this.props.history.replace(url + obj.reportId);
          }
        }
        this.setState({ notifications });
      })
      .catch(error => {
        Utils.showError(error);
      });
  };

  componentWillUnmount = () => {
    if (this.socket) {
      this.socket.disconnect();
    }
  };

  onLogout = () => {
    this.props.logout();
    this.props.cleanTemplates();
    localStorage.clear();
    this.props.history.replace("/");
  };

  onProfile = () => {
    this.props.history.replace("/app/profile");
  };

  onReport = () => {
    this.props.history.replace("/app/report");
  };

  render() {
    const { user } = this.props;
    const { notifications } = this.state;
    return (
      <HomeRender
        user={user}
        onLogout={this.onLogout}
        onProfile={this.onProfile}
        onReport={this.onReport}
        notifications={notifications}
        workplaceName={user.workPlace.shortName}
        markSeenNotification={this.markSeenNotification}
      />
    );
  }
}

Home.defaultProps = {
  reports: []
};

function mapStateToProps({ usersReducers, reportReducers }) {
  return {
    user: usersReducers.user,
    reports: reportReducers.reports,
    workplaces: usersReducers.workplaces
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Home)
);
