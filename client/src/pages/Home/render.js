import React, { Suspense } from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import { Notification, AvatarDropdown, Menu, Loading } from "components";
import { Config, Constants } from "common";
import "./styles.css";
import { findIndex } from "lodash";
const Report = React.lazy(() => import("pages/Report"));
const Profile = React.lazy(() => import("pages/Profile"));
const ReportSent = React.lazy(() => import("pages/ReportSent"));
const ReportDraft = React.lazy(() => import("pages/ReportDraft"));
const ReportNew = React.lazy(() => import("pages/ReportNew"));
const ReportDetail = React.lazy(() => import("pages/ReportDetail"));
const ReportFailed = React.lazy(() => import("pages/ReportFailed"));
const ReportUnit = React.lazy(() => import("pages/ReportUnit"));
const ReportSummaryUpdating = React.lazy(() =>
  import("pages/ReportSummaryUpdating")
);
const ReportSummaryDetail = React.lazy(() =>
  import("pages/ReportSummaryDetail")
);
const ReportSummarySent = React.lazy(() => import("pages/ReportSummarySent"));
const ReportSummaryReject = React.lazy(() =>
  import("pages/ReportSummaryReject")
);
const ReportSummaryNew = React.lazy(() => import("pages/ReportSummaryNew"));
const Template = React.lazy(() => import("pages/Template"));
const Templates = React.lazy(() => import("pages/Templates"));
const TemplateDetail = React.lazy(() => import("pages/TemplateDetail"));
const Users = React.lazy(() => import("pages/Users"));
const ReportMerge = React.lazy(() => import("pages/ReportMerge"));
const ViewReportUnit = React.lazy(() => import("pages/ViewReportUnit"));
const ReportComplete = React.lazy(() => import("pages/ReportComplete"));
const Notifications = React.lazy(() => import("pages/Notifications"));
const Statistics = React.lazy(() => import("pages/Statistics"));

class HomeRender extends React.PureComponent {
  constructor(props) {
    super(props);
    const { pathname } = props.location;
    let indexMenu = findIndex(Config.MENUS, m => m.path === pathname);
    this.state = {
      selectMenu: indexMenu
    };
  }

  render() {
    const {
      user,
      onLogout,
      onProfile,
      onReport,
      notifications,
      workplaceName,
      markSeenNotification
    } = this.props;
    if (!user) return <div />;
    let fullName = user.username;
    if (user && user.firstName && user.lastName) {
      fullName = user.firstName + " " + user.lastName;
    }
    return (
      <div className="wrapper wrapper-font-size">
        <header className="main-header">
          <a href="#" className="logo">
            <span className="logo-mini">
              <b>PV</b>
            </span>
            <img
              src="/public/AdminLTE/img/pvg_header.JPG"
              width="230"
              height="50"
            />
          </a>
          <nav className="navbar navbar-static-top">
            <a
              href="#"
              className="sidebar-toggle"
              data-toggle="push-menu"
              role="button"
            />
            <div className="navbar-custom-menu">
              <ul className="nav navbar-nav">
                <Notification
                  reports={notifications}
                  onClick={markSeenNotification}
                />
                <AvatarDropdown
                  user={user}
                  onProfile={onProfile}
                  onLogout={onLogout}
                />
              </ul>
            </div>
          </nav>
        </header>
        <aside className="main-sidebar">
          <section className="sidebar">
            <div
              style={{ cursor: "pointer" }}
              className="user-panel"
              onClick={onProfile}
            >
              <div className="pull-left image">
                <img
                  src="/public/AdminLTE/img/ic_default_avatar.jpg"
                  className="img-circle"
                  alt="User Image"
                />
              </div>
              <div className="pull-left info">
                <p>{fullName}</p>
                <a href="#">{workplaceName}</a>
              </div>
            </div>
            {user.role == Constants.Role.NV && (
              <form
                action="#"
                method="get"
                className="sidebar-form"
                style={{ border: "none" }}
              >
                <button type="button" className="create-btn" onClick={onReport}>
                  <i className="fa fa-plus text-success" />
                  &nbsp;&nbsp;Tạo báo cáo
                </button>
              </form>
            )}
            {user.role !== Constants.Role.NV && <br />}
            <Menu user={user} />
          </section>
        </aside>
        <div
          className="content-wrapper"
          style={{ minHeight: "80vh", overflow: "auto" }}
        >
          <Switch>
            <Route path="/app/report">
              <Suspense fallback={<Loading />}>
                <Report />
              </Suspense>
            </Route>
            <Route path="/app/profile">
              <Suspense fallback={<Loading />}>
                <Profile />
              </Suspense>
            </Route>
            <Route path="/app/reportSent">
              <Suspense fallback={<Loading />}>
                <ReportSent />
              </Suspense>
            </Route>
            <Route path="/app/reportDraft">
              <Suspense fallback={<Loading />}>
                <ReportDraft />
              </Suspense>
            </Route>
            <Route path="/app/reportNew">
              <Suspense fallback={<Loading />}>
                <ReportNew />
              </Suspense>
            </Route>
            <Route path="/app/reportDetail/:id">
              <Suspense fallback={<Loading />}>
                <ReportDetail />
              </Suspense>
            </Route>
            <Route path="/app/reportFailed">
              <Suspense fallback={<Loading />}>
                <ReportFailed />
              </Suspense>
            </Route>
            <Route path="/app/reportUnit">
              <Suspense fallback={<Loading />}>
                <ReportUnit />
              </Suspense>
            </Route>
            <Route path="/app/reportSummaryUpdating">
              <Suspense fallback={<Loading />}>
                <ReportSummaryUpdating />
              </Suspense>
            </Route>
            <Route path="/app/reportSummaryDetail/:id">
              <Suspense fallback={<Loading />}>
                <ReportSummaryDetail />
              </Suspense>
            </Route>
            <Route path="/app/reportSummarySent">
              <Suspense fallback={<Loading />}>
                <ReportSummarySent />
              </Suspense>
            </Route>
            <Route path="/app/reportSummaryReject">
              <Suspense fallback={<Loading />}>
                <ReportSummaryReject />
              </Suspense>
            </Route>
            <Route path="/app/reportSummaryNew">
              <Suspense fallback={<Loading />}>
                <ReportSummaryNew />
              </Suspense>
            </Route>
            <Route path="/app/template">
              <Suspense fallback={<Loading />}>
                <Template />
              </Suspense>
            </Route>
            <Route path="/app/templates">
              <Suspense fallback={<Loading />}>
                <Templates />
              </Suspense>
            </Route>
            <Route path="/app/templateDetail/:id">
              <Suspense fallback={<Loading />}>
                <TemplateDetail />
              </Suspense>
            </Route>
            <Route path="/app/users">
              <Suspense fallback={<Loading />}>
                <Users />
              </Suspense>
            </Route>
            <Route path="/app/reportMerge">
              <Suspense fallback={<Loading />}>
                <ReportMerge />
              </Suspense>
            </Route>
            <Route path="/app/viewReportUnit">
              <Suspense fallback={<Loading />}>
                <ViewReportUnit />
              </Suspense>
            </Route>
            <Route path="/app/reportComplete">
              <Suspense fallback={<Loading />}>
                <ReportComplete />
              </Suspense>
            </Route>
            <Route path="/app/notifications">
              <Suspense fallback={<Loading />}>
                <Notifications />
              </Suspense>
            </Route>
            <Route path="/app/statistics">
              <Suspense fallback={<Loading />}>
                <Statistics />
              </Suspense>
            </Route>
          </Switch>
        </div>
      </div>
    );
  }
}

export default withRouter(HomeRender);
