import React from "react";
import UsersRender from "./render";
import * as Services from "services";
import { Utils, Constants } from "common";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";

class Users extends React.PureComponent {
  state = {
    pagination: {
      pageSize: Constants.Limit,
      current: 1,
      total: 0
    },
    users: [],
    loading: false,
    submiting: false
  };
  componentDidMount = () => {
    this.getUsers(1);
  };

  getUsers = (page, filters) => {
    this.setState({ loading: true });
    Services.getUsers(page, filters)
      .then(response => {
        let pagination = { ...this.state.pagination };
        pagination.current = page;
        pagination.total = response.total;
        this.setState({ loading: false, users: response.list, pagination });
      })
      .catch(error => {
        Utils.showError(error);
        this.setState({ loading: false });
      });
  };

  createUser = (user, callback) => {
    if (Utils.isEmpty(user.firstName)) {
      return Utils.showWarning("Chưa nhập họ của người dùng");
    }
    if (Utils.isEmpty(user.lastName)) {
      return Utils.showWarning("Chưa nhập tên của người dùng");
    }
    if (Utils.isEmpty(user.email)) {
      return Utils.showWarning("Chưa nhập địa chỉ email của người dùng");
    }
    if (Utils.isEmpty(user.username)) {
      return Utils.showWarning("Chưa nhập tài khoản của người dùng");
    }
    if (Utils.isEmpty(user.password)) {
      return Utils.showWarning("Chưa nhập mật khẩu của người dùng");
    }
    this.setState({ submiting: true });
    Services.createUser(user)
      .then(response => {
        this.getUsers(1);
        Utils.showSuccess("Tạo người dùng thành công");
        this.setState({ submiting: false });
        callback();
      })
      .catch(error => {
        Utils.showError(error);
        this.setState({ submiting: false });
        callback();
      });
  };

  updateUser = (user, callback) => {
    if (Utils.isEmpty(user.firstName)) {
      return Utils.showWarning("Chưa nhập họ của người dùng");
    }
    if (Utils.isEmpty(user.lastName)) {
      return Utils.showWarning("Chưa nhập tên của người dùng");
    }
    if (Utils.isEmpty(user.email)) {
      return Utils.showWarning("Chưa nhập địa chỉ email của người dùng");
    }
    this.setState({ submiting: true });
    Services.updateUser(user)
      .then(response => {
        this.getUsers(1);
        Utils.showSuccess("Cập nhật người dùng thành công");
        this.setState({ submiting: false });
        callback();
      })
      .catch(error => {
        Utils.showError(error);
        this.setState({ submiting: false });
        callback();
      });
  };

  deleteUser = id => {
    Services.deleteUser(id)
      .then(() => {
        this.getUsers(1);
        Utils.showSuccess("Xoá người dùng thành công");
      })
      .catch(error => {
        Utils.showError(error);
      });
  };

  render() {
    const { users, loading, pagination, submiting } = this.state;
    const { workplaces, type } = this.props;
    return (
      <UsersRender
        type={type}
        users={users}
        workplaces={workplaces}
        loading={loading}
        submiting={submiting}
        pagination={pagination}
        createUser={this.createUser}
        updateUser={this.updateUser}
        deleteUser={this.deleteUser}
        getUsers={this.getUsers}
      />
    );
  }
}

Users.defaultProps = {
  user: {},
  workplaces: []
};
function mapStateToProps({ usersReducers }) {
  return {
    user: usersReducers.user,
    workplaces: usersReducers.workplaces,
    type: usersReducers.type
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Users));
