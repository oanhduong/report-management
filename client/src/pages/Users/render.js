import React from "react";
import { Button, HeaderPage } from "components";
import {
  Table,
  Modal,
  Form,
  Input,
  Select,
  Row,
  Col,
  Radio,
  Button as AButton,
  Icon
} from "antd";
import { Constants, Utils } from "common";
import * as ActionTypes from "actions/ActionTypes";
const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 }
  }
};

class UsersRender extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      user: {
        _id: "",
        firstName: "",
        lastName: "",
        username: "",
        email: "",
        password: "",
        role: Constants.Role.NV,
        workPlace:
          (props.workplaces && props.workplaces.length > 0)
            ? props.workplaces[0]
            : {},
        workType: Constants.RoleGD.VHBD
      }
    };
  }

  getColumnSearchProps = name => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`${name}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <AButton
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ marginRight: 8 }}
        >
          Tìm kiếm
        </AButton>
        <AButton
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Huỷ bỏ
        </AButton>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    )
  });

  handleReset = clearFilters => {
    clearFilters();
  };

  handleSearch = (selectedKeys, confirm) => {
    confirm();
  };

  columns = [
    {
      title: "Tài khoản",
      dataIndex: "username",
      width: "15%"
    },
    {
      title: "Họ",
      width: "5%",
      dataIndex: "firstName",
      ...this.getColumnSearchProps("Tìm kiếm theo họ")
    },
    {
      title: "Tên",
      with: "25%",
      dataIndex: "lastName",
      ...this.getColumnSearchProps("Tìm kiếm theo tên")
    },
    {
      title: "Email",
      with: "20%",
      dataIndex: "email",
      ...this.getColumnSearchProps("Tìm kiếm theo email")
    },
    {
      title: "Quyền",
      with: "10%",
      dataIndex: "role",
      filterMultiple: true,
      filters: Object.keys(Constants.Role).map(item => {
        return {
          text: Utils.getRoleName(item),
          value: item
        };
      })
    },
    {
      title: "Nơi làm việc",
      with: "15%",
      dataIndex: "workPlace",
      ...this.getColumnSearchProps("Tên nơi làm việc"),
      render: (name, record) => {
        if (record.workPlace) {
          return record.workPlace.name;
        } else {
          return "";
        }
      }
    },
    {
      title: "Thao tác",
      width: "10%",
      render: (name, record) => (
        <>
          <Button
            className="btn btn-info btn-xs"
            onClick={this.open.bind(this, record)}
          >
            <i className="fa fa-edit" /> Sửa
          </Button>
          &nbsp;
          <Button
            className="btn btn-danger btn-xs"
            onClick={this.deleteUser.bind(this, record._id)}
          >
            <i className="fa fa-trash" /> Xoá
          </Button>
        </>
      )
    }
  ];

  open = user => {
    if (user) {
      if (!user.workType) {
        user.workType = Constants.RoleGD.VHBD;
      }
	  this.setState({ visible: true, user });
    } else {
      const { workplaces } = this.props;
      let u = { ...this.state.user };
      u._id = "";
      u.firstName = "";
      u.lastName = "";
      u.email = "";
      u.password = "";
      u.role = Constants.Role.NV;
      u.workPlace = workplaces[0];
      u.username = "";
      u.workType = Constants.RoleGD.VHBD;
	  this.setState({ visible: true, user: u });
    }
  };

  submit = () => {
    let user = Object.assign({}, this.state.user);
    if (
      !(
        user.role === Constants.Role.GD ||
        user.role === Constants.Role.TP ||
        user.role === Constants.Role.TPB ||
        user.role === Constants.Role.GDDV ||
        user.role === Constants.Role.TPKT
      )
    ) {
      delete user.workType;
    }
    if (user._id) {
      this.props.updateUser(user, () => {
        this.close();
      });
    } else {
      this.props.createUser(user, () => {
        this.close();
      });
    }
  };

  close = () => {
    this.setState({ visible: false });
  };

  onChangeInput = (e, field) => {
    let user = { ...this.state.user };
    user[field] = e.target.value;
    this.setState({ user });
  };

  onChangeFirstName = e => {
    this.onChangeInput(e, "firstName");
  };

  onChangeLastName = e => {
    this.onChangeInput(e, "lastName");
  };

  onChangeUsername = e => {
    this.onChangeInput(e, "username");
  };

  onChangePassword = e => {
    this.onChangeInput(e, "password");
  };
  onChangeEmail = e => {
    this.onChangeInput(e, "email");
  };

  onChangeRole = value => {
    let user = { ...this.state.user };
    user.role = value;
    this.setState({ user });
  };

  onChangeWorkPlace = id => {
    let user = { ...this.state.user };
    user.workPlace = this.props.workplaces.find(wp => wp._id === id);
    this.setState({ user });
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps.type === ActionTypes.GET_WORKPLACES_SUCCESS) {
      let user = { ...this.state.user };
      user.workPlace =
        nextProps.workplaces && nextProps.workplaces.length > 0
          ? nextProps.workplaces[0]
          : {};
      this.setState({ user });
    }
  };

  deleteUser = id => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn xoá người dùng không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.deleteUser(id);
      }
    });
  };

  onChangeWorkType = e => {
    let user = { ...this.state.user };
    user.workType = e.target.value;
    this.setState({ user });
  };

  handleTableChange = (pagination, filters, sorter, extra) => {
    this.props.getUsers(pagination.current, filters);
  };

  render() {
    const { loading, users, pagination, workplaces, submiting } = this.props;
    const { visible, user } = this.state;
    const isEdit = !(user._id !== null && user._id.length === 0);

    return (
      <div>
        <HeaderPage title={"Quản lý người dùng"} />
        <section className="content v-content">
          <div className="box box-primary">
            <div className="box-header with-border">
              <Button
                icon={<i className="fa fa-plus" />}
                className="btn btn-primary report-row btn-sm"
                onClick={this.open.bind(this, null)}
              >
                &nbsp; Tạo mới
              </Button>
            </div>
            <div className="box-body">
              <Table
                loading={loading}
                columns={this.columns}
                rowKey={record => record._id}
                dataSource={users}
                pagination={pagination}
                onChange={this.handleTableChange}
              />
            </div>
          </div>
        </section>

        <Modal
          title="Thông tin người dùng"
          visible={visible}
          onOk={this.submit}
          onCancel={this.close}
          okText={"Lưu"}
          cancelText={"Huỷ bỏ"}
          width={750}
          okButtonProps={{ loading: submiting }}
        >
          <Form {...formItemLayout} labelAlign={"left"}>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Họ và chữ lót" className="form-item">
                  <Input
                    value={user.firstName}
                    onChange={this.onChangeFirstName}
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Tên" className="form-item">
                  <Input
                    value={user.lastName}
                    onChange={this.onChangeLastName}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Địa chỉ email" className="form-item">
                  <Input value={user.email} onChange={this.onChangeEmail} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Quyền" className="form-item">
                  <Select value={user.role} onChange={this.onChangeRole}>
                    {Object.keys(Constants.Role).map((item, index) => {
                      return (
                        <Option key={index} value={Constants.Role[item]}>
                          {item}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            {(user.role === Constants.Role.GD ||
              user.role === Constants.Role.TP ||
              user.role === Constants.Role.TPB ||
              user.role === Constants.Role.GDDV ||
              user.role === Constants.Role.TPKT) && (
              <Row gutter={16}>
                <Col span={12} />
                <Col span={12} style={{ marginBottom: "10px" }}>
                  <Radio.Group
                    name="radiogroup"
                    value={user.workType}
                    onChange={this.onChangeWorkType}
                  >
                    <Radio value={Constants.RoleGD.VHBD}>
                      Vận hành bảo dưỡng
                    </Radio>
                    <Radio value={Constants.RoleGD.AT}>An toàn</Radio>
                  </Radio.Group>
                </Col>
              </Row>
            )}
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Tên tài khoản" className="form-item">
                  <Input
                    value={user.username}
                    onChange={this.onChangeUsername}
                    disabled={isEdit}
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Mật khẩu" className="form-item">
                  <Input
                    value={user.password}
                    type={"password"}
                    onChange={this.onChangePassword}
                    disabled={isEdit}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Form.Item label="Nơi làm việc" className="form-item">
              <Select value={user.workPlace._id} onChange={this.onChangeWorkPlace}>
                {workplaces.map((item, index) => {
                  return (
                    <Option key={index} value={item._id}>
                      {item.name}
                    </Option>
                  );
                })}
              </Select>
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
}

export default UsersRender;
