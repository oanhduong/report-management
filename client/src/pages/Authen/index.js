import React, { Suspense } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { Loading } from "components";
const Login = React.lazy(() => import("pages/Login"));
const ForgetPassword = React.lazy(() => import("pages/ForgetPassword"));

class Authen extends React.PureComponent {
  render() {
    return (
      <Switch>
        <Route
          exact
          path="/authen"
          render={() => {
            return <Redirect to="/authen/login" />;
          }}
        />
        <Route path="/authen/forget">
          <Suspense fallback={<Loading />}>
            <ForgetPassword />
          </Suspense>
        </Route>
        <Route path="/authen/login">
          <Suspense fallback={<Loading />}>
            <Login />
          </Suspense>
        </Route>
      </Switch>
    );
  }
}

export default Authen;
