import React from "react";
import { Button, DateRange, HistoryModal, HeaderPage } from "components";
import moment from "moment";
import { Link } from "react-router-dom";
import { Table, Icon, Input, Button as AButton } from "antd";
import { Modal } from "antd";

class ReportDraftRender extends React.PureComponent {
  state = {
    searchText: ""
  };
  dateRange = null;

  getColumnSearchProps = () => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Tên báo cáo`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <AButton
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ marginRight: 8 }}
        >
          Tìm kiếm báo cáo
        </AButton>
        <AButton
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Huỷ bỏ
        </AButton>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    )
  });

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };
  columns = [
    {
      title: "Tên báo cáo",
      dataIndex: "name",
      ...this.getColumnSearchProps(),
      width: "60%",
      render: (name, record) => (
        <Link to={`/app/reportDetail/${record._id}`}>{record.name}</Link>
      )
    },
    {
      title: "Cập nhật lần cuối",
      dataIndex: "updatedAt",
      with: "20%",
      render: (name, record) => {
        return moment(new Date(record.updatedAt)).calendar();
      }
    },
    {
      title: "Lịch sử",
      with: "20%",
      render: (text, record) => (
        <>
          <Button
            className="btn btn-info btn-xs"
            onClick={this.showHistory.bind(this, record)}
          >
            <i className="fa fa-search" /> Xem
          </Button>
          &nbsp;&nbsp;
          <Button
            className="btn btn-danger btn-xs"
            onClick={this.onDeleteReport.bind(this, record._id)}
          >
            <i className="fa fa-trash" /> Xoá
          </Button>
        </>
      )
    }
  ];

  showHistory = reportId => {
    this.refs.history.open(reportId);
  };

  handleTableChange = (pagination, filters, sorter, extra) => {
    let name = filters.name ? filters.name[0] : null;
    this.props.loadReportDraft(pagination.current, name, this.dateRange);
  };

  onDeleteReport = id => {
    Modal.confirm({
      title: "Xác nhận",
      content: "Bạn có muốn xoá báo cáo không?",
      okText: "Đồng ý",
      cancelText: "Huỷ bỏ",
      onOk: () => {
        this.props.onDeleteReport(id);
      }
    });
  };

  onDateRange = date => {
    this.dateRange = date;
    this.props.loadReportDraft(1, this.state.searchText, date);
  };

  render() {
    const { pagination, loading, reports } = this.props;
    return (
      <div>
        <HeaderPage title={"Báo cáo đang chỉnh sửa"} />
        <section className="content v-content">
          <div className="box box-primary">
            <div className="box-header with-border">
              <h3 className="box-title">Danh sách báo cáo đang chỉnh sửa</h3>
            </div>
            <div className="box-body">
              <DateRange onDate={this.onDateRange} />
              <Table
                columns={this.columns}
                rowKey={record => record._id}
                dataSource={reports}
                pagination={pagination}
                loading={loading}
                onChange={this.handleTableChange}
              />
            </div>
          </div>
        </section>
        <HistoryModal ref="history" />
      </div>
    );
  }
}

export default ReportDraftRender;
