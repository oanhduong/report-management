import React from "react";
import ReportDraftRender from "./render";
import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { Constants, Utils } from "common";
import * as Services from "services";

class ReportDraft extends React.PureComponent {
  state = {
    loading: false,
    pagination: {
      pageSize: Constants.Limit,
      current: 1,
      total: 0
    },
    reports: [],
    deleting: false
  };

  componentDidMount = () => {
    this.loadReportDraft(1);
  };

  loadReportDraft = (page, name, date) => {
    this.setState({ loading: true, reports: [] });
    Services.getReportsNormal(Constants.Status.Updating, page, name, date)
      .then(response => {
        let pagination = { ...this.state.pagination };
        pagination.current = page;
        pagination.total = response.total;
        this.setState({ loading: false, reports: response.list, pagination });
      })
      .catch(error => {
        this.setState({ loading: false, reports: [] });
        Utils.showError(error);
      });
  };

  deleteReport = id => {
    this.setState({ deleting: true });
    Services.deleteReport([id])
      .then(response => {
        this.loadReportDraft(1);
        Utils.showSuccess("Xoá báo cáo thành công");
      })
      .catch(error => {
        this.setState({ deleting: false });
        Utils.showError(error);
      });
  };

  render() {
    const { loading, pagination, reports } = this.state;
    return (
      <ReportDraftRender
        loading={loading}
        pagination={pagination}
        reports={reports}
        loadReportDraft={this.loadReportDraft}
        onDeleteReport={this.deleteReport}
      />
    );
  }
}

ReportDraft.defaultProps = {
  reportDrafts: []
};

function mapStateToProps({ reportReducers }) {
  return {
    type: reportReducers.type,
    reportDrafts: reportReducers.reportDrafts
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ReportDraft));
