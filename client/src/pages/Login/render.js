import React from "react";
import { Button } from "components";
import { Link } from "react-router-dom";
import "./styles.css";

class LoginRender extends React.PureComponent {
  render() {
    const {
      username,
      password,
      onLogin,
      onChangePassword,
      onChangeUsername,
      loading
    } = this.props;
    return (
      <div className="login-page login-container">
        <div className="login-card">
          <div className="login-logo">
            <img src="/public/AdminLTE/img/pvg_login.JPG" width="158" height="242" />
          </div>
          <div className="login-box-body">
            <p className="login-box-msg">Đăng nhập bắt đầu phiên làm việc</p>
            <form onSubmit={onLogin}>
              <div className="form-group has-feedback">
                <input
                  type="text"
                  value={username}
                  className="form-control"
                  placeholder="Tài khoản"
                  onChange={onChangeUsername}
                />
                <span className="glyphicon glyphicon-user form-control-feedback" />
              </div>
              <div className="form-group has-feedback">
                <input
                  type="password"
                  className="form-control"
                  value={password}
                  onChange={onChangePassword}
                  placeholder="Mật khẩu"
                />
                <span className="glyphicon glyphicon-lock form-control-feedback" />
              </div>
              <div className="row">
                <div className="col-xs-7">
                  <label>
                    <Link to={`/authen/forget`}>
                      Quên mật khẩu
                    </Link>
                  </label>
                </div>
                <div className="col-xs-5">
                  <Button
                    type="submit"
                    loading={loading}
                    className="btn btn-primary btn-block btn-flat"
                    icon={<i className="fa fa-sign-in"></i>}
                  >
                    &nbsp; Đăng nhập
                  </Button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginRender;
