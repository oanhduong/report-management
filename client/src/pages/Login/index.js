import React from "react";
import { withRouter } from "react-router-dom";

import { connect } from "react-redux";
import { ActionCreators } from "actions";
import { bindActionCreators } from "redux";
import * as ActionTypes from "actions/ActionTypes";
import LoginRender from "./render";
import { Utils, Constants } from "common";

class Login extends React.PureComponent {
  state = {
    username: "",
    password: ""
  };

  onChangePassword = event => {
    this.setState({ password: event.target.value });
  };

  onChangeUsername = event => {
    this.setState({ username: event.target.value });
  };

  onLogin = e => {
    e.preventDefault();
    this.props.login(this.state);
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps.type === ActionTypes.SIGNIN_SUCCESS) {
      switch (nextProps.user.role) {
        case Constants.Role.NV:
          this.props.history.push("/app/reportSent");
          break;
        case Constants.Role.TP:
        case Constants.Role.TPB:
        case Constants.Role.GDDV:
          this.props.history.push("/app/reportNew");
          break;
        case Constants.Role.NVKT:
          this.props.history.push("/app/reportUnit");
          break;
        case Constants.Role.TPKT:
        case Constants.Role.GD:
          this.props.history.push("/app/reportSummaryNew");
          break;
        case Constants.Role.ADMIN:
          this.props.history.push("/app/templates");
          break;
      }
    }
    if (nextProps.type === ActionTypes.SIGNIN_FAIL) {
      Utils.showError(nextProps.message);
    }
  };

  render() {
    const { username, password } = this.state;
    const { type } = this.props;
    return (
      <LoginRender
        onChangePassword={this.onChangePassword}
        onChangeUsername={this.onChangeUsername}
        username={username}
        password={password}
        onLogin={this.onLogin}
        loading={type === ActionTypes.SIGNIN_PENDING}
      />
    );
  }
}

Login.defaultProps = {
  message: false
};

function mapStateToProps({ usersReducers }) {
  return {
    type: usersReducers.type,
    message: usersReducers.message,
    user: usersReducers.user
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Login));
