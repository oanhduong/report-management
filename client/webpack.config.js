const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: ["babel-polyfill", "./src/index.js"],
  output: {
    path: path.join(__dirname, "/dist"),
    filename: "index_bundle.js",
    publicPath: "/"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(jpg|png|gif|pdf|ico)$/,
        loader: "file-loader"
      },
      {
        test: /\.(woff|woff2|eot|ttf|svg)$/,
        loader: "url-loader"
      }
    ]
  },
  resolve: {
    modules: [path.resolve(__dirname, "./src"), "node_modules"],
    extensions: [".js", ".jsx", ".json"],
    alias: {
      actions: path.resolve(__dirname, "./src/actions"),
      common: path.resolve(__dirname, "./src/common")
    }
  },
  devServer: {
    historyApiFallback: true,
    disableHostCheck: true
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./public/index.html"
    }),
	new CopyPlugin([
      { from: './public/AdminLTE', to: 'public/AdminLTE' },
      { from: './public/bootstrap', to: 'public/bootstrap' },
      { from: './public/ckeditor', to: 'public/ckeditor' },
      { from: './public/jquery', to: 'public/jquery' },
      { from: './public/jquery-ui', to: 'public/jquery-ui' }
    ])
  ]
};
